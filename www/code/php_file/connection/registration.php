<?php
include('../../../topMenu.ini.php');
$emaerror = "Електронна адреса уже зареєстрована";
include('../database/db.php');
if (isset($_REQUEST['submit'])) {
    $chb = $_REQUEST['cookie'];
    $name = htmlspecialchars(trim($_REQUEST['name']));
    $lname = htmlspecialchars(trim($_REQUEST['lname']));
    $email = htmlspecialchars(trim($_REQUEST['email']));
    $password = htmlspecialchars(trim($_REQUEST['password']));
    $r_password = htmlspecialchars(trim($_REQUEST['r_password']));
    if (!empty($chb)) {
        session_start();
        $_SESSION["email"] = $email;
        $_SESSION["password"] = $password;
    }
    //$query_in = mysql_query("SELECT email FROM tuser WHERE email='$e_email'");
    if ((mysqli_query($link, "SELECT email FROM tuser WHERE email=='$e_email'"))) {
        echo file_get_contents('registration.html');
        print_r($emaerror);
        exit();
    }
    if ($password == $r_password) {
        $password = md5($password);
        $query = mysqli_query($link, "INSERT INTO tuser VALUES('', '$name', '$lname', '$email', '$password')") or die(mysqli_error($link));
        echo($query);
        echo '<script>document.location.replace("/code/php-file/connection/login.php");</script>';
    } else {
        die('Перевірте введення паролю!');
    }
}
?>
<!DOCTYPE html>
<html lang="ua">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/styles/css/bootstrap-responsive.css">
    <link rel="stylesheet" href="/styles/css/main.css">
    <link rel="stylesheet" href="/styles/css/cover.css">
    <title>Вхід</title>
</head>
<body>
<div class='jumbotron container'>
    <div>
        <form method='post' action='registration.php' class='form-signin' role='form'>
            <input type='text' class='form-control' name='name' placeholder='| Дмитро' required/> <br>
            <input type='text' class='form-control' name='lname' placeholder='| Кучер' required/> <br>
            <input type='email' class='form-control' name='email' placeholder='| mail@ukr.net' required/> <br>
            <input type='password' class='form-control' name='password' placeholder='| Введіть пароль' required/> <br>
            <input type='password' class='form-control' name='r_password' placeholder='| Повторіть пароль'/><br/>
            <br>
            <input type='submit' class='btn btn-lg btn-primary bttn' name='submit' value='Зареєструвати'/>
        </form>
        <div class='move'>
            <h6>
                <pre>Реєстрація не є необхідною для проходження даного курсу.
Та для засвоєння курсу рекомендується зареєструватися.
Вам буде створено дошку успішності для журналювання успіху засвоєння матеріалу.
Тестування доступне і без реєстрації, проте тоді успішність не буде збережено
і при перезавантажені сторінки дані щодо проходження будуть стерті.
</pre>
            </h6>
        </div>
    </div>
</div>
</body>
</html>
