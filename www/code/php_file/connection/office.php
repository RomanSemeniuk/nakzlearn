<?php
session_start();
if (empty($_SESSION['email'])) {
    echo "<script>document.location.replace('/index.php');</script>";
}
/*-=--=-=-=-=-=-==-=-==-*/
include_once('../database/db.php');
include_once('../../../topMenu.ini.php');
$mail = $_SESSION["email"];
$query = mysqli_query($link, "SELECT * FROM tuser WHERE email='$mail'");
if ($query) {
    $user_data = $query->fetch_array(MYSQLI_BOTH);
}
$mas = "<p>" . "Інформація про користувача: <br/>" . "Email: " . $user_data['email'] . " Ім'я: " . $user_data['name'] .
    " Прізвище: " . $user_data['lname'] . "</p>";
/*<!--Кнопка виходу-->*/
if (isset($_POST['logout'])) {
    unset($_SESSION['email']);
    session_destroy();
    echo "Кабінет відсутній" .
        "<script>document.location.replace('login.php');</script>";
}
$query1 = mysqli_query($link, "SELECT * FROM trating WHERE email='$mail'");
$gData = array();
if ($query1) {
    while ($user_data1 = mysqli_fetch_assoc($query1)) {
        array_push($gData, $user_data1);
    }
}

echo "<script type=\"text/javascript\">";
echo "var xData = " . json_encode($gData);
echo "</script>";


?>
<!DOCTYPE html>
<html lang="ua">
<head>
<body>
<script src="/styles/js/cards.js"></script>
<link rel="stylesheet" href="/styles/css/bootstrap-responsive.css">
<link rel="stylesheet" href="/styles/css/main.css">
<link rel="stylesheet" href="/styles/css/cover.css">
<link rel="stylesheet" href="/styles/css/graphs.css">
<form method="post" action="office.php" class="form-signin" role="form">
    <p class="lead">
        <input type="submit" class="btn btn-lg btn-default" name="logout" value="Вихід"/>
    </p>
</form>
<script>
    var tests = [],
        temp = {};
    console.log(xData || "No data!");
    xData.forEach(function (item) {
        temp.id = item.TestName.charAt(item.TestName.length - 1);
        temp.multiplier = item.count * 3.0;
        tests.push(temp);
        temp = {};
    });
</script>

<div class="graph">
    <div class="g-scale-v">
        <ul class="graph-scale-vertical">
            <li>0%</li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li class="pass">70% Дост. рів.</li>
            <li></li>
            <li></li>
            <li>100%</li>
        </ul>
        <p class="g-scale-label">
        </p>
    </div>
    <div class="g-bargroup">
        <div class="g-bar-nest">
            <div class="g-bar"></div>
            <div class="g-bar"></div>
            <div class="g-bar"></div>
            <div class="g-bar"></div>
            <div class="g-bar"></div>
            <div class="g-bar"></div>
            <div class="g-bar"></div>
            <div class="g-bar"></div>
            <div class="g-bar"></div>
            <div class="g-bar"></div>
        </div>
    </div>
    <div class="g-scale-h">
        <ul class="graph-scale-horizontal">
            <li>Тест 1</li>
            <li>Тест 2</li>
            <li>Тест 3</li>
            <li>Тест 4</li>
            <li>Тест 5</li>
            <li>Тест 6</li>
            <li>Тест 7</li>
            <li>Тест 8</li>
            <li>Тест 9</li>
            <li>Тест 10</li>
        </ul>
        <p class="g-scale-label"></p>
    </div>
</div>

</body>
</html>
<script>

    tests.forEach(function (item) {
        var target = document.getElementsByClassName('g-bar-nest')[0];
        var curtarget = target.getElementsByClassName('g-bar')[item.id - 1];
        curtarget.style.height = item.multiplier + 'px';
    });

</script>