<?php
//Друк масиву
function print_arr($arr){
    echo '<pre>' . print_r($arr, true) . '</pre>';
}

//Отримання списку тестів
function get_tests(){
    global $db;
    $query = "SELECT * FROM test";
    $res = mysqli_query($db, $query);
    if (!$res) return false;
    $data = array();
    while($row = mysqli_fetch_assoc($res)){
        $data = $row;
    }
    return $data;
}

define('PAGE_PER_NO', 8); // number of results per page.
function getPagination($count)
{
    $paginationCount = floor($count / PAGE_PER_NO);
    $paginationModCount = $count % PAGE_PER_NO;
    if (!empty($paginationModCount)) {
        $paginationCount++;
    }
    return $paginationCount;
}

?>