<?php
session_start();
include('topMenu.ini.php');
?>
</<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="ua">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Nakslearn</title>
</head>
<body>
<div class="inner cover">
    <h1 class="cover-heading">
    </h1>

    <div class="lead">По-справжньому безпечною можна вважати лише систему,
        що виключена, замурована в бетонний корпус, замкнена в приміщенні
        зі свинцевими стінами й охороняється збройною вартою, але й у цьому
        випадку сумніви не залишають мене.

        -
        Юджин Х. Спаффорд
    </div>
    <div class="lead">
        <a href="/code/php_file/about.php" class="btn btn-lg btn-default">Дізнатися більше</a>
    </div>
</div>

<div class="mastfoot">
    <div class="inner">
        Copyright 2015, by <a href="http://lnkd.in/dgaEqmB">Roman Semenyuk</a>.
    </div>
</div>

</div>

</div>

</div>
</body>
</html>