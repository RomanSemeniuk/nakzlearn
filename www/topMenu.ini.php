<?php
session_start();
?>
<html lang="ru">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="styles/css/bootstrap-responsive.css" rel="stylesheet">
    <link rel="icon" href="/images/im.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="styles/css/main.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <link href="styles/css/cover.css" rel="stylesheet">
    <style id="holderjs-style" type="text/css"></style>
</head>

<body>

<div class="site-wrapper">

    <div class="site-wrapper-inner">

        <div class="cover-container">

            <div class="masthead clearfix">
                <div class="inner">
                    <h3 class="masthead-brand">Nakslearn</h3>
                    <ul class="nav masthead-nav"><br>
                        <li><a href="/index.php">Головна сторінка</a></li>
                        <li><a href="/learn_files/learn.php"><span class="glyphicon glyphicon-book"> Курс</span></a></li>

                        <li>
                            <a href="/code/php_file/connection/registration.php">
                                <?php
                                if (!isset($_SESSION["email"])) {
                                    echo 'Реєстрація';
                                } ?>
                            </a>
                        </li>
                        <li>
                            <a href="/code/php_file/connection/login.php">
                                <?php
                                if (!isset($_SESSION["email"])) {
                                    echo 'Вхід';
                                } ?>
                            </a>
                        </li>
                        <li>
                            <a href="/code/php_file/connection/office.php">
                                <?php
                                if (isset($_SESSION["email"])) {
                                    echo "Користувач: " . $_SESSION["email"];
                                } ?>
                            </a>
                        </li>
                    </ul>
                </div>
                <div id="foot"></div>
            </div>