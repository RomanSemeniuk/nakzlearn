<?php
/**
 * Created by PhpStorm.
 * User: Naks
 * Date: 11.10.2015
 * Time: 22:27
 */
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
    <TITLE></TITLE>
    <META NAME="GENERATOR" CONTENT="LibreOffice 4.1.6.2 (Linux)">
    <META NAME="AUTHOR" CONTENT="Naks ®">
    <META NAME="CREATED" CONTENT="20151011;143700000000000">
    <META NAME="CHANGEDBY" CONTENT="Naks ®">
    <META NAME="CHANGED" CONTENT="20151011;143700000000000">
    <META NAME="AppVersion" CONTENT="14.0000">
    <META NAME="Company" CONTENT="SPecialiST RePack">
    <META NAME="DocSecurity" CONTENT="0">
    <META NAME="HyperlinksChanged" CONTENT="false">
    <META NAME="LinksUpToDate" CONTENT="false">
    <META NAME="ScaleCrop" CONTENT="false">
    <META NAME="ShareDoc" CONTENT="false">
    <STYLE TYPE="text/css">
        <!--
        @page { margin-left: 0.98in; margin-right: 0.59in; margin-top: 0.59in; margin-bottom: 0.59in }
        P { margin-bottom: 0.08in; direction: ltr; line-height: 100%; widows: 2; orphans: 2 }
        P.western { font-family: "Times New Roman", serif; font-size: 10pt; so-language: ru-RU }
        P.cjk { font-family: "Times New Roman"; font-size: 10pt; so-language: ru-RU }
        P.ctl { font-family: "Times New Roman"; font-size: 10pt }
        H2 { margin-top: 0.08in; margin-bottom: 0.04in; direction: ltr; line-height: 100%; widows: 2; orphans: 2 }
        H2.western { font-family: "Peterburg", serif; font-size: 11pt; so-language: uk-UA }
        H2.cjk { font-family: "Times New Roman"; font-size: 11pt; so-language: ru-RU }
        H2.ctl { font-family: "Times New Roman"; font-size: 10pt; font-weight: normal }
        -->
    </STYLE>
</HEAD>
<BODY LANG="uk-UA" DIR="LTR">
<div id="wrapper">
    <scrip><?php include('../toplearn.ini.php');?></scrip>
    <div id="content">
        <H2 CLASS="western" ALIGN=CENTER><A NAME="_Toc365048745"></A><FONT FACE="Times New Roman, serif"><FONT SIZE=5 STYLE="font-size: 20pt">НД
                    ТЗІ 1.1-003-99 &quot;Термінологія в галузі
                    захисту інформації в комп’ютерних
                    системах від несанкціонованого доступу&quot;</FONT></FONT></H2>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">1. </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">Затверджено
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">наказом
Департаменту спеціальних телекомунікаційних
систем та захисту інформації Служби
безпеки України</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
від “ 28 ” </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">квітня
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">1999 р. № 22:</SPAN></FONT></P>
        <P LANG="en-US" ALIGN=JUSTIFY STYLE="margin-bottom: 0in; widows: 0; orphans: 0; page-break-after: auto">
            <FONT SIZE=3><FONT SIZE=4><SPAN LANG="uk-UA">А) Положення
про порядок здійснення криптографічного
захисту інформації в Україні;</SPAN></FONT></FONT></P>
        <P LANG="en-US" ALIGN=JUSTIFY STYLE="margin-bottom: 0in; widows: 0; orphans: 0; page-break-after: auto">
            <FONT SIZE=3><FONT SIZE=4><SPAN LANG="uk-UA">Б) Правила
забезпечення захисту інформації в
інформаційних, телекомунікаційних та
інформаційно-комунікаційних системах;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=4><SPAN LANG="uk-UA">В)
НД ТЗІ 1.1-003-99 Термінологія в галузі
захисту інформації в комп’ютерних
системах від несанкціонованого доступу;</SPAN></FONT></P>
        <P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=4><SPAN STYLE="font-style: normal"><SPAN STYLE="font-weight: normal">Г)
НД ТЗІ 2.5-004-99 Критерії оцінки захищеності
інформації в комп’ютерних системах
від несанкціонованого доступу.</SPAN></SPAN></FONT></P>
        <P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; font-style: normal; font-weight: normal; line-height: 100%">
            <BR>
        </P>
        <P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=4><I><B>2.
                        Згідно із </B></I><SPAN STYLE="font-style: normal"><SPAN STYLE="font-weight: normal">НД
ТЗІ 1.1-003-99 Термінологія в галузі захисту
інформації в комп’ютерних системах
від несанкціонованого доступу</SPAN></SPAN><B><I>
                        поняття «обчислювальна система» – це:</I></B></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">А) сукупність програмних
та апаратних засобів, призначених для
обробки інформації;</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">Б) організаційно-технічна
система, що реалізує інформаційну
технологію і об’єднує операційні
системи, фізичне середовище, персонал
і інформацію, яка обробляється;</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">В) сукупність
програмно-апаратних засобів, яка подана
для оцінки.</SPAN></FONT></P>
        <P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=4><I><B>3.
                        Згідно із </B></I><SPAN STYLE="font-style: normal"><SPAN STYLE="font-weight: normal">НД
ТЗІ 1.1-003-99 Термінологія в галузі захисту
інформації в комп’ютерних системах
від несанкціонованого доступу</SPAN></SPAN><B><I>
                        поняття «автоматизована система» –
                        це:</I></B></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">А) сукупність програмних
та апаратних засобів, призначених для
обробки інформації;<BR>Б) організаційно-технічна
система, що реалізує інформаційну
технологію і об’єднує операційні
системи, фізичне середовище, персонал
і інформацію, яка обробляється;</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">В) сукупність
програмно-апаратних засобів, яка подана
для оцінки.</SPAN></FONT></P>
        <P CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
        </P>
        <P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=4><I><B>4.
                        Згідно із </B></I><SPAN STYLE="font-style: normal"><SPAN STYLE="font-weight: normal">НД
ТЗІ 1.1-003-99 Термінологія в галузі захисту
інформації в комп’ютерних системах
від несанкціонованого доступу</SPAN></SPAN><B><I>
                        поняття «комп’ютерна система» – це:</I></B></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">А) сукупність програмних
та апаратних засобів, призначених для
обробки інформації;</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">Б) організаційно-технічна
система, що реалізує інформаційну
технологію і об’єднує операційні
системи, фізичне середовище, персонал
і інформацію, яка обробляється;</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">В) сукупність
програмно-апаратних засобів, яка подана
для оцінки.</SPAN></FONT></P>
        <P CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
        </P>
        <P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=4><I><B>5.
                        Згідно із </B></I><SPAN STYLE="font-style: normal"><SPAN STYLE="font-weight: normal">НД
ТЗІ 1.1-003-99 Термінологія в галузі захисту
інформації в комп’ютерних системах
від несанкціонованого доступу</SPAN></SPAN><B><I>
                        поняття «об’єкт комп’ютерної системи»
                        – це:</I></B></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">А) виконувана в даний
момент програма, яка повністю
характеризується своїм контекстом
(поточним станом регістрів обчислювальної
системи, адресним простором, повноваженнями
тощо);</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">Б) елемент ресурсу
комп’ютерної системи, що знаходиться
під керуванням КЗС(комплекс засобів
захисту) і характеризується певними
атрибутами і поводженням;</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">В) подання фізичного
користувача в комп’ютерну систему, що
створюється в процесі входження
користувача в систему і повністю
характеризується своїм контекстом
(псевдонімом, ідентифікаційним кодом,
повноваженнями тощо).</SPAN></FONT></P>
        <P CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
        </P>
        <P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=4><I><B>6.
                        Згідно із </B></I><SPAN STYLE="font-style: normal"><SPAN STYLE="font-weight: normal">НД
ТЗІ 1.1-003-99 Термінологія в галузі захисту
інформації в комп’ютерних системах
від несанкціонованого доступу</SPAN></SPAN><B><I>
                        поняття «об’єкт-процес» – це:</I></B></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">А) виконувана в даний
момент програма, яка повністю
характеризується своїм контекстом
(поточним станом регістрів обчислювальної
системи, адресним простором, повноваженнями
тощо);</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">Б) елемент ресурсу
комп’ютерної системи, що знаходиться
під керуванням КЗС(комплекс засобів
захисту) і характеризується певними
атрибутами і поводженням;</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">В) подання фізичного
користувача в комп’ютерну систему, що
створюється в процесі входження
користувача в систему і повністю
характеризується своїм контекстом
(псевдонімом, ідентифікаційним кодом,
повноваженнями і т. ін.).</SPAN></FONT></P>
        <P CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><BR>
        </P>
        <P ALIGN=JUSTIFY STYLE="margin-bottom: 0in; line-height: 100%"><FONT SIZE=4><I><B>7.
                        Згідно із </B></I><SPAN STYLE="font-style: normal"><SPAN STYLE="font-weight: normal">НД
ТЗІ 1.1-003-99 Термінологія в галузі захисту
інформації в комп’ютерних системах
від несанкціонованого доступу</SPAN></SPAN><B><I>
                        поняття «об’єкт-користувач» – це:</I></B></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">А) виконувана в даний
момент програма, яка повністю
характеризується своїм контекстом
(поточним станом регістрів обчислювальної
системи, адресним простором, повноваженнями
тощо);</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">Б) елемент ресурсу
комп’ютерної системи, що знаходиться
під керуванням КЗС(комплекс засобів
захисту) і характеризується певними
атрибутами і поводженням;</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" STYLE="margin-bottom: 0in"><FONT SIZE=4><SPAN LANG="uk-UA">В)
подання фізичного користувача в
комп’ютерну систему, що створюється в
процесі входження користувача в систему
і повністю характеризується своїм
контекстом (псевдонімом, ідентифікаційним
кодом, повноваженнями і т. ін.).</SPAN></FONT></P>
    </div></div>
</BODY>
</HTML>
