SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS `people` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE people;

CREATE TABLE IF NOT EXISTS `tcourse` (
  `id` int(11) NOT NULL,
  `cname` varchar(250) NOT NULL) AUTO_INCREMENT=48;

INSERT INTO `tcourse` (`id`, `cname`) VALUES
(1, 'zahistInf'),
(2, 'test1'),
(3, 'zahistInf'),
(4, 'test2');

CREATE TABLE IF NOT EXISTS `trating` (
  `id` int(11) NOT NULL,
  `TestName` text NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0',
  `email` text NOT NULL
) AUTO_INCREMENT=58;

INSERT INTO `trating` (`id`, `TestName`, `count`, `email`) VALUES
(48, 'Inf Def 1', 7, 'igo@qapint.com'),
(53, 'Inf Def 3', 15, 'igo@qapint.com'),
(52, 'Inf Def 2', 10, 'igo@qapint.com'),
(54, 'Inf Def 1', 0, ''),
(55, 'Inf Def 1', 0, ''),
(56, 'Inf Def 1', 7, ''),
(57, 'Inf Def 1', 14, '');

CREATE TABLE IF NOT EXISTS `tuser` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `lname` varchar(250) NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL
)  AUTO_INCREMENT=56;

INSERT INTO `tuser` (`id`, `name`, `lname`, `email`, `password`) VALUES
(55, 'igo', 'igo', 'igo@qapint.com', '202cb962ac59075b964b07152d234b70');


ALTER TABLE `tcourse`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `trating`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `tuser`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `tcourse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=48;
ALTER TABLE `trating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=58;
ALTER TABLE `tuser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
