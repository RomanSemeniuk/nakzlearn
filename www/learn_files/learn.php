<?php
include('../topMenu.ini.php');
/**if ($_SESSION['email']) {
    echo "connection<script>document.location.replace('/code/php_file/connection/login.php');</script>";
    exit;
}*/
if (isset($_REQUEST[session_name()])) session_start();
?>
<!DOCTYPE html>
<html lang="ua">
<body>
<script src="/styles/js/cards.js"></script>
<div class="">
    <!--  <div class="right-info">
          <input type="checkbox" id="hd-1" class="hide"/>
          <label for="hd-1" ><pre>Інформація про курс</pre></label>
          <div class="right-info">


      В даному курсі матеріал подається в теоритичній формі.
      Весь курс розбито на блоки.
      Після ознайомлення із певним блоком теорії,
      вам буде запропоновано пройти тестування.
      Тестування, допоможе закріпити навички
      в оволодінню теорією та
      зрозуміти рівень засвоєння матеріалу.
      В даному курсі проходження тестування не є обов'язковим.
              </div>
  </div>
  <!--  -->
    <script type="text/javascript" src="/styles/js/jquery.js"></script>
    <style type="text/css">
        @import url(/styles/css/css.css);
    </style>
    <script type="text/javascript" src="/styles/js/loader.js"></script>
    <!--<div class="cards-wrapper">-->
    <div id="wrapper">
        <ul id="nav">
            <li><a href="/learn_files/lecture/z1.php"><span class="glyphicon glyphicon-file"></span></a></li>

            <li><a href="/learn_files/test/tst1.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z2.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst2.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z3.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst3.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z4.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst4.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z5.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst5.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z6.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst6.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z7.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst7.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z8.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst8.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z9.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst9.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z10.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst10.php"><span class="glyphicon glyphicon-check"></a></li>

        </ul>
        <div id="content">
            <h2>Вітаю!</h2>

            <p>Кілька інструкцій</p>

            <p>Зверху розміщено іконки, кожна з яких містить один вид завдання:<br>
                <span class="glyphicon glyphicon-file">(файл) - це теоритичний блок,
                <span class="glyphicon glyphicon-check">(тетстування) - блок призначений для перевірки завсоєння матеріалу.
            </p>

            <p>Здійснюйте перехід послідовно натискаючи на них. За бажанням тестувальні блоки можна пропустити, та це
                погіршить засвоєння матеріалу
                та графік успішності, що знаходиться в особистому кабінеті буде не заповнено</p>

            <p>Бажаю вам плідної праці</p>
        </div>
        <div id="foot">Copyright 2015, by <a href="http://lnkd.in/dgaEqmB">Roman Semenyuk</a>.</div>
    </div>
</div>
</div>
</div>
</body>
</html>
