<!--#include virtual="/topMenu.php" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
    <TITLE></TITLE>
    <META NAME="GENERATOR" CONTENT="LibreOffice 4.1.6.2 (Linux)">
    <META NAME="AUTHOR" CONTENT="Naks ®">
    <META NAME="CREATED" CONTENT="20151011;142700000000000">
    <META NAME="CHANGEDBY" CONTENT="Naks ®">
    <META NAME="CHANGED" CONTENT="20151011;142700000000000">
    <META NAME="AppVersion" CONTENT="14.0000">
    <META NAME="Company" CONTENT="SPecialiST RePack">
    <META NAME="DocSecurity" CONTENT="0">
    <META NAME="HyperlinksChanged" CONTENT="false">
    <META NAME="LinksUpToDate" CONTENT="false">
    <META NAME="ScaleCrop" CONTENT="false">
    <META NAME="ShareDoc" CONTENT="false">
    <STYLE TYPE="text/css">
        <!--
        @page { margin-left: 0.98in; margin-right: 0.59in; margin-top: 0.59in; margin-bottom: 0.59in }
        P { text-indent: 0.3in; margin-bottom: 0in; direction: ltr; line-height: 100%; text-align: justify; widows: 2; orphans: 2 }
        P.western { font-family: "Times New Roman", serif; font-size: 12pt; so-language: ru-RU }
        P.cjk { font-family: "Times New Roman"; font-size: 12pt; so-language: ru-RU }
        P.ctl { font-family: "Times New Roman"; font-size: 10pt }
        A:link { color: #0000ff; so-language: zxx }
        -->
    </STYLE>
</HEAD>
<BODY LANG="uk-UA" LINK="#0000ff" DIR="LTR">
<div id="wrapper">
    <scrip><?php include('../toplearn.ini.php');?></scrip>
    <div id="content">
        <P LANG="ru-RU" CLASS="western" ALIGN=CENTER STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>ЗАКОН УКРАЇНИ
                            “ПРО ІНФОРМАЦІЮ”</B></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Як вже відзначалось
раніше, Закон України “Про інформацію”
був прийнятий Верховною Радою України
і введений в дію 2 жовтня 1992 р. Закон
закріплює право громадян України на
інформацію, закладає правові основи
інформаційної діяльності.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Ґрунтуючись
на Декларації про державний суверенітет
України та Акті проголошення її
незалежності, Закон стверджує інформаційний
суверенітет України і визначає правові
форми міжнародного співробітництва в
галузі інформації. Закон визначив, що
під інформацією розуміються документовані
або публічно оголошені відомості про
події та явища, що відбуваються у
суспільстві, державі та навколишньому
природному середовищі.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Закон встановив
загальні правові основи одержання,
використання, поширення та зберігання
інформації, закріпив право особи на
інформацію в усіх сферах суспільного
і державного життя України, а також
систему інформації, її джерела, визначив
статус учасників інформаційних відносин,
частково врегулював доступ до інформації
та забезпечення її охорони, закріпив
вимоги, що захищають особу та суспільство
від неправдивої інформації.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Дія Закону
поширюється на інформаційні відносини,
які виникають у всіх сферах життя і
діяльності суспільства і держави при
одержанні, використанні, поширенні та
зберіганні інформації. Законодавство
України про інформацію міститься у
Конституції України, це Закон, законодавчі
акти про окремі галузі, види, форми і
засоби інформації, міжнародні договори
та угоди ратифіковані Україною, та
принципи і норми міжнародного права. З
позицій організаційно-правового
забезпечення захисту інформації
розглянемо, передовсім статті, які в
подальшому будуть необхідні у вирішенні
інших проблем інформаційної безпеки.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>1. Основні
                                принципи інформаційних відносин</B></I></SPAN></FONT></FONT></P>
        <UL>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">гарантованість
	права на інформацію;</SPAN></FONT></FONT></P>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">відкритість,
	доступність інформації та свобода її
	обміну;</SPAN></FONT></FONT></P>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">об'єктивність,
	вірогідність інформації;</SPAN></FONT></FONT></P>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">повнота і
	точність інформації;</SPAN></FONT></FONT></P>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">законність
	одержання, використання, поширення та
	зберігання інформації.</SPAN></FONT></FONT></P>
        </UL>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>2. Державна
                                інформаційна політика</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Державна
інформаційна політика – це сукупність
основних напрямів і методів діяльності
держави по одержанню, використанню,
поширенню та зберіганню інформації.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Головними
напрямами і методами державної
інформаційної політики є:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- забезпечення
доступу громадян до інформації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- створення
національних систем і мереж інформації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- зміцнення
матеріально-технічних, фінансових,
організаційних, правових і наукових
основ інформаційної діяльності;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- забезпечення
ефективного використання інформації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- сприяння
постійному оновленню, збагаченню та
зберіганню національних інформаційних
ресурсів;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- створення
загальної системи охорони інформації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.37in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- сприяння
міжнародному співробітництву в галузі
інформації і гарантування інформаційного
суверенітету України.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Державну
інформаційну політику розробляють і
здійснюють органи державної влади
загальної компетенції, а також відповідні
органи спеціальної компетенції.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Суб'єктами
інформаційних відносин є громадяни
України, юридичні особи, держава.
Суб'єктами інформаційних відносин, крім
цього, можуть бути також інші держави,
їх громадяни та юридичні особи, міжнародні
організації та особи без громадянства.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Об'єктами
інформаційних відносин є документована
або публічно оголошувана інформація
про події та явища в галузі політики,
економіки, культури, а також у соціальній,
екологічній, міжнародній та інших
сферах.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>3. Право на
                                інформацію</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Всі громадяни
України, юридичні особи і державні
органи мають право на інформацію, що
передбачає можливість вільного одержання,
використання, поширення та зберігання
відомостей, необхідних їм для реалізації
ними своїх прав, свобод і законних
інтересів, здійснення завдань і функцій.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Реалізація
права на інформацію громадянами,
юридичними особами і державою не повинна
порушувати громадські, політичні,
економічні, соціальні, духовні, екологічні
та інші права, свободи і законні інтереси
інших громадян, права та інтереси
юридичних осіб.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Кожному
громадянину забезпечується вільний
доступ до інформації, яка стосується
його особисто, крім випадків, передбачених
законами України.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>4. Гарантії
                                права на інформацію</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Право на
інформацію забезпечується:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- обов'язком
органів державної влади, а також органів
місцевого і регіонального самоврядування
інформувати про свою діяльність та
прийняті рішення;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- створенням у
державних органах спеціальних
інформаційних служб або систем, що
забезпечували б у встановленому порядку
доступ до інформації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- вільним
доступом суб'єктів інформаційних
відносин до статистичних даних, архівних,
бібліотечних і музейних фондів;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- обмеження
цього доступу зумовлюються лише
специфікою цінностей та особливими
умовами їх зберігання, що визначаються
законодавством;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- створенням
механізму здійснення права на інформацію;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- здійсненням
державного контролю за додержанням
законодавства про інформацію;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- встановленням
відповідальності за порушення
законодавства про інформацію.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>5. Основні
                                види інформаційної діяльності</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.37in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Основними
видами інформаційної діяльності є
одержання, використання, поширення та
зберігання інформації.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.37in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Одержання
інформації – це набуття, придбання,
накопичення відповідно до чинного
законодавства України документованої
або публічно оголошуваної інформації
громадянами, юридичними особами або
державою. Використання інформації –
це задоволення інформаційних потреб
громадян, юридичних осіб і держави.
Поширення інформації – це розповсюдження,
оприлюднення, реалізація у встановленому
законом порядку документованої або
публічно оголошуваної інформації.
Зберігання інформації – це забезпечення
належного стану інформації та її
матеріальних носіїв. Одержання,
використання, поширення та зберігання
документованої або публічно оголошуваної
інформації здійснюється у порядку,
передбаченому цим Законом та іншими
законодавчими актами в галузі інформації.
</SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>6. Галузі
                                інформації</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Галузі інформації
– це сукупність документованих або
публічно оголошених відомостей про
відносно самостійні сфери життя і
діяльності суспільства та держави.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Основними
галузями інформації є: політична,
економічна, духовна, науково-технічна,
соціальна, екологічна, міжнародна.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>7. Види
                                інформації</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Основними
видами інформації є статистична
інформація, масова інформація, інформація
про діяльність державних органів влади
та органів місцевого і регіонального
самоврядування, правова інформація;
інформація про особу, інформація
довідково-енциклопедичного характеру,
соціологічна інформація.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>8. Джерела
                                інформації</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Джерелами
інформації є передбачені або встановлені
Законом носії інформації: документи та
інші носії інформації, що являють собою
матеріальні об'єкти, що зберігають
інформацію, а також повідомлення засобів
масової інформації, публічні виступи.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>9. Режим
                                доступу до інформації</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Режим доступу
до інформації – це передбачений правовими
нормами порядок одержання, використання,
поширення і зберігання інформації.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">За режимом
доступу інформація поділяється на
відкриту інформацію та інформацію з
обмеженим доступом. Держава здійснює
контроль за режимом доступу до інформації.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Завдання
контролю за режимом доступу до інформації
полягає у забезпеченні додержання вимог
законодавства про інформацію всіма
державними органами, підприємствами,
установами та організаціями, недопущенні
необґрунтованого віднесення відомостей
до категорії інформації з обмеженим
доступом.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Державний
контроль за додержанням встановленого
режиму здійснюється спеціальними
органами, які визначають Верховна Рада
України і Кабінет Міністрів України.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.37in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">У порядку
контролю Верховна Рада України може
вимагати від урядових установ, міністерств,
відомств звіти, що містять відомості
про їх діяльність із забезпечення
інформацією зацікавлених осіб (кількість
випадків відмови у наданні доступу до
інформації із зазначенням мотивів таких
відмов), а також кількість та обґрунтування
застосування режиму обмеженого доступу
до окремих видів інформації; кількість
скарг на неправомірні дії посадових
осіб, які відмовили у доступі до
інформації, та вжиті щодо них заходи
тощо).</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>10. Інформація
                                з обмеженим доступом</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Інформація з
обмеженим доступом за своїм правовим
режимом поділяється на конфіденційну
і таємну.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.37in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Конфіденційна
інформація – це відомості, які знаходяться
у володінні, користуванні або розпорядженні
окремих фізичних або юридичних осіб і
поширюються за їх бажанням відповідно
до передбачених ними умов.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Громадяни,
юридичні особи, які володіють інформацією
професійного, ділового, виробничого,
банківського, комерційного та іншого
характеру, одержаною на власні кошти,
або такою, яка є предметом їх професійного,
ділового, виробничого, банківського,
комерційного та іншого інтересу і не
порушує передбаченої законом таємниці,
самостійно визначають режим доступу
до неї, включаючи належність її до
категорії конфіденційної, та встановлюють
для неї систему (способи) захисту. Виняток
становить інформація комерційного та
банківського характеру, а також
інформація, правовий режим якої
встановлено Верховною Радою України
за поданням Кабінету Міністрів України
(з питань статистики, екології, банківських
операцій, податків тощо), та інформація,
приховування якої загрожує життю і
здоров'ю людей.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">До таємної
інформації належить інформація, що
містить відомості, які становлять
державну та іншу передбачену законом
таємницю, розголошення якої завдає
шкоди особі, суспільству і державі.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Віднесення
інформації до категорії таємних
відомостей, які становлять державну
таємницю, і доступ до неї громадян
здійснюється відповідно до закону про
цю інформацію.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Порядок обігу
таємної інформації та її захисту
визначається відповідними державними
органами за умови додержання вимог,
встановлених цим Законом. Порядок і
терміни оприлюднення таємної інформації
визначаються відповідним законом.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>11. Документи
                                та інформація, що не підлягають наданню
                                для ознайомлення за запитами</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Не підлягають
обов'язковому наданню для ознайомлення
за інформаційними запитами офіційні
документи, які містять у собі:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- інформацію,
визнану у встановленому порядку державною
таємницею;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- конфіденційну
інформацію;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- інформацію
про оперативну і слідчу роботу органів
прокуратури, МВС, СБУ, роботу органів
дізнання та суду у тих випадках, коли
її розголошення може зашкодити оперативним
заходам, розслідуванню чи дізнанню,
порушити право людини на справедливий
та об'єктивний судовий розгляд її справи,
створити загрозу життю або здоров'ю
будь-якої особи;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- інформацію,
що стосується особистого життя громадян;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- документи, що
становлять внутрівідомчу службову
кореспонденцію (доповідні записки,
переписування між підрозділами та
інше), якщо вони пов'язані з розробкою
напряму діяльності установи, процесом
прийняття рішень і передують їх прийняттю;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- інформацію,
що не підлягає розголошенню згідно з
іншими законодавчими або нормативними
актами. Установа, до якої звернено запит,
може не надавати для ознайомлення
документ, якщо він містить інформацію,
яка не підлягає розголошенню на підставі
нормативного акту іншої державної
установи, а та державна установа, яка
розглядає запит, не має права вирішувати
питання щодо її розсекречення;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- інформацію
фінансових установ, підготовлену для
контрольно-фінансових відомств.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>12. Право
                                власності на інформацію</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Право власності
на інформацію – це врегульовані законом
суспільні відносини щодо володіння,
користування і розпорядження інформацією.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Інформація є
об'єктом права власності громадян,
організацій (юридичних осіб) і держави.
Інформація може бути об'єктом права
власності як у повному обсязі, так і
об'єктом лише володіння, користування
чи розпорядження. Власник інформації
щодо об'єктів своєї власності має право
здійснювати будь-які законні дії.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Підставами
виникнення права власності на інформацію
є:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- створення
інформації своїми силами і за свій
рахунок;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- договір на
створення інформації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- договір, що
містить умови переходу права власності
на інформацію до іншої особи.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Інформація,
створена кількома громадянами або
юридичними особами, є колективною
власністю її творців. Порядок і правила
користування такою власністю визначаються
договором, укладеним між співвласниками.
Інформація, створена організаціями
(юридичними особами) або придбана ними
іншим законним способом, є власністю
цих організацій. Інформація, створена
на кошти державного бюджету, є державною
власністю. Інформацію, створену на
правах індивідуальної власності, може
бути віднесено до державної власності
у випадках передачі її на зберігання у
відповідні банки даних, фонди або архіви
на договірній основі. Власник інформації
має право призначати особу, яка здійснює
володіння, використання і розпорядження
інформацією, і визначати правила обробки
інформації та доступ до неї, а також
встановлювати інші умови щодо інформації.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>13. Інформація
                                як товар</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Інформаційна
продукція та інформаційні послуги
громадян та юридичних осіб, які займаються
інформаційною діяльністю, можуть бути
об'єктами товарних відносин, що регулюються
чинним цивільним та іншим законодавством.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Ціни і
ціноутворення на інформаційну продукцію
та інформаційні послуги встановлюються
договорами, за винятком випадків,
передбачених Законом.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>14. Інформаційна
                                продукція</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Інформаційна
продукція – це матеріалізований
результат інформаційної діяльності,
призначений для задоволення інформаційних
потреб громадян, державних органів,
підприємств, установ і організацій.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>15. Інформаційна
                                послуга</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.37in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Інформаційна
послуга – це здійснення у визначеній
законом формі інформаційної діяльності
по доведенню інформаційної продукції
до споживачів з метою задоволення їх
інформаційних потреб.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>16. Учасники
                                інформаційних відносин</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Учасниками
інформаційних відносин є громадяни,
юридичні особи або держава, які набувають
передбачених законом прав і обов'язків
у процесі інформаційної діяльності.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Основними
учасниками цих відносин є автори,
споживачі, поширювачі, зберігачі
(охоронці) інформації.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>17. Охорона
                                права на інформацію</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Право на
інформацію охороняється законом. Держава
гарантує всім учасникам інформаційних
відносин рівні права і можливості
доступу до інформації.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Ніхто не може
обмежувати права особи у виборі форм і
джерел одержання інформації, за винятком
випадків, передбачених законом. Суб'єкт
права на інформацію може вимагати
усунення будь-яких порушень його права.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Забороняється
вилучення друкованих видань, експонатів,
інформаційних банків, документів із
архівних, бібліотечних, музейних фондів
та знищення їх з ідеологічних чи
політичних міркувань.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>18.
                                Неприпустимість зловживання правом на
                                інформацію</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Інформація не
може бути використана для закликів до
повалення конституційного ладу, порушення
територіальної цілісності України,
пропаганди війни, насильства, жорстокості,
розпалювання расової, національної,
релігійної ворожнечі, зазіхання на
права і свободи людини.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Не підлягають
розголошенню відомості, що становлять
державну або іншу передбачену
законодавством таємницю. Не підлягають
розголошенню також відомості, що
стосуються лікарської таємниці, грошових
вкладів, прибутків від підприємницької
діяльності, всиновлення (удочеріння),
листування, телефонних розмов і
телеграфних повідомлень, крім випадків,
передбачених законом.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>19.
                                Відповідальність за порушення
                                законодавства про інформацію</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Порушення
законодавства України про інформацію
тягне за собою дисциплінарну,
цивільно-правову, адміністративну або
кримінальну відповідальність згідно
з законодавством України.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Відповідальність
за порушення законодавства про інформацію
несуть особи, винні у вчиненні таких
порушень, як:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- необґрунтована
відмова від надання відповідної
інформації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- надання
інформації, що не відповідає дійсності;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- несвоєчасне
надання інформації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- навмисне
приховування інформації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- примушення
до поширення або перешкоджання поширенню
чи безпідставна відмова від поширення
певної інформації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- поширення
відомостей, що не відповідають дійсності,
ганьблять честь і гідність особи;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.37in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- використання
і поширення інформації стосовно
особистого життя громадянина без його
згоди особою, яка є власником відповідної
інформації внаслідок виконання своїх
службових обов'язків;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- розголошення
державної або іншої таємниці, що
охороняється законом, особою, яка повинна
охороняти цю таємницю;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- порушення
порядку зберігання інформації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- навмисне
знищення інформації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- необґрунтоване
віднесення окремих видів інформації
до категорії відомостей з обмеженим
доступом.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>20. Відшкодування
                                матеріальної та моральної шкоди</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">У випадках,
коли правопорушення завдають громадянам,
підприємствам, установам, організаціям
та державним органам матеріальної або
моральної шкоди, особи, винні в цьому,
відшкодовують її на підставі рішення
суду. Розмір відшкодування визначається
судом.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>21. Інформаційний
                                суверенітет</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Основою
інформаційного суверенітету України
є національні інформаційні ресурси.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">До інформаційних
ресурсів України входить вся належна
їй інформація, незалежно від змісту,
форм, часу і місця створення.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Україна
самостійно формує інформаційні ресурси
на своїй території і вільно розпоряджається
ними, за винятком випадків, передбачених
законами і міжнародними договорами.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>22. Гарантії
                                інформаційного суверенітету України</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Інформаційний
суверенітет України забезпечується:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- виключним
правом власності України на інформаційні
ресурси, що формуються за рахунок коштів
державного бюджету;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- створенням
національних систем інформації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.38in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">- встановленням
режиму доступу інших держав до
інформаційних ресурсів України;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=LEFT STYLE="text-indent: 0in"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">-
використанням інформаційних ресурсів
на основі рівноправного співробітництва
з іншими державами.</SPAN></FONT></FONT></P>
        <P CLASS="western" ALIGN=LEFT STYLE="text-indent: 0in"><BR>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=LEFT STYLE="text-indent: 0in"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>2.
                            Загальна характеристика організаційних
                            методів захисту інформації в КС</B></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Закони та
нормативні акти здійснюються тільки в
тому випадку, якщо вони підкріплюються
організаторською діяльністю відповідних
структур, створюваних у державі, у
відомствах, установах і організаціях.
При розгляді питань безпеки інформації
така діяльність відноситься до
організаційних методів захисту
інформації.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Організаційні
методи захисту інформації включають
заходи, заходи і дії, які повинні
здійснювати посадові особи в процесі
створення і експлуатації КС для
забезпечення заданого рівня безпеки
інформації.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Організаційні
методи захисту інформації тісно пов'язані
з правовим регулюванням у галузі безпеки
інформації. У відповідності до законів
і нормативних актів в міністерствах,
відомствах, на підприємствах (незалежно
від форм власності) для захисту інформації
створюються спеціальні служби безпеки
(на практиці вони можуть називатися і
інакше). Ці служби підпорядковуються,
як правило, керівництву установи.
Керівники служб організовують створення
і функціонування систем захисту
інформації. На організаційному рівні
вирішуються наступні завдання забезпечення
безпеки інформації в КС:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">організація
робіт з розробки системи захисту
інформації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">обмеження
доступу на об'єкт і до ресурсів КС;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">розмежування
доступу до ресурсів КС;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">планування
заходів;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">розробка
документації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">виховання
та навчання обслуговуючого персоналу
і користувачів;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">сертифікація
засобів захисту інформації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">ліцензування
діяльності по захисту інформації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">атестація
об'єктів захисту;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">вдосконалення
системи захисту інформації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">оцінка
ефективності функціонування системи
захисту інформації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">контроль
виконання встановлених правил роботи
в КС. </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=LEFT STYLE="text-indent: 0in"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Організаційні
методи є стрижнем комплексної системи
захисту інформації в КС. Тільки за
допомогою цих методів можливе об'єднання
на правовій основі технічних, програмних
і криптографічних засобів захисту
інформації в єдину комплексну систему.
Конкретні організаційні методи захисту
інформації будуть приводитися при
розгляді блокування загроз безпеки
інформації. Найбільшу увагу організаційним
заходам приділяється при викладі питань
побудови та організації функціонування
комплексної системи захисту інформації.</SPAN></FONT></FONT></P>
    </div></div>
</BODY>
</HTML>
