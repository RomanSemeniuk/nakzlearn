<!--#include virtual="/topMenu.php" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
    <TITLE></TITLE>
    <META NAME="GENERATOR" CONTENT="LibreOffice 4.1.6.2 (Linux)">
    <META NAME="AUTHOR" CONTENT="Naks ®">
    <META NAME="CREATED" CONTENT="20151011;142900000000000">
    <META NAME="CHANGEDBY" CONTENT="Naks ®">
    <META NAME="CHANGED" CONTENT="20151011;143000000000000">
    <META NAME="AppVersion" CONTENT="14.0000">
    <META NAME="Company" CONTENT="SPecialiST RePack">
    <META NAME="DocSecurity" CONTENT="0">
    <META NAME="HyperlinksChanged" CONTENT="false">
    <META NAME="LinksUpToDate" CONTENT="false">
    <META NAME="ScaleCrop" CONTENT="false">
    <META NAME="ShareDoc" CONTENT="false">
    <STYLE TYPE="text/css">
        <!--
        @page { margin-left: 0.98in; margin-right: 0.59in; margin-top: 0.59in; margin-bottom: 0.59in }
        P { text-indent: 0.3in; margin-bottom: 0in; direction: ltr; line-height: 100%; text-align: justify; widows: 2; orphans: 2 }
        P.western { font-family: "Times New Roman", serif; font-size: 12pt; so-language: ru-RU }
        P.cjk { font-family: "Times New Roman"; font-size: 12pt; so-language: ru-RU }
        P.ctl { font-family: "Times New Roman"; font-size: 10pt }
        H2 { margin-top: 0.08in; margin-bottom: 0.04in; direction: ltr; line-height: 100%; widows: 2; orphans: 2 }
        H2.western { font-family: "Peterburg", serif; font-size: 11pt; so-language: uk-UA }
        H2.cjk { font-family: "Times New Roman"; font-size: 11pt; so-language: ru-RU }
        H2.ctl { font-family: "Times New Roman"; font-size: 10pt; font-weight: normal }
        A:link { color: #0000ff; so-language: zxx }
        -->
    </STYLE>
</HEAD>
<BODY LANG="uk-UA" LINK="#0000ff" DIR="LTR">
<div id="wrapper">
    <scrip><?php include('../toplearn.ini.php');?></scrip>
    <div id="content">
        <H2 CLASS="western" ALIGN=CENTER><A NAME="_Toc365048723"></A><FONT SIZE=5 STYLE="font-size: 20pt">Криптографічні
                методи захисту інформації</FONT></H2>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><I><B>Мета</B></I></FONT><FONT SIZE=4>:
                </FONT><FONT SIZE=4><SPAN LANG="uk-UA">ознайомити студентів
із основними поняттями криптогафічних
методів захисту інформації</SPAN></FONT><FONT SIZE=4>.</FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><I><B>Професійна</B></I></FONT><FONT SIZE=4>
                </FONT><FONT SIZE=4><I><B>спрямованість</B></I></FONT><FONT SIZE=4>:
                    дана лекція є складовою частиною
                    професійної підготовки вчителя
                    інформатики до </FONT><FONT SIZE=4><SPAN LANG="uk-UA">майбутньої
професійної діяльності</SPAN></FONT><FONT SIZE=4>.</FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><I><B>Основні</B></I></FONT><FONT SIZE=4>
                </FONT><FONT SIZE=4><I><B>поняття</B></I></FONT><FONT SIZE=4>:
                </FONT><FONT SIZE=4><SPAN LANG="uk-UA">інформація, захист
інформації, шифрування, криптологія,
криптографія, криптоаналіз, ключ,
криптосистема</SPAN></FONT><FONT SIZE=4>.</FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><I><B>План</B></I></FONT><FONT SIZE=4>
                </FONT><FONT SIZE=4><I><B>лекції</B></I></FONT><FONT SIZE=4>:</FONT></FONT></P>
        <OL>
            <LI><P LANG="ru-RU" STYLE="text-indent: 0in; line-height: 100%; widows: 2; orphans: 2">
                    <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt"><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-style: normal"><SPAN STYLE="font-weight: normal">Класифікація
	методів криптографічного перетворення
	інформації</SPAN></SPAN></SPAN></FONT></FONT></FONT></FONT></P>
        </OL>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-weight: normal">2.
Шифрування. Основні поняття</SPAN></SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><I><B>Обрані</B></I></FONT><FONT SIZE=4>
                </FONT><FONT SIZE=4><I><B>методи</B></I></FONT><FONT SIZE=4>:
                    лекція-бесіда.</FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><I><B>Наочність</B></I></FONT><FONT SIZE=4>:
                    схематичн</FONT><FONT SIZE=4><SPAN LANG="uk-UA">і
</SPAN></FONT><FONT SIZE=4>зображення. </FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><I><B>Питання</B></I></FONT><FONT SIZE=4>
                </FONT><FONT SIZE=4><I><B>по</B></I></FONT><FONT SIZE=4> </FONT><FONT SIZE=4><I><B>темі</B></I></FONT><FONT SIZE=4>
                </FONT><FONT SIZE=4><I><B>для</B></I></FONT><FONT SIZE=4>
                </FONT><FONT SIZE=4><I><B>самостійного</B></I></FONT><FONT SIZE=4>
                </FONT><FONT SIZE=4><I><B>вивчення</B></I></FONT><FONT SIZE=4>:</FONT></FONT></P>
        <OL>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Найвідоміші
	шифри.</SPAN></FONT></FONT></P>
        </OL>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><I><B>Запитання</B></I></FONT><FONT SIZE=4>
                </FONT><FONT SIZE=4><I><B>для</B></I></FONT><FONT SIZE=4>
                </FONT><FONT SIZE=4><I><B>самоаналізу</B></I></FONT><FONT SIZE=4>
                </FONT><FONT SIZE=4><I><B>та</B></I></FONT><FONT SIZE=4>
                </FONT><FONT SIZE=4><I><B>самоперевірки</B></I></FONT><FONT SIZE=4>:</FONT></FONT></P>
        <OL>
            <OL>
                <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in">
                        <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Шифрування.</SPAN></FONT></FONT></P>
                <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in">
                        <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Криптологія.</SPAN></FONT></FONT></P>
                <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in">
                        <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Криптографія,
		крипто аналіз.</SPAN></FONT></FONT></P>
                <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in">
                        <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Криптосистема.</SPAN></FONT></FONT></P>
            </OL>
        </OL>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Рекомендована</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>література</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">:
[4; 3, 345-371; 1, 86-92] </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; page-break-after: avoid">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Текст</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>лекції</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Криптологія</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
– наука про захист інформації, шляхом
її перетворення. Криптологія поєднує
два напрямки – криптографію й криптоаналіз.
Слід зазначити, що ці дві науки – парна
категорія. Розвиток однієї з них є
поштовхом для іншої. Розглядати окремо
криптографію від криптоаналізу, значить
порушити основи філософії й один з її
законів єдності й боротьби протилежностей.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Криптографія</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
займається пошуком і дослідженням
методів перетворення інформації з метою
приховання її змісту. Основні напрямки
використання криптографічних методів
– передача конфіденційної інформації
з каналів зв'язку, установлення дійсності
переданих повідомлень, зберігання
інформації (документів, баз даних) на
носіях у зашифрованому виді.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Криптоаналіз</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
– дослідження можливості розшифровування
інформації без знання ключів. У якості
інформації, що підлягає шифруванню й
розшифровуванню, будуть розглядатися
тексти, побудовані на деякому алфавіті.
Під цими термінами розуміється наступне.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Алфавіт</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
— кінцева множина використовуваних
для кодування інформації знаків. Слід
зазначити той факт, що в якості алфавіту
можуть виступати як множина символів
національних алфавітів, так і множина
різних символів (наприклад, танцюючих
чоловічків) і цифр.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Текст</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
— упорядкований набір з елементів
алфавіту.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Шифрування</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
– процес перетворення вихідного тексту,
який носить також назва відкритого
тексту, у шифрований текст.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Розшифрування
процес, зворотний шифруванню. На основі
ключа шифрований текст перетвориться
у вихідний.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Ключ</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
– цей конкретний секретний стан деяких
параметрів алгоритму криптографічного
перетворення даних, що забезпечує вибір
тільки одного варіанта з усіх можливих
для даного алгоритму. Звичайно ключ
являє собою послідовний ряд символів
алфавіту.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Простір</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>ключів</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>ДО</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
— це набір можливих значень ключа.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Криптографічна</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>система</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
являє собою сімейство Т перетворень
відкритого тексту.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Криптосистеми</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
підрозділяються на симетричні й
асиметричні (або з відкритим ключем).</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">У </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>симетричних</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>криптосистемах</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
для шифрування й для розшифровування
використовується один і той самий ключ.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">У системах з
відкритим ключем використовуються два
ключі-відкритий і закритий (секретний),
які математично зв'язано один з одним.
Інформація шифрується за допомогою
відкритого ключа, який доступний усім
бажаючим, а розшифровується за допомогою
закритого ключа, відомого тільки
одержувачеві повідомлення.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Терміни «</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>розподіл</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>ключів</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">»
і «</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>керування</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>ключами</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">»
ставляться до процесів системи обробки
інформації, змістом яких є вироблення
й розподіл ключів між користувачами.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Електронним</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>цифровим</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>підписом</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
називається його криптографічне
перетворення, що приєднується до тексту,
яке дозволяє при одержанні тексту іншим
користувачем перевірити авторство й
дійсність повідомлення.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Криптологічною</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>стійкістю</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
називається характеристика шифру, що
визначає його стійкість до розшифровування
без знання ключа (тобто криптоаналізу).
Є кілька показників криптостійкости,
серед яких:</SPAN></FONT></FONT></P>
        <UL>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">кількість усіх
	можливих ключів;</SPAN></FONT></FONT></P>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">середній час,
	необхідний для успішної криптоаналитичної
	атаки того або іншого виду.</SPAN></FONT></FONT></P>
        </UL>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Ефективність
шифрування з метою захисту інформації
залежить від збереження таємниці ключа
й криптостійкости шифру.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <BR>
        </P>
        <OL>
            <LI><P LANG="ru-RU" STYLE="text-indent: 0in; line-height: 100%; widows: 2; orphans: 2">
                    <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt"><I><B><FONT SIZE=4><SPAN LANG="uk-UA">Класифікація
	методів криптографічного перетворення
	інформації</SPAN></FONT></B></I></FONT></FONT></FONT></P>
        </OL>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Під
</SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>криптографічним
                                        захистом інформації</B></I></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
розуміється таке перетворення вихідної
інформації, в результаті якого вона
стає недоступною для ознайомлення та
використання особами, які не мають на
це повноважень.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Відомі
різні підходи до класифікації методів
криптографічного перетворення інформації.
По виду впливу на вихідну інформацію
методи криптографічного перетворення
інформації можуть бути розділені на
чотири групи.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><B><FONT SIZE=4><SPAN LANG="uk-UA">Методи
криптографічного перетворення інформації:</SPAN></FONT></B></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><B><FONT SIZE=4><SPAN LANG="uk-UA">1)
Шифрування</SPAN></FONT></B></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><B><FONT SIZE=4><SPAN LANG="uk-UA">2)
Стеганографія</SPAN></FONT></B></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><B><FONT SIZE=4><SPAN LANG="uk-UA">3)
Кодування</SPAN></FONT></B></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><B><FONT SIZE=4><SPAN LANG="uk-UA">4)
Стиснення</SPAN></FONT></B></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Процес
</SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>шифрування</B></I></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
полягає в проведенні оборотних
математичних, логічних, комбінаторних
та інших перетворень вихідної інформації,
в результаті яких зашифрована інформація
являє собою хаотичний набір букв, цифр,
інших символів і двійкових кодів.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Для
</SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>шифрування</B></I></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
інформації використовуються алгоритм
перетворення і ключ. Як правило, алгоритм
для певного методу шифрування є незмінним.
Вихідними даними для алгоритму шифрування
служать інформація, що підлягає
зашифрування, і ключ шифрування. Ключ
містить керуючу інформацію, яка визначає
вибір перетворення на певних кроках
алгоритму і величини операндів,
використовувані при реалізації алгоритму
шифрування.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">На
відміну від інших методів криптографічного
перетворення інформації, методи
</SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>стеганографії</B></I></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
дозволяють приховати не тільки сенс
збереженої або переданої інформації,
але й сам факт зберігання або передачі
закритої інформації. У комп'ютерних
системах практичне використання
стеганографії тільки починається, але
проведені дослідження показують її
перспективність. В основі всіх методів
стеганографії лежить маскування закритої
інформації серед відкритих файлів.
Обробка мультимедійних файлів в КС
відкрила практично необмежені можливості
перед стеганографією.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Існує
кілька методів прихованої передачі
інформації. Одним з них є простий метод
приховування файлів при роботі в
операційній системі MS DOS. За текстовим
відкритим файлом записується прихований
двійковий файл, обсяг якого багато менше
текстового файлу. В кінці текстового
файлу поміщається мітка EOF (комбінація
клавіш Control і Z). При зверненні до цього
текстового файлу стандартними засобами
ОС зчитування припиняється по досягненню
мітки EOF і прихований файл залишається
недоступним. Для двійкових файлів ніяких
міток в кінці файлу не передбачено.
Кінець такого файлу визначається при
обробці атрибутів, в яких зберігається
довжина файлу в байтах. Доступ до
прихованого файлу може бути отриманий,
якщо файл відкрити як двійковий.
Прихований файл може бути зашифрований.
Якщо хтось випадково виявить прихований
файл, то зашифрована інформація буде
сприйнята як збій у роботі системи.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Графічна
та звукова інформація представляються
в числовому вигляді. Так в графічних
об'єктах найменший елемент зображення
може кодуватися одним байтом. У молодші
розряди певних байтів зображення
відповідно до алгоритму криптографічного
перетворення поміщаються біти прихованого
файлу. Якщо правильно підібрати алгоритм
перетворення і зображення, на фоні якого
міститься прихований файл, то людському
оку практично неможливо відрізнити
отримане зображення від вихідного. Дуже
складно виявити приховану інформацію
і за допомогою спеціальних програм.
Найкращим чином для впровадження
прихованої інформації підходять
зображення місцевості: фотознімки з
супутників, літаків і т. п. За допомогою
засобів стеганографії можуть маскуватися
текст, зображення, мова, цифровий підпис,
зашифроване повідомлення. Комплексне
використання стеганографії та шифрування
багаторазово підвищує складність
рішення задачі виявлення і розкриття
конфіденційної інформації.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Змістом
процесу </SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>кодування</B></I></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
інформації є заміна смислових конструкцій
вихідної інформації (слів, речень)
кодами. В якості кодів можуть
використовуватися сполучення літер,
цифр, букв та цифр. При кодуванні і
зворотному перетворенні використовуються
спеціальні таблиці або словники.
Кодування інформації доцільно
застосовувати в системах з обмеженим
набором смислових конструкцій. Недоліками
кодування конфіденційної інформації
є необхідність зберігання та поширення
кодіровочних таблиць, які необхідно
часто міняти, щоб уникнути розкриття
кодів статистичними методами обробки
перехоплених повідомлень.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><B><FONT SIZE=4><SPAN LANG="uk-UA">Стиснення
інформації </SPAN></FONT></B></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-weight: normal">може
бути віднесено до методів криптографічного
перетворення інформації з певними
застереженнями. Метою стиснення є
скорочення обсягу інформації. У той же
час стисла інформація не може бути
прочитана чи використана без зворотного
перетворення. Враховуючи доступність
засобів стиснення і зворотного
перетворення, ці методи не можна
розглядати як надійні засоби
криптографічного перетворення інформації.
Навіть якщо тримати в секреті алгоритми,
то вони можуть бути порівняно легко
розкриті статистичними методами обробки.
Тому стислі файли конфіденційної
інформації піддаються подальшому
шифруванню. Для скорочення часу доцільно
поєднувати процес стиснення і шифрування
інформації.</SPAN></SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <BR>
        </P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><B><FONT SIZE=4><SPAN LANG="uk-UA">2.
Шифрування. Основні поняття</SPAN></FONT></B></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Основним
видом </SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>криптографічного</B></I></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
перетворення інформації в КС є </SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>шифрування</B></I></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">.
Під </SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>шифруванням</B></I></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
розуміється процес перетворення
відкритої інформації в зашифровану
інформацію (шифртексту) або процес
зворотного перетворення зашифрованої
інформації у відкриту. Процес перетворення
відкритої інформації в закриту отримав
назву зашифрування, а процес перетворення
закритої інформації у відкриту –
розшифрування.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">За
багатовікову історію використання
шифрування інформації людством винайдено
безліч методів шифрування або шифрів.
Методом шифрування (шифром) називається
сукупність оборотних перетворень
відкритої інформації в закриту інформацію
відповідно до алгоритму шифрування.
Більшість методів шифрування не витримали
перевірку часом, а деякі використовуються
й досі. Поява ЕОМ і КС ініціювало процес
розробки нових шифрів, що враховують
можливості використання ЕОМ як для
зашифрування / розшифрування інформації,
так і для атак на шифр. Атака на шифр
(криптоаналіз) – це процес розшифрування
закритої інформації без знання ключа
і, можливо, за відсутності відомостей
про алгоритм шифрування.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><U><B>Сучасні
                                            методи шифрування повинні відповідати
                                            наступним вимогам:</B></U></I></SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">стійкість
шифру протистояти криптоаналізу
(криптостійкість) повинна бути такою,
щоб розшифрування його могло бути
здійснене тільки шляхом рішення задачі
повного перебору ключів;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">криптостійкість
забезпечується не секретністю алгоритму
шифрування, а секретністю ключа;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">шифртекст
не повинен істотно перевершувати за
об'ємом вихідну інформацію;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">помилки,
що виникають при шифруванні, не повинні
приводити до спотворень і втрат
інформації;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">час
шифрування не повинен бути великим;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">вартість
шифрування повинна бути узгоджена з
вартістю закритоїя інформації.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Криптостійкість
                                        шифру</B></I></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
є його основним </SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>показником
                                        ефективності.</B></I></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
Вона вимірюється часом або вартістю
засобів, необхідних криптоаналітика
для отримання вихідної інформації по
шифртексту, за умови, що йому невідомий
ключ.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Зберегти
в секреті широко використовуваний
алгоритм шифрування практично неможливо.
Тому алгоритм не повинен мати прихованих
слабких місць, якими могли б скористатися
криптоаналітики. Якщо ця умова виконується,
то криптостійкість шифру визначається
довжиною ключа, так як єдиний шлях
розшифрування зашифрованої інформації
– перебір комбінацій ключа і виконання
алгоритму розшифрування. Таким чином,
час і кошти, що витрачаються на
криптоаналіз, залежать від довжини
ключа і складності алгоритму шифрування.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=LEFT STYLE="text-indent: 0in"><FONT SIZE=2><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">В
якості прикладу вдалого методу шифрування
можна навести шифр DES (Data Encryption Standard),
застосовуваний у США з 1978 року в якості
державного стандарту. Алгоритм шифрування
не є секретним і був опублікований у
відкритій пресі. За весь час використання
цього шифру не було оприлюднене жодного
випадку виявлення слабких місць в
алгоритмі шифрування.</SPAN></FONT></FONT></FONT></FONT></P>
    </div></div>
</BODY>
</HTML>
