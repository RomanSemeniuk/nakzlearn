<!--#include virtual="/topMenu.php" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
    <TITLE></TITLE>
    <META NAME="GENERATOR" CONTENT="LibreOffice 4.1.6.2 (Linux)">
    <META NAME="AUTHOR" CONTENT="Naks ®">
    <META NAME="CREATED" CONTENT="20151011;142800000000000">
    <META NAME="CHANGEDBY" CONTENT="Naks ®">
    <META NAME="CHANGED" CONTENT="20151011;142900000000000">
    <META NAME="AppVersion" CONTENT="14.0000">
    <META NAME="Company" CONTENT="SPecialiST RePack">
    <META NAME="DocSecurity" CONTENT="0">
    <META NAME="HyperlinksChanged" CONTENT="false">
    <META NAME="LinksUpToDate" CONTENT="false">
    <META NAME="ScaleCrop" CONTENT="false">
    <META NAME="ShareDoc" CONTENT="false">
    <STYLE TYPE="text/css">
        <!--
        @page { margin-left: 0.98in; margin-right: 0.59in; margin-top: 0.59in; margin-bottom: 0.59in }
        P { text-indent: 0.3in; margin-bottom: 0in; direction: ltr; line-height: 100%; text-align: justify; widows: 2; orphans: 2 }
        P.western { font-family: "Times New Roman", serif; font-size: 12pt; so-language: ru-RU }
        P.cjk { font-family: "Times New Roman"; font-size: 12pt; so-language: ru-RU }
        P.ctl { font-family: "Times New Roman"; font-size: 10pt }
        H2 { margin-top: 0.08in; margin-bottom: 0.04in; direction: ltr; line-height: 100%; widows: 2; orphans: 2 }
        H2.western { font-family: "Peterburg", serif; font-size: 11pt; so-language: uk-UA }
        H2.cjk { font-family: "Times New Roman"; font-size: 11pt; so-language: ru-RU }
        H2.ctl { font-family: "Times New Roman"; font-size: 10pt; font-weight: normal }
        A:link { color: #0000ff; so-language: zxx }
        -->
    </STYLE>
</HEAD>
<BODY LANG="uk-UA" LINK="#0000ff" DIR="LTR">
<div id="wrapper">
    <scrip><?php include('../toplearn.ini.php');?></scrip>
    <div id="content">
        <H2 CLASS="western" ALIGN=CENTER><A NAME="_Toc365048721"></A><FONT FACE="Times New Roman, serif"><FONT SIZE=5 STYLE="font-size: 20pt">Методи
                    і засоби захисту інформації в КС від
                    традиційного шпигунства і диверсій</FONT></FONT></H2>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>1. Система
                            охорони об'єкта КС</B></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">При захисті
інформації в КС від традиційного
шпигунства і диверсій використовуються
ті ж засоби і методи захисту, що і для
захисту інших об'єктів, на яких не
використовуються КС. Для захисту об'єктів
КС від загроз даного класу повинні бути
вирішені наступні завдання:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">створення
системи охорони об'єкта;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">організація
робіт з конфіденційними інформаційними
ресурсами на об'єкті КС;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">протидія
спостереженню;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">протидія
підслуховування;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">захист від
зловмисних дій персоналу.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Об'єкт, на якому
проводяться роботи з цінною конфіденційною
інформацією, має, як правило, кілька
рубежів захисту:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">1) контрольована
територія;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">2) будівля;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">3) приміщення;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">4) пристрій,
носій інформації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">5) програма;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">6) інформаційні
ресурси.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Від шпигунства
і диверсій необхідно захищати перші
чотири рубежі і обслуговуючий персонал.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Система охорони
об'єкта (СОО) КС створюється з метою
запобігання несанкціонованого проникнення
на територію та в приміщення об'єкта
сторонніх осіб, обслуговуючого персоналу
і користувачів.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Склад системи
охорони залежить від об'єкту, що
охороняється. У загальному випадку СОО
КС повинна включати наступні компоненти:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">інженерні
конструкції;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">охоронна
сигналізація;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">засоби
спостереження;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">підсистема
доступу на об'єкт;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">чергова зміна
охорони.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>1.1. Інженерні
                            конструкції</B></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Інженерні
конструкції служать для створення
механічних перешкод на шляху зловмисників.
Вони створюються по периметру
контрольованої зони. Інженерними
конструкціями обладнуються також
будівлі і приміщення об'єктів. По
периметру контрольованої території
використовуються бетонні або цегляні
огорожі, решітки або сіткові конструкції.
Бетонні і цегляні огорожі мають зазвичай
висоту в межах 1,8-2,5 м, сіткові – до 2,2 м.
Для підвищення захисних властивостей
загороджень поверх заборів зміцнюється
колючий дріт, гострі стрижні, армована
колюча стрічка. Остання виготовляється
шляхом армування колючої стрічки
сталевої оцинкованої дротом діаметром
2,5 мм. Армована колюча стрічка часто
використовується у вигляді спіралі
діаметром 500-955 мм. Для затруднення
проникнення зловмисника на контрольовану
територію можуть використовуватися
малопомітні перешкоди. Прикладом
малопомітних перешкод може служити
металева мережа з тонкого дроту. Така
мережа розташовується уздовж паркану
на ширину до 10 метрів. Вона виключає
швидке переміщення зловмисника.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">У будівлі та
приміщення зловмисники намагаються
проникнути, як правило, через двері чи
вікна. Тому за допомогою інженерних
конструкцій зміцнюють, перш за все, цю
слабку ланку в захисті об'єктів. Надійність
дверей залежить від механічної міцності
самих дверей і від надійності замків.
Чим вище вимоги до надійності двері,
тим міцнішою вона є, тим вище вимоги до
механічної міцності і здатності
протистояти несанкціонованому відкриванню
ставляться до замку.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Замість
механічних замків все частіше
використовуються кодові замки.
Найпоширенішими серед них (званих
звичайно сейфовими замками) є дискові
кодові замки з числом комбінацій коду
ключа в межах 106-107.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Найвищу стійкість
мають електронні замки, побудовані із
застосуванням мікросхем. Наприклад,
при побудові електронних замків широко
використовуються мікросхеми Touch Memory.
Мікросхема поміщена в сталевий корпус,
який за зовнішнім виглядом нагадує
елемент живлення наручного годинника,
калькуляторів і т. п. Діаметр циліндричної
частини дорівнює 16 мм, а висота – 3-5 мм.
Електроживлення мікросхеми забезпечується
знаходженням всередині корпусу елементів
живлення, ресурс якого розрахований на
10 років експлуатації. Корпус може
розміщуватися на пластиковій картці
або в пластмасовій оправі у вигляді
брелока. У мікросхемі зберігається її
індивідуальний 64-бітовий номер. Така
розрядність забезпечує близько 1020
комбінацій ключа, практично виключає
його підбір. Мікросхема має також
перезаписувану пам'ять, що дозволяє
використовувати її для запису і зчитування
додаткової інформації. Обмін інформацією
між мікросхемою і замком здійснюється
при дотику контакту замку і певної
частини корпусу мікросхеми.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">На базі
електронних замків будуються автоматизовані
системи контролю доступу в приміщення.
У кожен замок вводяться номери мікросхем,
власники яких допущені до відповідного
приміщення. Може також задаватися
індивідуальний часовий інтервал,
протягом якого можливий доступ у
приміщення. Всі замки можуть об'єднуватися
в єдину автоматизовану систему,
центральною частиною якої є ПЕОМ. Вся
керуюча інформація в замку передається
з ПЕОМ адміністратором. Якщо замок
відкривається зсередини також за
допомогою електронного ключа, то система
дозволяє фіксувати час входу і виходу,
а також час перебування власників ключів
в приміщеннях. Ця система дозволяє в
будь-який момент встановити місцезнаходження
співробітника. Система стежить за тим,
щоб двері завжди була закрита. При
спробах відкривання дверей в обхід
електронного замка включається сигнал
тривоги з оповіщенням на центральний
пункт управління. До таких автоматизованим
системам ставляться вітчизняні системи
&quot;Менует&quot; і «Полонез».</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">За статистикою
85% випадків проникнення на об'єкти
відбувається через віконні отвори. Ці
дані говорять про необхідність інженерного
укріплення вікон, яке здійснюється
двома шляхами:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">установка
віконних решіток;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">застосування
стекол, стійких до механічного впливу.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Традиційним
захистом вікон від проникнення
зловмисників є установка грат. Грати
повинні мати діаметр прутів не менше
10 мм, відстань між ними повинна бути не
більше 120 мм, а глибина закладення прутів
в стіну – не менше 200 мм .</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Не менш серйозною
перешкодою на шляху зловмисника можуть
бути і спеціальні скла. Підвищення
механічної міцності йде по трьох
напрямах:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">загартовування
скла;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">виготовлення
багатошарових стекол;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">застосування
захисних плівок.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Механічна
міцність напівзагартаваного скла в 2
рази, а загартованого в 4 рази вище
звичайного будівельного скла.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">У багатошарових
стеклах використовуються спеціальні
плівки з високим опором на розрив. За
допомогою цих «ламінованих» плівок і
синтетичного клею забезпечується
склеювання на молекулярному рівні
плівки і стекол. Такі багатошарові скла
товщиною 48-83 мм забезпечують захист від
сталевої 7,62 мм кулі, випущеної з автомата
Калашникова.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Дедалі більшого
поширення набувають багатофункціональні
захисні поліефірні плівки. Наклеєні на
звичайне віконне скло, вони підвищують
його міцність в 20 разів. Плівка складається
з шести дуже тонких (одиниці мікрон)
шарів: лавсану (3 шари), металізованого
і невисихаючого клею адгезиву і лакового
покриття. Крім механічної міцності вони
надають вікнам цілий ряд захисних
властивостей і покращують експлуатаційні
характеристики. Плівки послаблюють
електромагнітні випромінювання в 50
разів, суттєво ускладнюють ведення
розвідки візуально-оптичними методами
і перехоплення мовної інформації
лазерними засобами. Крім того, плівки
покращують зовнішній вигляд стекол,
відбивають до 99% ультрафіолетових
променів і 76% теплової енергії сонця,
стримують поширення вогню при пожежах
протягом 40 хвилин.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; page-break-after: avoid">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>1.2. Охоронна
                            сигналізація</B></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Охоронна
сигналізація служить для виявлення
спроб несанкціонованого проникнення
на об'єкт, що охороняється. Системи
охоронної сигналізації повинні
відповідати наступним вимогам:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">охоплення
контрольованої зони по всьому периметру;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">висока
чутливість до дій зловмисника;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">надійна
робота в будь-яких погодних і часових
умовах;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">стійкість
до природних перешкод;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">швидкість
і точність визначення місця порушення;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">можливість
централізованого контролю подій.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Датчик</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
(сповіщувач) являє собою пристрій, що
формує електричний сигнал тривоги при
впливі на датчик або на створюване їм
поле зовнішніх сил або об'єктів.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Шлейф сигналізації
утворює електричний ланцюг для передачі
сигналу тривоги від датчика до
приймально-контрольного пристрою.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Приймально-контрольний
пристрій служить для прийому сигналів
від датчиків, їх обробки і реєстрації,
а також для видачі сигналів в оповіщувач.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Оповіщувач
видає світлові і звукові сигнали
черговому охоронцю.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>За принципом
                            виявлення зловмисників датчики</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
діляться на:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">контактні;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">акустичні;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">оптико-електронні;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">мікрохвильові;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">вібраційні;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">ємнісні;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">телевізійні.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Контактні
                            датчики</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
реагують на замикання або розмикання
контактів, на обрив тонкого дроту або
смужки фольги. Вони бувають електроконтактні,
магнітоконтактні, ударно-контактними
і Обривність.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I>Електроконтактні
                            датчики</I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
являють собою кнопкові вимикачі, які
розмикають (замикають) електричні
ланцюги, по яких сигнал тривоги надходить
на приймально-контрольний пристрій при
несанкціонованому відкритті дверей,
вікон, люків, шаф тощо. </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I>Магнітоконтактні
                            датчики</I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
служать також для блокування дверей,
вікон і т. п. Крім того, ці датчики
використовуються для охорони переносимих
предметів (невеликих сейфів, носіїв
інформації, переносних пристроїв і т.
п.). Основу датчиків складають геркони.
У герконах контакти електричного кола
замикаються (розмикаються) під дією
постійного магнітного поля. Геркон
кріпиться на нерухомій частині, а магніт
на рухомої частини. При закритих дверях,
вікнах і т. п., а також при знаходженні
переносимих предметів на місці, геркон
знаходиться в полі магніту. При видаленні
магніту від геркона ланцюг розмикається
(замикається), і сигнал тривоги надходить
на приймально-контрольний пристрій. </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I>Ударноконтактні</I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
датчики використовуються для блокування
поверхонь, які розрушаються. За допомогою
датчиків цього типу блокуються шибки.
У датчиках цього типу нормально замкнуті
контакти розмикаються під дією сили
інерції при переміщенні корпуса датчика,
приклеєного до скла.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Акустичні
                            датчики</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
використовуються для охорони будинків
і приміщень. Принцип дії акустичних
датчиків заснований на використанні
акустичних хвиль, що виникають при
виламування елементів конструкції
приміщень або відбитих від зловмисника.
Використовуються датчики двох типів:
пасивні та активні.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I>Пасивні
                            датчики</I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
вловлюють акустичні хвилі, що виникають
при руйнуванні елементів конструкції
приміщення, найчастіше шибок. Пасивні
датчики поділяються на п'єзоелектричні
та електромагнітні. У п'єзоелектричних
датчиках використовується властивість
п'єзоелементів створювати електричний
сигнал при механічному впливі на їх
поверхню. В електромагнітних датчиках
використовується властивість виникнення
ЕРС в котушці електромагніту при зміні
відстані між сердечником електромагніта
і мембраною. </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I>Активні
                            датчики</I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
складаються з двох блоків. Один з них
випромінює акустичні хвилі ультразвукового
діапазону в приміщенні, а інший аналізує
відбиті хвилі. При появі будь-яких
предметів в контрольованому приміщенні
або загорянь змінюється акустичний
фон, що і фіксується датчиком. Активні
акустичні (ультразвукові) датчики
служать для виявлення зловмисників і
осередків пожеж у закритих приміщеннях.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Оптико-електронні
                            датчики</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
побудовані на використанні інфрачервоних
променів. Такі датчики діляться на
активні і пасивні. Для роботи активних
датчиків використовується випромінювач
гостронаправлених ІК-променів, які
приймаються приймачем. При екранування
ІК-променів яким-небудь об'єктом приймач
фіксує відсутність ІК-опромінення і
видає сигнал тривоги. Пасивні датчики
реагують на теплове випромінювання
людини або вогню. Для охорони коридорів,
вікон, дверей і території по периметру
використовуються активні датчики.
Випромінювач датчика створює від 2 до
16 паралельних ІК-променів. Відстань між
випромінювачем і приймачем датчика
знаходиться в діапазоні 20-300 метрів. Для
охорони територій по периметру
використовуються активні лінійні
оптико-електронні випромінювачі.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I>Пасивні
                            оптико-електронні датчики</I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
використовуються при охороні приміщень.
Вони здатні зафіксувати об'єкт, температура
якого не менше ніж на 3 ° С вище температури
фону. Датчики цього типу чутливі до
джерел тепла (батареї, електроприлади)
і сонячним променям. Ці особливості
датчиків повинні враховуватися при їх
установці.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>У мікрохвильових
                            (радіохвильових) датчиках</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
для виявлення зловмисників використовуються
електромагнітні хвилі в НВЧ діапазоні
(9-11ГГц). Ці датчики складаються з
випромінювача і приймача. Розрізняють
радіопроменеві і радіотехнічні датчики.
У радіопроменеві датчики використовуються
випромінювачі, антени яких формують
вузьку діаграму спрямованості у вигляді
витягнутого еліпсоїда з висотою і
шириною в середині зони виявлення 2-10
м. Протяжність ділянки виявлення досягає
300 м. Приймач реагує на ослаблення
напруженості поля при перетині об'єктом
електромагнітного променя. При охороні
територій по периметру використовуються
радіопроменеві датчики. </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">В </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I>радіотехнічних
                            датчиках</I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
зловмисник виявляється по зміні
характеристик НВЧ поля. У цих датчиках
у якості антени випромінювача в НВЧ
діапазоні використовується спеціальний
радіочастотний кабель, який прокладається
по периметру території, що охороняється.
Антена приймача знаходиться в центрі
території або являє собою кабель,
прокладений паралельно випромінює
кабелю. При попаданні зловмисника в
зону випромінювання характеристики
сигналу на вході приймача змінюються,
і приймач видає сигнал тривоги в
приймально-контрольний пристрій.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">В деяких
радіотехнічних датчиках електромагнітне
поле створюється між двома паралельно
розташованими коаксіальними кабелями
з отворами. Кабелі укладаються під землю
уздовж периметра контрольованої
території на глибині 10-15 см на відстані
2-3 метри один від одного. Один кабель
через отвори в обплетенні створює
електромагнітне поле, а паралельно
проходить кабель також через отвори
приймає це електромагнітне поле.
Створюване поле має розміри: ширина –
до 10 метрів, висота і глибина – до 70 см.
Така кабельна система охорони дозволяє
виявляти не тільки зловмисника, який
пересувається по поверхні землі, але й
фіксувати спроби підкопу.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Вібраційні
                            датчики</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
виявляють зловмисника по вібрації
землі, загороджень, створюваної ним при
проникненні на контрольовану територію.
Якщо датчики розміщуються під землею,
то їх називають сейсмічними. Вібраційні
датчики виконуються у вигляді окремих
п'єзо-та електромагнітних чутливих
елементів, у вигляді світловодів, кабелів
з електричним і магнітним полями, а
також у вигляді шлангів з рідиною. При
механічній дії на датчики змінюються
фізичні характеристики речовин, полів,
світлового променя, які перетворюються
в електричні сигнали тривоги. </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Принцип дії
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><B>ємнісних
                            датчиків</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
полягає в зміні еквівалентної ємності
в контурі генератора сигналів датчика,
яке викликається збільшенням розподіленої
ємності між зловмисником і антеною
датчика. Відстань спрацьовування
становить 10-30 см. В якості антени може
бути використаний охороняється металевий
об'єкт (сейф, шафа) або провід. Провід-антена
може бути прокладений по верхній частині
паркану, вздовж вікон, дверних прорізів
і т. п. Ємнісні датчики широко
використовуються при охороні контрольованих
територій, конструкцій будівель і
приміщень.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Для контролю
охоронюваної зони невеликих розмірів
або окремих важливих приміщень можуть
використовуватися </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><B>телевізійні
                            датчики</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">.
Такий датчик являє собою телевізійну
камеру, яка безперервно передає зображення
ділянки місцевості. Приймально-контрольний
пристрій з певною дискретністю (до 20
разів на секунду) опитує датчики і
порівнює зображення з отриманим раніше.
Якщо в зображеннях помічається різниця
(поява нових об'єктів, рух об'єктів), то
включається монітор чергового охоронця
з подачею звукового сигналу і включенням
відеомагнітофона.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">При спробах
знищення, знеструмлення датчиків і
шлейфів всіх розглянутих типів черговий
оператор охорони отримує сигнал тривоги.
Кожен тип датчиків фіксує спроби
проникнення на територію, що охороняється
з певною ймовірністю. Для датчиків також
можливо помилкове спрацьовування при
появі природних перешкод, таких як
сильний вітер, птахи і тварини, грім і
ін Підвищення надійності роботи систем
контролю доступу на територію об'єкта
досягається шляхом:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">комбінованого
використання датчиків різного типу;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">вдосконалення
датчиків і приймально-контрольних
пристроїв.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Комбіноване
використання датчиків різних типів
значно знижує ймовірність безконтрольного
проникнення зловмисника на територію
об'єкту КС. Основними напрямами
вдосконалення датчиків є підвищення
чутливості і завадостійкості. Найбільш
складним завданням є підвищення
завадостійкості датчиків. Для вирішення
цього завдання в датчиках повинні бути
закладені наступні можливості:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">регулювання
чутливості;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">аналіз
декількох ознак можливого зловмисника
(наприклад, розміру та динаміки
переміщення);</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">здатність
до навчання;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">стійкість
до змін погодних умов.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Щоб забезпечити
реалізацію таких можливостей, сучасні
датчики створюються з використанням
мікропроцесорної техніки.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Удосконалення
приймально-контрольних пристроїв йде
в напрямку збільшення числа підключаються
шлейфів і типів датчиків, підвищення
достовірності сигналів тривоги за
рахунок додаткової обробки вступних
сигналів від датчиків, інтеграції
управління всіма охоронними системами,
включаючи систему пожежної безпеки, в
одному пристрої керування комплексною
системою охорони об'єкта. Такий пристрій
виконується на базі ПЕОМ.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>1.3. Засоби
                            спостереження</B></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Організація
безперервного спостереження або
відеоконтролю за об'єктом є однією з
основних складових системи охорони
об'єкта. У сучасних умовах функція
спостереження за об'єктом реалізується
за допомогою систем замкнутого
телебачення. Їх називають також
телевізійними системами відеоконтролю
(ТСВ).</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Телевізійна
                                система відеоконтролю забезпечує:</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">автоматизоване
відеоспостереження за рубежами захисту;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">контроль за
діями персоналу організації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">відеозапис
дій зловмисників;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">режим
відеоохорони.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">У режимі
відеоохорони ТСВ виконує функції
охоронної сигналізації. Оператор ТСВ
оповіщається про рух в зоні спостереження.
У загальному випадку телевізійна система
відеоконтролю включає наступні пристрої:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">передавальні
телевізійні камери;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">монітори;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">пристрій
обробки і комутації відеоінформації
(ПОКВ);</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">пристрої
реєстрації інформації (ПРІ). </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Діапазон
застосовуваних телевізійних камер в
ТСВ дуже широкий. Використовуються
чорно-білі та кольорові камери. Телекамери
можуть встановлюватися приховано. Для
цих цілей використовуються мініатюрні
спеціальні камери із зменшеним зовнішнім
діаметром вічка. Камери розрізняються
також роздільною здатністю, довжиною
фокусної відстані і рядом інших
характеристик. Для нормального
функціонування телекамер в зоні їх
застосування повинна підтримуватися
необхідна освітленість.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Використовуються
чорно-білі та кольорові монітори. Вони
відрізняються також роздільною здатністю
і розмірами екрану.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">У найпростіших
ТСВ зображення від телекамер безпосередньо
подається на входи моніторів.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">При наявності
моніторів від 4-х і більше оператору
складно вести спостереження. Для
скорочення числа моніторів використовуються
пристрої керування. У якості пристроїв
обробки і комутації відеоінформації
можуть застосовуватися такі пристрої:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">комутатори;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">квадратори;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">мультиплексори;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">детектори
руху.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Комутатори</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
дозволяють підключити до одного монітора
від 4 до 16 телекамер з можливістю ручного
або автоматичного перемикання з камери
на камеру.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Квадратори</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
забезпечують одночасну видачу зображення
на одному моніторі від кількох телекамер.
Для цього екран монітора ділиться на
частини за кількістю телекамер.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Мультиплексор</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
є більш досконалим ПОКВ. Він може
виконувати функції комутатора і
квадратора. Крім того, він дозволяє
здійснювати запис зображення на
відеомагнітофон з будь-якої камери.
Мультиплексор може включати в себе
вбудований детектор руху.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Детектор руху</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
оповіщає оператора про рух в зоні
контролю телекамери, підключає цю камеру
для запису відеоінформації на
відеомагнітофон.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Відеомагнітофон
відноситься до пристроїв реєстрації
відеоінформації. У системах ТСВ
використовуються спеціальні
відеомагнітофони, які забезпечують
набагато більший час запису (від 24 годин
до 40 діб), ніж побутові відеомагнітофони.
Це досягається за рахунок пропуску
кадрів, ущільнення запису, запису при
спрацюванні детектора руху або по
команді оператора.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Для фіксації
окремих кадрів на папері використовується
другий ПРІ – відеопринтер.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; page-break-after: avoid">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>1.4. Підсистема
                            доступу на об'єкт</B></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Доступ на
об'єкти проводиться на контрольно-пропускних
пунктах (КПП), прохідних, через
контрольований вхід до будівлі та
приміщення. На КПП і прохідних чергують
контролери зі складу чергової зміни
охорони. Вхід до будівлі та приміщення
може контролюватися тільки технічними
засобами. Прохідні, КПП, входи в будівлі
і приміщення обладнуються засобами
автоматизації та контролю доступу.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Одним з основних
завдань, що вирішуються при організації
допуску на об'єкт, є ідентифікація і
автентифікація осіб, що допускаються
на об'єкт. Їх називають суб'єктами
доступу.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Під ідентифікацією
розуміється присвоєння суб'єктам доступу
ідентифікаторів і (або) порівняння
пропонованих ідентифікаторів з переліком
привласнених ідентифікаторів, власники
(носії) яких допущені на об'єкт.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Аутентифікація
означає перевірку приналежності суб'єкту
доступу пред'явленого їм ідентифікатора,
підтвердження справжності.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Розрізняють
два способи ідентифікації людей:
атрибутивний і біометричний. Атрибутивний
спосіб передбачає видачу суб'єкту
доступу або унікального предмета, або
пароля (коду), або предмета, що містить
код.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Предметами,
які ідентифікують суб'єкт доступу,
можуть бути пропуска, жетони або ключі
від вхідних дверей. Пропуска, жетони і
тому подібні ідентифікатори не дозволяють
автоматизувати процес допуску.
Ідентифікація та автентифікація
особистості здійснюється контролером
і тому носить суб'єктивний характер.
Пароль являє собою набір символів і
цифр, який відомий тільки власнику
пароля і введений в систему, що забезпечує
доступ. Паролі використовуються, як
правило, в системах розмежування доступу
до пристроїв КС. При допуску на об'єкти
КС частіше використовуються коди. Вони
використовуються для відкриття кодових
замків і містять, в основному, цифри.
Найбільш перспективними є ідентифікатори,
які являють собою матеріальний носій
інформації, що містить ідентифікаційний
код суб'єкта доступу. Найчастіше носій
коду виконується у вигляді пластикової
карти невеликого розміру (площа карти
приблизно в 2 рази більше площі поверхні
сірникової коробки). Код ідентифікатора
може бути лічений тільки за допомогою
спеціального пристрою. Крім коду карта
може містити фотографію, короткі дані
про власника, тобто ту інформацію, яка
зазвичай мається на пропусках.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Пластикові
картки повинні відповідати ряду вимог:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">складність
несанкціонованого зчитування коду та
виготовлення дубля карти;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">високі
експлуатаційні якості;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">достатня
довжина коду;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">низька
вартість.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Під експлуатаційними
якостями розуміється надійність
функціонування, можливість періодичної
зміни коду, стійкість до впливів
зовнішнього середовища, зручність
зберігання та використання, тривалий
термін служби.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Залежно від
фізичних принципів запису, зберігання
та зчитування ідентифікаційної інформації
карти діляться на:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">магнітні;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">інфрачервоні;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">карти оптичної
пам'яті;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">штрихові;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">напівпровідникові.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Магнітні
                            картки</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"> мають
магнітну смугу, на якій може зберігатися
близько 100 байт інформації. Ця інформація
зчитується спеціальним пристроєм при
проштовхуванні карти в прорізи пристрої.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">На внутрішньому
шарі </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><B>інфрачервоних
                            карт</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"> за
допомогою спеціальної речовини, що
поглинає інфрачервоні промені, наноситься
ідентифікаційна інформація. Верхній
шар карт прозорий для інфрачервоних
променів. Ідентифікаційний код зчитується
при опроміненні карти зовнішнім джерелом
інфрачервоних променів.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">При виготовленні
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><B>карт оптичної
                            пам'яті</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
використовується WORM-технологія, яка
застосовується при виробництві
компакт-дисків. Дзеркальна поверхня
обробляється променем лазера, який
пропалює в потрібних позиціях отвори
на цій поверхні. Інформація зчитується
в спеціальних пристроях шляхом аналізу
відбитих від поверхні променів. Ємність
такої карти від 2 до 16 Мбайт інформації.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>У штрихових
                            картах</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"> на
внутрішньому шарі наносяться штрихи,
які доступні для сприйняття тільки при
спеціальному опроміненні променями
світла. Варіюючи товщину штрихів і їх
послідовність, отримують ідентифікаційний
код. Процес зчитування здійснюється
протягуванням карти в прорізи зчитує.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Напівпровідникові
                            карти</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"> містять
напівпровідникові мікросхеми і можуть
бути контактними і безконтактними.
Контактні карти мають за стандартом
ISO 7816-1:1988 вісім металевих контактів з
золотим покриттям. Найбільш простими
напівпровідниковими контактними
картками є карти, що містять тільки
мікросхеми пам'яті. Найбільше поширення
з карт такого типу отримали карти Touch
Memory. Карта містить постійну пам'ять
об'ємом 64 біта, в якій зберігається
серійний номер Touch Memory. Карта може мати
і пере записувану енергонезалежну
пам'ять об'ємом від 1Кбіт до 4Кбіт. Карта
цього типу не має роз'єму. Його замінює
двохпровідний інтерфейс послідовного
типу.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Напівпровідникові
                            карти,</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"> що
мають у своєму складі мікропроцесор і
пам'ять, називають інтелектуальними
або смарт-картами. Смарт-карти фактично
містять мікро-ЕОМ. Крім завдань
ідентифікації такі карти вирішують
цілий ряд інших завдань, пов'язаних з
розмежуванням доступу до інформації в
КС. Ще більш широке коло завдань здатні
вирішувати суперсмарт-карти. Прикладом
може служити багатоцільова картка фірми
Toshiba, яка використовується в системі
VISA. Можливості смарт-карти в таких картах
доповнені мініатюрним монітором і
клавіатурою.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I>Безконтактні
                            («проксиміті»)</I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
карти мають у своєму складі енергонезалежну
пам'ять, радіочастотний ідентифікатор
і рамкову антену. </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Найменш
захищеними від фальсифікації є магнітні
карти. Максимальну захищеність мають
смарт-карти. Карти «проксиміті» дуже
зручні в експлуатації.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Всі атрибутивні
ідентифікатори володіють одним істотним
недоліком. Ідентифікаційний ознака
слабко або зовсім не пов'язана з
особистістю пред'явника.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Цього недоліку
позбавлені методи біометричної
ідентифікації. Вони засновані на
використанні індивідуальних біологічних
особливостей людини.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Для
                                біометричної ідентифікації людини
                                використовуються:</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">папілярні
візерунки пальців;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">візерунки
сітківки очей;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">форма кисті
руки;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">особливості
мови;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">форма і
розміри особи.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">динаміка
підпису;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">ритм роботи
на клавіатурі;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">запах тіла;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">термічні
характеристики тіла. </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Дактилоскопічний
                            метод ідентифікації</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
людини використовується давно. Він
показав високу достовірність ідентифікації.
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Папілярні
                                візерунки</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
зчитуються з пальця спеціальним сканером.
Отримані результати порівнюються з
даними, що зберігаються в системі
ідентифікації.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Для здешевлення
устаткування ідентифікація проводиться
з використанням не всіх ознак. На
ймовірність помилки впливають деякі
фактори, наприклад, температура пальців.
</SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">По надійності
і витратам часу метод ідентифікації по
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>візерунках
                                сітківки очей</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
зіставимо з дактилоскопічний методом.
За допомогою високоякісної телекамери
здійснюється сканування сітківки ока.
Фіксується кутовий розподіл кровоносних
судин на поверхні сітківки щодо сліпої
плями ока та інших ознак. Всього
налічується близько 250 ознак. Обидва
методи доставляють суб'єктам доступу
деякий дискомфорт. Дактилоскопічний
метод у багатьох асоціюється зі зняттям
відбитків пальців у злочинців. Метод
сканування сітківки ока доставляє
незручності, які людина відчуває в
процесі сканування. Крім того, метод
ідентифікації по візерунку сітківки
ока вимагає використання дорогого
обладнання.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Ідентифікація
людини </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>за
                                формою кисті руки</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
заснована на аналізі тривимірного
зображення кисті. Метод менш надійний,
пристрій ідентифікації досить громіздкий.
Разом з тим метод технологічний і не
вимагає зберігання великих обсягів
інформації.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Широке поширення
знайшли способи ідентифікації людини
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>по голосу
                                і по параметрах особи</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">.
По надійності методи поступаються
методам ідентифікації за відбитками
пальців і візерункам сітківки ока.
Пояснюється це значно меншою стабільністю
параметрів голосу та особи людини. Проте
кращі системи забезпечують вірогідність
достовірної ідентифікації порядку
0,98, що дозволяє використовувати їх на
практиці (Voice Bolt).</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Системи
ідентифікації за </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>почерком
                            </B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">аналізують
графічне накреслення, інтенсивність
натискання і швидкість написання букв.
Контрольне слово пишеться на спеціальному
планшеті, який перетворює характеристики
письма в електричні сигнали. Системи
такого типу забезпечують високу
надійність ідентифікації.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Ідентифікація
по </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>ритму
                                роботи на клавіатурі</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
ґрунтується на вимірюванні часу між
послідовним натисканням двох клавіш.
В системі зберігаються результати
вимірювань на тексті, оброблені методами
математичної статистики. Ідентифікація
проводиться шляхом набору, статистичної
обробки довільного або фіксованого
тексту і порівняння зі збереженими
даними. Метод забезпечує високу надійність
ідентифікації. Це єдиний біометричний
метод ідентифікації, який не потребує
додаткових апаратних витрат, якщо він
використовується для допуску до роботи
на технічних засобах, що мають набірні
пристрої.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Методи
ідентифікації по </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>запаху
                                і термічним характеристикам</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
тіла поки не знайшли широкого застосування.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Основною
перевагою біометричних методів
ідентифікації є дуже висока ймовірність
виявлення спроб несанкціонованого
доступу. Але цим методам властиві два
недоліки. Навіть в найкращих системах
ймовірність помилкового відмови в
доступі суб'єкту, який має право на
доступ, становить 0,01. Витрати на
забезпечення біометричних методів
доступу, як правило, перевершують витрати
на організацію атрибутивних методів
доступу.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Для підвищення
надійності аутентифікації використовуються
декілька ідентифікаторів.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Підсистема
доступу на об'єкт виконує також функції
реєстрації суб'єктів доступу і управління
доступом. Якщо на об'єкті реалізована
ідентифікація з використанням
автоматизованої системи на базі ПЕОМ,
то з її допомогою може вестися протокол
перебування співробітників на об'єкті,
в приміщеннях. Така система дозволяє
здійснювати дистанційний контроль
відкривання дверей, воріт і т. п., а також
оперативно змінювати режим доступу
співробітників в приміщення. До засобів
керування доступом можна віднести
засоби дистанційного керування замками,
приводами дверей, воріт, турнікетів
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">тощо</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>1.5. Чергова
                            зміна охорони</B></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Склад чергової
зміни, його екіпірування, місце розміщення
визначається статусом об'єкта, що
охороняється. Використовуючи охоронну
сигналізацію, системи спостереження і
автоматизації доступу, чергова зміна
охорони забезпечує тільки санкціонований
доступ на об'єкт і в охоронювані
приміщення. Чергова зміна може бути на
об'єкті постійно або прибувати на об'єкт
при отриманні сигналів тривоги від
систем сигналізації та спостереження.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>2. Організація
                            робіт з конфіденційними інформаційними
                            ресурсами на об'єктах КС</B></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Для протидії
таким загрозам як розкрадання документів,
носіїв інформації, атрибутів систем
захисту, а також вивчення відходів
носіїв інформації і створення неврахованих
копій документів необхідно визначити
порядок обліку, зберігання, видачі,
роботи і знищення носіїв інформації.
Для забезпечення такої роботи в установі
можуть створюватися спеціальні підрозділи
конфіденційного діловодства, або
вводитися штатні або позаштатні посади
співробітників. Робота з конфіденційними
інформаційними ресурсами здійснюється
відповідно до законів України і відомчими
інструкціями. </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><B>У
                            кожній організації повинні бути</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">розмежовані
повноваження посадових осіб по допуску
їх до інформаційних ресурсів:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">визначені
та обладнані місця зберігання
конфіденційних інформаційних ресурсів
та місця роботи з ними;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">встановлено
порядок обліку, видачі, роботи і здачі
на зберігання конфіденційних інформаційних
ресурсів;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">призначені
відповідальні особи з визначенням їх
повноважень та обов'язків;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">організовано
збір і знищення непотрібних документів
і списаних машинних носіїв;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">організований
контроль над виконанням встановленого
порядку роботи з конфіденційними
ресурсами.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>3. Протидія
                            спостереженню в оптичному діапазоні</B></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Спостереження
в оптичному діапазоні зловмисником, що
знаходяться за межами об'єкта із КС,
малоефективно. З відстані 50 метрів
навіть досконалим довгофокусним
фотоапаратом неможливо прочитати текст
з документа або монітора. Так телеоб'єктив
з фокусною відстанню 300 мм забезпечує
роздільну здатність лише 15x15 мм. Крім
того, загрози такого типу легко обмежуються
за допомогою:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">використання
шибок з односторонньою провідністю
світла;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">застосування
штор і захисного фарбування стекол;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">розміщення
робочих столів, моніторів, табло та
плакатів таким чином, щоб вони не
проглядалися через вікна або відкриті
двері.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Для протидії
спостереженню в оптичному діапазоні
зловмисником, що знаходяться на об'єкті,
необхідно, щоб:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">двері
приміщень були закритими;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; widows: 0; orphans: 0">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">розташування
столів і моніторів ЕОМ виключало
можливість спостереження документів
або видаваної інформації на сусідньому
столі або моніторі;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">стенди з
конфіденційною інформацією мали штори.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>4. Протидія
                            підслуховування</B></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Методи боротьби
з підслуховуванням можна розділити на
два класи:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">1) методи захисту
мовної інформації при передачі її по
каналах зв'язку;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">2) методи захисту
від прослуховування акустичних сигналів
в приміщеннях.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Мовленнєва
інформація, що передається по каналах
зв'язку, захищається від прослуховування
(закривається) з використанням методів
аналогового скремблювання та дискретизації
мови з подальшим шифруванням.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Під </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><B>скремблюванням</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
розуміється зміна характеристик мовного
сигналу таким чином, що отриманий
модульований сигнал, володіючи
властивостями нерозбірливості і
невпізнання, займає таку ж смугу частот
спектру, як і вихідний відкритий.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Зазвичай
аналогові скремблери перетворюють
вихідний мовний сигнал шляхом зміни
його частотних та часових характеристик.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Застосовуються
кілька способів частотного перетворення
сигналу:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">частотна
інверсія спектра сигналу;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">частотна
інверсія спектра сигналу зі зміщенням
несучої частоти;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">поділ смуги
частот мовного сигналу на піддіапазони
з наступною перестановкою і інверсією.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Частотна
інверсія спектра сигналу полягає в
дзеркальному відображенні спектру
вихідного сигналу відносно вибраної
частоти і f</SPAN></FONT><SUB><FONT SIZE=4><SPAN LANG="uk-UA">0</SPAN></FONT></SUB><FONT SIZE=4><SPAN LANG="uk-UA">
спектру. Внаслідок низькі частоти
перетворюються у високі, і навпаки. </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Такий спосіб
скремблювання забезпечує невисокий
рівень захисту, так як частота його
легко визначається. Пристрої, що
реалізують такий метод захисту, називають
маскіраторами.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Частотна
інверсія спектра сигналу зі зміщенням
несучої частоти забезпечує більш високий
ступінь захисту.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Спосіб частотних
перестановок полягає в поділі спектру
вихідного сигналу на піддіапазони
рівної ширини (до 10-15 піддіапазонів) з
подальшим їх перемішуванням у відповідності
з деяким алгоритмом. Алгоритм залежить
від ключа – деякого числа.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">При тимчасовому
скремблювання квант мовної інформації
(кадр) перед відправленням запам'ятовується
і розбивається на сегменти однакової
тривалості. Сегменти перемішуються
аналогічно частотним перестановкам.
При прийомі кадр піддається зворотному
перетворенню.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Комбінації
тимчасового і частотного скремблювання
дозволяють значно підвищити ступінь
захисту мовної інформації. За це
доводиться платити істотним підвищенням
складності скремблеров.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Діскретизація
мовної інформації з подальшим шифруванням
забезпечує найвищий ступінь захисту.
У процесі дискретизації мовна інформація
представляється у цифровій формі. У
такому вигляді вона перетворюється
відповідно з обраними алгоритмами
шифрування, які застосовуються для
перетворення даних в КС.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Задовільна
якість мовної інформації, передаваної
в цифровому вигляді, забезпечується
при швидкості передачі не нижче 64 кбіт
/ с. Для зниження вимог до каналів зв'язку
використовуються пристрої кодування
мови (вокодер). Спектр мовного сигналу
змінюється відносно повільно. Це дозволяє
дискретно знімати характеристики
сигналу, представляти їх в цифровому
вигляді і передавати по каналу зв'язку.
На приймальній стороні вокодер за
отриманими характеристиками реалізує
один з алгоритмів синтезу мови. Найбільшого
поширення набули вокодер з лінійним
передбаченням мови. Такі вокодери в
процесі формування мови реалізують
кусочно-лінійну апроксимацію. Застосування
вокодерів дозволяє знизити вимоги до
швидкості передачі даних до 2400 біт / с,
а з деякою втратою якості – до 800 біт /
с.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Захист акустичної
інформації в приміщеннях КС є важливим
напрямком протидії підслуховування. </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Існує кілька
                                методів захисту від прослуховування
                                акустичних сигналів:</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">звукоізоляція
і звукопоглинання акустичного сигналу;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">зашумлення
приміщень або твердого середовища для
маскування акустичних сигналів;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">захист від
несанкціонованого запису мовної
інформації на диктофон;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">виявлення і
вилучення заставних пристроїв. </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Звукоізоляція
забезпечує локалізацію джерела звуку
в замкнутому просторі. Звукоізоляційні
властивості конструкцій та елементів
приміщень оцінюються величиною ослаблення
акустичної хвилі і виражаються в
децибелах. Найбільш слабкими звукоізолюючими
властивостями в приміщеннях володіють
двері та вікна. Для посилення звукопоглинання
дверей застосовуються наступні прийоми:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; widows: 0; orphans: 0">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">усуваються
зазори і щілини за рахунок використання
ущільнювачів по периметру дверей;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">двері
покриваються додатковими звукопоглинальними
матеріалами;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">використовуються
подвійні двері з покриттям тамбурів
звукопоглинальними матеріалами.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Звукоізоляція
вікон підвищується наступними способами:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">використання
штор;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">збільшення
числа рядів стекол (ширина повітряного
проміжку між склом повинна бути не менше
200 мм);</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">застосування
поліефірних плівок (ускладнюють
прослуховування лазерним методом);</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">використання
спеціальних віконних блоків із створенням
розрідження в міжскляному просторі.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Звукопоглинання
здійснюється шляхом перетворення
кінетичної енергії звукової хвилі в
теплову енергію. Звукопоглинальні
матеріали використовуються для утруднення
прослуховування через стіни, стелю,
повітроводи вентиляції та кондиціонування
повітря, кабельні канали і тому подібні
елементи будівель. Звукопоглинальні
матеріали можуть бути суцільними і
пористими.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Активним методом
захисту є зашумлення приміщень за
допомогою генераторів акустичних
сигналів. </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Більш надійним
способом захисту акустичної інформації
є вібраційне зашумлення. Шуми звукового
діапазону створюються п'єзокерамічними
вібраторами в твердих тілах, через які
зловмисник намагається прослуховувати
приміщення. Вібратори приклеюються до
поверхні зашумлять огорожі (вікна,
стіни, стелі і т. д.) або твердотільного
звукопровода (труби водопостачання і
опалювання). Один вібратор створює
зашумлення в радіусі 1,5-5 метрів.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Для запобігання
несанкціонованого запису мовної
інформації необхідно мати засоби
виявлення працюючого диктофона і засобів
впливу на нього, у результаті якого
якість запису знижується нижче допустимого
рівня.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Несанкціонований
запис мовної інформації здійснюється
спеціальними диктофонами, в яких знижені
демаскуючі ознаки: безшумна робота
механізму протягування стрічки, відсутні
генератори підмагнічування і стирання,
використовуються екрановані головки
і т. п.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Найбільшу
інформативність має низькочастотне
пульсуюче магнітне поле працюючого
електродвигуна. Слабке поле електродвигуна
може бути виявлено на невеликій відстані.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">При виявленні
працюючого диктофона керівник може
прийняти одне з можливих рішень:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">скасувати
переговори, нараду тощо;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">не вести
конфіденційних розмов;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">використовувати
засоби, що створюють перешкоди запису
на диктофон мовної інформації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Пристрої захисту
від запису мовної інформації за допомогою
диктофона впливають створюваними ними
полями на підсилювачі запису диктофонів.
У результаті такого впливу якість запису
погіршується настільки, що неможливо
розбірливе відтворення мови. </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>5. Засоби
                            боротьби із підкладними підслуховуючими
                            пристроями</B></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Пошук і
нейтралізація закладних пристроїв для
підслуховування ускладнюється
різноманіттям їх типів.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Засоби боротьби
із заставними підслуховуючими пристроями
діляться на:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">засоби
радіоконтролю приміщень;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">засоби пошуку
неизлучающий закладок;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">засоби
придушення закладних пристроїв.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>5.1. Засоби
                            радіоконтролю приміщень</B></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Для здійснення
радіоконтролю приміщень – виявлення
радіовипромінювальних закладок –
застосовуються наступні типи пристроїв:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">індикатори
електромагнітного поля;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">побутові
радіоприймачі;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">спеціальні
радіоприймачі;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">автоматизовані
комплекси.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Індикатори
електромагнітного поля інформують про
наявність електромагнітного поля вище
фонового. Чутливість таких пристроїв
мала, і вони здатні виявляти поля
радіозакладок в безпосередній близькості
від джерела випромінювання (кілька
метрів).</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Побутові
радіоприймачі мають більшу чутливість,
ніж Виявителі поля. Основним недоліком
побутових приймачів є вузький діапазон
контрольованих частот.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Широко поширеним
типом пристроїв виявлення випромінюючих
закладок є спеціальний приймач. Серед
пристроїв цього типу найбільш
перспективними є радіоприймачі з
автоматичним скануванням радіодіапазону
і випромінювачем тестового акустичного
сигналу. Вбудований мікропроцесор
забезпечує пошук «свого» сигналу, тобто
сигналу, який видає радіозакладка при
отриманні тестового акустичного сигналу.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Найбільш
досконалими засобами виявлення
радіозакладок є автоматизовані
апаратно-програмні комплекси. Основу
таких комплексів складають спеціальний
радіоприймач і мобільна персональна
ЕОМ. Такі комплекси зберігають у пам'яті
ПЕОМ рівні і частоти радіосигналів в
контрольованому приміщенні і виявляють,
за їх наявності, закладки по зміні
спектрограм випромінювань. Автоматизовані
комплекси визначають координати
радіозакладок і містять, як правило,
також блок контролю проводових ліній.
Всі операції автоматизовані, тому такі
комплекси є багатофункціональними і
можуть використовуватися безперервно.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>5.2. Засоби
                            пошуку невипромінюючих закладок</B></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Для виявлення
не випромінюючих закладок використовуються:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">засоби
контролю проводових ліній;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">засоби
виявлення елементів закладок.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Найбільш
поширеними провідними лініями, за якими
закладні пристрої передають інформацію,
є телефонні лінії і лінії електроживлення,
а також лінії пожежної та охоронної
сигналізації, лінії селекторного
зв'язку. Принцип роботи апаратури
контролю проводових ліній заснований
на тому, що будь-яке підключення до них
викликає зміну електричних параметрів
ліній, таких як напруга, струм, опір,
ємність і індуктивність. Апаратура
контролю встановлює також наявність
позаштатних електричних сигналів в
лінії. Дані можуть підключатися до ліній
паралельно і послідовно. При паралельному
підключенні і високому вхідному опорі
закладок виявити їх дуже складно. Для
підвищення чутливості засобів контролю
збільшують число вимірюваних параметрів,
вводять статистичну обробку результатів
вимірювань. Деякі пристрої контролю
дозволяють визначати довжину ділянки
провідної лінії до закладки. Ці пристрої
використовують властивість сигналу
відбиватися від неоднорідностей, які
створюються в місцях фізичного
підключення.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Для виявлення
закладок, в тому числі і тих, що знаходяться
у непрацюючому стані, використовуються
наступні засоби:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">пристрої
нелінійної локації;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Виявителі
пустот;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">металодетектори;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">рентгенівські
установки.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">У пристроях
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>нелінійної
                                локації</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
використовуються нелінійні властивості
напівпровідників. При опроміненні
напівпровідників високочастотним
електромагнітним випромінюванням з
частотою f0 у відбитих хвилях з'являються
гармоніки з частотами, кратними f0 –
2f0, 3fo і т. д. Амплітуда відбитих хвиль
різко зменшується з ростом кратності
частоти. </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Для прихованого
розміщення закладок в елементах
конструкцій будівель, в меблях та інших
суцільних фізичних середовищах необхідно
створити закамуфльовані поглиблення,
отвори і т. п. Такі зміни конструкцій є
демаскуючі ознакою закладки. Тому
можливий непрямий пошук закладок </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>шляхом
                                пошуку порожнеч в суцільних фізичних
                                середовищах</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">.
При виявленні порожнеч вони можуть бути
обстежені більш ретельно іншими засобами
контролю.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Порожнечі в
суцільних середовищах виявляються з
використанням пристроїв, принцип дії
яких ґрунтується на різних фізичних
властивостях порожнеч:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">зміна характеру
поширення звуку;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">відмінність
у значеннях діелектричної проникності;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">відмінність
в теплопровідності середовища і
порожнечі.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Порожнечі
виявляються простим простукуванням
суцільних середовищ. Для цієї ж мети
використовуються ультразвукові прилади.
Електричне поле деформується пустотами
за рахунок різниці діелектричних
властивостей середовища і порожнечі.
Це властивість електричного поля
використовується для пошуку порожнеч.
Порожнечі виявляються також по різниці
температур за допомогою тепловізорів.
Такі прилади здатні фіксувати різницю
температур 0,05 ° С.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Принцип дії
метаплодетекторов заснований на
використанні властивостей провідників
взаємодіяти із зовнішнім електричним
і магнітним полем. Будь закладка містить
провідники: резистори, шини, корпус
елементів живлення і самої закладки та
ін..</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">При впливі
електромагнітного поля в провідниках
об'єкта виникають вихрові струми. Поля,
створювані цими струмами, посилюються
і потім аналізуються мікропроцесором
метало-детектора. Відстань, з якої
виявляється об'єкт, залежить від розмірів
провідника і типу металодетектора. </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Рідше
використовуються для пошуку закладок
переносні рентгенівські установки.
Використовуються такі установки для
контролю нерозбірних предметів.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>5.3. Засоби
                            придушення закладних пристроїв</B></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Виявлену
закладку можна вилучити, використовувати
для дезінформації або придушити. Під
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>придушенням</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
розуміється такий вплив на закладку, в
результаті якого вона не здатна виконувати
покладені на неї функції. </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><U>Для
                                придушення закладок використовуються</U></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">генератори
перешкод;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">засоби
порушення функціонування закладок;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">засоби
руйнування закладок.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Генератори
використовуються для придушення сигналів
закладок як у лініях, так і для просторового
зашумлення радіозакладок. Генератори
створюють сигнали перешкод, що перекривають
по частоті діапазони частот, на яких
працюють закладки. Амплітуда сигналу-завади
повинна в кілька разів перевищувати
амплітуду сигналів закладки.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Засоби порушення
роботи закладки впливають на закладку
з метою зміни режимів її роботи, зміни
умов функціонування.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Руйнування
закладок без їх вилучення здійснюється
у лініях (телефонного, гучного зв'язку,
електроживлення і т. п.) шляхом подачі
коротких імпульсів високої напруги (до
4000 В). Попередньо від ліній відключаються
всі кінцеві радіоелектронні пристрої.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; page-break-after: avoid">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>6. Захист від
                            зловмисних дій обслуговуючого персоналу
                            і користувачів</B></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">За статистикою
80% випадків злочинних дій на інформаційні
ресурси скоюються людьми, що мають
безпосереднє відношення до експлуатації
КС. Такі дії відбуваються або під впливом
злочинних груп (розвідувальних служб),
або спонукаються внутрішніми причинами
(заздрість, помста, корисливість і т.
п.). Для блокування загроз такого типу
керівництво організації за допомогою
служби безпеки має здійснювати наступні
організаційні заходи:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; widows: 0; orphans: 0">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">здобувати
всіма доступними законними шляхами
інформацію про своїх співробітників,
про людей або організації, що представляють
потенційну загрозу інформаційним
ресурсам;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">забезпечувати
охорону співробітників;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">встановлювати
розмежування доступу до ресурсів, що
захищаються;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">контролювати
виконання встановлених заходів безпеки;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">створювати
і підтримувати в колективі здоровий
моральний клімат.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Керівництво
повинно володіти, по можливості, повною
інформацією про спосіб життя своїх
співробітників. Основну увагу при цьому
слід звертати на отримання інформації
про найближче оточення, про відповідність
легальних доходів і витрат, про наявність
шкідливих звичок, про негативні риси
характеру, про стан здоров'я, про ступінь
задоволеності професійною діяльністю
і займаною посадою. Для отримання такої
інформації використовуються співробітники
служби безпеки, психологи, керівний
склад установи. З цією ж метою здійснюється
взаємодія з органами МВС і спецслужбами.
Збір інформації необхідно вести, не
порушуючи закони і права особистості.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Поза межами
об'єкта охороняються, як правило, тільки
керівники і співробітники, яким реально
загрожує вплив зловмисників.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">В організації,
що працює з конфіденційною інформацією,
обов'язковим є розмежування доступу до
інформаційних ресурсів. У разі зради
або інших зловмисних дій співробітника
збиток повинен бути обмежений рамками
його компетенції. Співробітники установи
повинні знати, що виконання встановлених
правил контролюється керівництвом і
службою безпеки.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=LEFT STYLE="text-indent: 0in"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Далеко
не останню роль у блокуванні загроз
даного типу відіграє моральний клімат
в колективі. В ідеалі кожен співробітник
є патріотом колективу, дорожить своїм
місцем, його ініціатива і відмінності
цінуються керівництвом.</SPAN></FONT></FONT></P>
    </div></div>
</BODY>
</HTML>
