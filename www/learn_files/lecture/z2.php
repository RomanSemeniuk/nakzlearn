<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
    <TITLE></TITLE>
    <META NAME="GENERATOR" CONTENT="LibreOffice 4.1.6.2 (Linux)">
    <META NAME="AUTHOR" CONTENT="Naks ®">
    <META NAME="CREATED" CONTENT="20151011;142500000000000">
    <META NAME="CHANGEDBY" CONTENT="Naks ®">
    <META NAME="CHANGED" CONTENT="20151011;142500000000000">
    <META NAME="AppVersion" CONTENT="14.0000">
    <META NAME="Company" CONTENT="SPecialiST RePack">
    <META NAME="DocSecurity" CONTENT="0">
    <META NAME="HyperlinksChanged" CONTENT="false">
    <META NAME="LinksUpToDate" CONTENT="false">
    <META NAME="ScaleCrop" CONTENT="false">
    <META NAME="ShareDoc" CONTENT="false">
    <STYLE TYPE="text/css">
        <!--
        @page { margin-left: 0.98in; margin-right: 0.59in; margin-top: 0.59in; margin-bottom: 0.59in }
        P { text-indent: 0.3in; margin-bottom: 0in; direction: ltr; line-height: 100%; text-align: justify; widows: 2; orphans: 2 }
        P.western { font-family: "Times New Roman", serif; font-size: 12pt; so-language: ru-RU }
        P.cjk { font-family: "Times New Roman"; font-size: 12pt; so-language: ru-RU }
        P.ctl { font-family: "Times New Roman"; font-size: 10pt }
        H2 { margin-top: 0.08in; margin-bottom: 0.04in; direction: ltr; line-height: 100%; widows: 2; orphans: 2 }
        H2.western { font-family: "Peterburg", serif; font-size: 11pt; so-language: uk-UA }
        H2.cjk { font-family: "Times New Roman"; font-size: 11pt; so-language: ru-RU }
        H2.ctl { font-family: "Times New Roman"; font-size: 10pt; font-weight: normal }
        A:link { color: #0000ff; so-language: zxx }
        -->
    </STYLE>
</HEAD>
<BODY LANG="uk-UA" LINK="#0000ff" DIR="LTR">
<div id="wrapper">
   <scrip><?php include('../toplearn.ini.php');?></scrip>
    <div id="content">
        <H2 CLASS="western" ALIGN=CENTER STYLE="page-break-after: auto"><A NAME="_Toc365048718"></A>
            <FONT FACE="Times New Roman, serif"><FONT SIZE=5 STYLE="font-size: 20pt">Загрози
                    безпеки інформації в комп'ютерних
                    системах</FONT></FONT></H2>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">З позиції
забезпечення безпеки інформації в КС
такі системи доцільно розглядати у
вигляді єдності трьох компонент, що
роблять взаємний вплив один на одного:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">інформація;
</SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">технічні і
програмні засоби;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">обслуговуючий
персонал і користувачі. </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <IMG SRC="learn_files/z2/U-2.png" ALIGN=LEFT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=CENTER STYLE="text-indent: 0in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I>Рис. 1.2. Загрози
                            безпеки інформації в КС</I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Метою
                                створення</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
будь-якої КС є задоволення потреб
користувачів в своєчасному здобутті
достовірної інформації і збереженні
її конфіденційності (при необхідності).
Інформація є кінцевим &quot;продуктом
вжитку&quot; в КС і виступає у вигляді
центральної компоненти системи. Безпеку
інформації на рівні КС забезпечують
дві інші компоненти системи. Причому
це завдання повинне вирішуватися шляхом
захисту від зовнішніх і внутрішніх
недозволених (несанкціонованих) дій.
Особливості взаємодії компонентів
полягають у наступному. Зовнішні дії
найчастіше роблять несанкціонований
вплив на інформацію шляхом дії на інші
компоненти системи. Наступною особливістю
є можливість несанкціонованих дій, що
викликаються внутрішніми причинами,
відносно інформації з боку технічних,
програмних засобів, обслуговуючого
персоналу і користувачів. У цьому полягає
основне протиріччя взаємодії цих
компонентів з інформацією. Причому,
обслуговуючий персонал і користувачі
можуть свідомо здійснювати спроби
несанкціонованої дії на інформацію.
Таким чином, забезпечення безпеки
інформації в КС повинне передбачати
захист усіх компонентів від зовнішніх
і внутрішніх дій (загроз). </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Під </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>загрозою
                                безпеки інформації</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
розуміється потенційно можлива подія,
процес або явище, які можуть привести
до знищення, втрати цілісності,
конфіденційності або доступності
інформації. </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Вся безліч
потенційних загроз безпеці інформації
в КС може бути розділене на два класи
(див. рис. 1.2).</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>1. Випадкові
                                загрози. </B></I></SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Загрози, які
не пов'язані з навмисними діями
зловмисників і реалізуються у випадкові
моменти часу, називають </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>випадковими</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Реалізація
загроз цього класу приводить до найбільших
втрат інформації (за статистичними
даними – до 80% від збитку, що наноситься
інформаційним ресурсам КС будь-якими
загрозами). При цьому можуть відбуватися
знищення, порушення цілісності і
доступності інформації. Рідше порушується
конфіденційність інформації, проте при
цьому створюються передумови для
зловмисної дії на інформацію.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Стихійні лиха
і аварії несуть найбільш руйнівні
наслідки для КС, оскільки останні
піддаються фізичному руйнуванню,
інформація втрачається або доступ до
неї стає неможливий.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Збої і відмови
складних систем неминучі. В результаті
збоїв і відмов порушується працездатність
технічних засобів, знищуються і
спотворюються дані і програми, порушується
алгоритм роботи пристроїв. Порушення
алгоритмів роботи окремих вузлів і
пристроїв можуть також призвести до
порушення конфіденційності інформації.
Наприклад, збої і відмови засобів видачі
інформації можуть призвести до
несанкціонованого доступу до інформації
шляхом несанкціонованої її видачі в
канал зв'язку, на принтер тощо.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Помилки при
розробці КС, алгоритмічні і програмні
помилки приводять до наслідків,
аналогічних наслідків збоїв і відмов
технічних засобів. Крім того, такі
помилки можуть бути використані
зловмисниками для дії на ресурси КС.
Особливу небезпеку представляють
помилки в операційних системах (ОС) і в
програмних засобах захисту інформації.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Згідно даних
Національного Інституту Стандартів і
Технологій США (NIST) 65% випадків порушення
безпеки інформації відбувається в
результаті помилок користувачів і
обслуговуючого персоналу. Некомпетентне,
недбале або неуважне виконання
функціональних обов'язків співробітниками
приводять до знищення, порушення
цілісності і конфіденційності інформації.
Характеризуючи загрози інформації в
КС, що не пов'язані з навмисними діями,
в цілому, слід зазначити, що механізм
їх реалізації вивчений досить добре, а
також накопичений значний досвід
протидії цим загрозам. Сучасна технологія
розробки технічних і програмних засобів,
ефективна система експлуатації КС, що
включає обов'язкове резервування
інформації, дозволяють значно понизити
втрати від реалізації загроз цього
класу.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>2. Навмисні
                                загрози </B></I></SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Другий клас
загроз безпеці інформації в КС складають
навмисно створювані загрози. </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Даний клас
загроз вивчений недостатньо, дуже
динамічний і постійно поповнюється
новими загрозами. Загрози цього класу
відповідно до їх фізичної суті і
механізмів реалізації можуть бути
розподілені на п'ять груп:</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">традиційне
або універсальне шпигунство і диверсії;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">несанкціонований
доступ до інформації; </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">електромагнітні
випромінювання і наведення;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">модифікація
структур КС; </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">шкідливі
програми.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>2.1. Традиційне
                                шпигунство і диверсії </B></I></SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Як джерела
небажаної дії на інформаційні ресурси
як і раніше актуальні методи і засоби
шпигунства і диверсій, які використовувалися
і використовуються для добування або
знищення інформації на об'єктах, КС. Ці
методи також дієві і ефективні в умовах
використання комп'ютерних систем.
Найчастіше вони використовуються для
здобуття відомостей про систему захисту
з метою проникнення в КС, а також для
розкрадання і знищення інформаційних
ресурсів.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">До методів
шпигунства і диверсій відносяться: </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">підслуховування;
</SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">візуальне
спостереження; </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">розкрадання
документів і машинних носіїв інформації;
</SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">розкрадання
програм і атрибутів системи захисту; </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">підкуп і
шантаж співробітників; </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">збір і аналіз
відходів машинних носіїв інформації; </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">підпали; </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">вибухи.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Для підслуховування
зловмисникові не обов'язково проникати
на об'єкт. Сучасні засоби дозволяють
підслуховувати розмови з відстані
декількох сотень метрів. Так пройшла
випробування система підслуховування,
що дозволяє з відстані 1 км фіксувати
розмову в приміщенні із закритими
вікнами. У міських умовах дальність дії
пристрою скорочується до сотень і
десятків метрів залежно від рівня
фонового шуму. Принцип дії таких пристроїв
заснований на аналізі відбитого променя
лазера від скла вікна приміщення, яке
коливається від звукових хвиль. Коливання
шибок від акустичних хвиль у приміщенні
можуть зніматися і передаватися на
відстані за допомогою спеціальних
пристроїв, укріплених на шибці. Такі
пристрої перетворять механічні коливання
скла в електричний сигнал з подальшою
передачею його по радіоканалу. Поза
приміщеннями підслуховування ведеться
за допомогою надчутливих направлених
мікрофонів. Реальна відстань підслуховування
за допомогою направлених мікрофонів
складає 50-100 метрів.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Розмови в
сусідніх приміщеннях, за стінами будівель
можуть контролюватися за допомогою
стетоскопічних мікрофонів. Стетоскопи
перетворюють акустичні коливання в
електричні. Такі мікрофони дозволяють
прослухувати розмови при товщині стін
до 50-100 см. Знімання інформації може
здійснюватися також із скла,
металоконструкцій будівель, труб
водопостачання і опалювання.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Аудіоінформація
може бути отримана також шляхом
високочастотного нав'язування. Суть
цього методу полягає в дії високочастотним
електромагнітним полем або електричними
сигналами на елементи, що здатні
модулювати ці поля. Як такі, елементи
можуть використовувати різні порожнини
з електропровідною поверхнею, що є
високочастотним контуром з розподіленими
параметрами, які змінюються під дією
акустичних хвиль. При збігу частоти
такого контура з частотою високочастотного
нав'язування і за наявності дії акустичних
хвиль на поверхню порожнини контур
перевипромінює і модулює зовнішнє поле
(високочастотний електричний сигнал).
Найчастіше цей метод прослухування
реалізується за допомогою телефонної
лінії. При цьому як модулюючий елемент
використовується телефонний апарат,
на який по телефонних дротах подається
високочастотний електричний сигнал.
Нелінійні елементи телефонного апарату
під впливом мовного сигналу модулюють
високочастотний сигнал. Модульований
високочастотний сигнал може демодулюватися
в приймачі зловмисника.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Одним з можливих
каналів просочування звукової інформації
може бути прослухування переговорів,
що ведуться за допомогою засобів зв'язку.
Контролюватися можуть як дротяні канали
зв'язку, так і радіоканали. Прослухування
переговорів по дротяних і радіоканалам
не вимагає дорогого устаткування і
високої кваліфікації зловмисника.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Дистанційна
відеорозвідка для здобуття інформації
в КС малопридатна і носить, як правило,
допоміжний характер.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Відеорозвідка
організовується в основному для виявлення
режимів роботи і розташування механізмів
захисту інформації. З КС інформація
реально може бути отримана при використанні
на об'єкті екранів, табло, плакатів, якщо
є прозорі вікна і перераховані вище
засоби розміщені без врахування
необхідності протидіяти такій загрозі.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Відеорозвідка
може вестися з використанням технічних
засобів, таких як оптичні прилади, фото-,
кіно- і телеапаратура. Багато з цих
засобів допускають консервацію
(запам'ятовування) відеоінформації, а
також передачу її на певну відстань.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">У пресі з'явилися
повідомлення про створення в США
мобільного мікроробота для ведення
дистанційної розвідки. Пьезокерамічний
робот розміром близько 7 см і масою 60 г
здатний самостійно пересуватися із
швидкістю 30 см/с протягом 45 хв. За цей
час &quot;мікророзвідник&quot; здатний
здолати відстань в 810 метрів, здійснюючи
транспортування 28 г корисного вантажу
(для порівняння – комерційна
мікровідеокамера важить 15 г).</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Для вербування
співробітників і фізичного знищення
об'єктів КС також не обов'язково мати
безпосередній доступ на об'єкт. Зловмисник,
що має доступ на об'єкт КС, може
використовувати будь-який з методів
традиційного шпигунства.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Зловмисниками,
котрі мають доступ на об'єкт, можуть
використовуватися мініатюрні засоби
фотографування, відео – і аудіозаписи.
Для аудіо-та відеоконтролю приміщень
і при відсутності в них зловмисника
можуть використовуватися закладні
пристрої або &quot;жучки&quot;. Для об'єктів
КС найбільш ймовірними є закладні
пристрої, що забезпечують прослуховування
приміщень. Закладні пристрої діляться
на дротові і випромінюючі. Дротяні
заставні пристрої вимагають значного
часу на установку і мають істотну
демаскуючу ознаку – дроти. Випромінюючі
&quot;закладки&quot; (&quot;радіозакладки&quot;)
швидко встановлюються, але також мають
демаскуючу ознаку – випромінювання в
радіо або оптичному діапазоні.
&quot;Радіозакладки&quot; можуть використовувати
як джерело електричні або акустичні
сигнали. Прикладом використання
електричних сигналів як джерела є
вживання сигналів внутрішнього
телефонного, гучномовного зв'язку.
Найбільшого поширення набули акустичні
&quot;радіозакладки&quot;. Вони сприймають
акустичний сигнал, перетворюють його
в електричний і передають у вигляді
радіосигналу на дальність до 8 км. З
вживаних на практиці &quot;радіозакладок&quot;
переважна більшість (близько 90%)
розраховані на роботу в діапазоні
відстаней 50 – 800 метрів.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Для деяких
об'єктів КС існує загроза озброєного
нападу терористичних або диверсійних
груп. При цьому можуть бути застосовані
засоби вогневої поразки. </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>2.2.
                                Несанкціонований доступ до інформації</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Термін
&quot;</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>несанкціонований
                                доступ до інформації</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">&quot;
(НСДІ) визначений як доступ до інформації,
що порушує правила розмежування доступу
з використанням штатних засобів
обчислювальної техніки або автоматизованих
систем.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Під правилами
розмежування доступу розуміється
сукупність положень, що регламентують
права доступу осіб або процесів (суб'єктів
доступу) до одиниць інформації (об'єктам
доступу).</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Право доступу
до ресурсів КС визначається керівництвом
для кожного співробітника відповідно
до його функціональних обов'язків.
Процеси ініціюються в КС на користь
певних осіб, тому і на них накладаються
обмеження по доступу до ресурсів.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Виконання
встановлених правил розмежування
доступу в КС реалізується за рахунок
створення системи розмежування доступу
(СРД), яку детально ми розглянемо пізніше.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Несанкціонований
доступ до інформації можливий лише з
використанням штатних апаратних і
програмних засобів в наступних випадках:
</SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">відсутня
система розмежування доступу; </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">збій або
відмова в КС; </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">помилкові
дії користувачів або обслуговуючого
персоналу комп'ютерних систем; </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">помилки в
СРД; </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">фальсифікація
повноважень.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Якщо СРД
відсутній, то зловмисник, що має навики
роботи в КС, може дістати без обмежень
доступ до будь-якої інформації. В
результаті збоїв або відмов засобів
КС, а також помилкових дій обслуговуючого
персоналу і користувачів можливі стани
системи, при яких спрощується НСДІ.
Зловмисник може виявити помилки в СРД
і використовувати їх для НСДІ. Фальсифікація
повноважень є одним з найбільш вірогідних
доріг (каналів) НСДІ.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>2.3.
                                Електромагнітні випромінювання і
                                наведення</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Процес обробки
і передачі інформації технічними
засобами КС супроводжується
електромагнітними випромінюваннями в
навколишній простір і наведенням
електричних сигналів в лініях зв'язку,
сигналізації, заземленні і інших
провідниках. Вони отримали назви побічних
електромагнітних випромінювань і
наведень (ПЕВІН). За допомогою спеціального
устаткування сигнали приймаються,
виділяються, посилюються і можуть або
бути видимими, або записуватися в
запам'ятовуючих пристроях. Найбільший
рівень електромагнітного випромінювання
в КС властивий працюючим пристроям
відображення інформації на
електронно-променевих трубках. Вміст
екрану такого пристрою може бути видимим
за допомогою звичайного телевізійного
приймача, доповненого нескладною схемою,
основною функцією якої є синхронізація
сигналів. Використання направленої
антени приймача дозволяє збільшити
зону упевненого прийому сигналів до 1
км. Відновлення даних можливе також
шляхом аналізу сигналів випромінювання
неекранованого електричного кабелю на
відстані до 300 метрів.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Наведені в
провідниках електричні сигнали можуть
виділятися і фіксуватися за допомогою
устаткування, що підключається до цих
провідників на відстані в сотні метрів
від джерела сигналів. Для добування
інформації зловмисник може використовувати
також &quot;просочування&quot; інформаційних
сигналів в ланцюзі електроживлення
технічних засобів КС.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">&quot;Просочування&quot;
інформаційних сигналів в ланцюзі
електроживлення можливо за наявності
магнітного зв'язку між вихідним
трансформатором підсилювача і
трансформатором випрямного пристрою.
&quot;Просочування&quot; також можливо за
рахунок падіння напруги на внутрішньому
опорі джерела живлення при проходженні
струмів підсилюваних інформаційних
сигналів. Якщо загасання у фільтрі
випрямного пристрою недостатньо, то
інформаційні сигнали можуть бути
виявлені в ланцюзі живлення. Інформаційний
сигнал може бути виділений в ланцюзі
живлення за рахунок залежності значень
споживаного струму в крайових каскадах
підсилювачів (інформаційні сигнали) і
значень струмів у випрямлячах, а значить
і у вихідних ланцюгах.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Електромагнітні
випромінювання використовуються
зловмисниками не лише для здобуття
інформації, але і для її знищення.
Електромагнітні імпульси здатні знищити
інформацію на магнітних носіях. </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; page-break-after: avoid">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>2.4.
                                Несанкціонована модифікація структур</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Велику загрозу
безпеці інформації в КС являє
несанкціонована модифікація алгоритмічної,
програмної і технічної структур системи.
Несанкціонована модифікація структур
може здійснюватися на будь-якому
життєвому циклі КС. Несанкціонована
зміна структури КС на етапах розробки
і модернізації отримала назву &quot;</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>закладка</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">&quot;.
В процесі розробки КС &quot;закладки&quot;
упроваджуються, як правило, в спеціалізовані
системи, призначені для експлуатації
в якій-небудь фірмі або державних
установах. В універсальні &quot;закладки&quot;
КС упроваджуються рідше, в основному
для дискредитації таких систем конкурентом
або на державному рівні, якщо передбачаються
постачання КС у ворожу державу. &quot;Закладки&quot;,
упроваджені на етапі розробки, складно
виявити зважаючи на високу кваліфікацію
їх авторів і складнощі сучасних КС.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Алгоритмічні,
програмні і апаратні &quot;закладки&quot;
використовуються або для безпосередньої
шкідливої дії на КС, або для забезпечення
неконтрольованого входу в систему.
Шкідливі дії &quot;закладок&quot; на КС
здійснюються при здобутті відповідної
команди ззовні (в основному характерний
для апаратних &quot;закладок&quot;) і при
настанні певних подій в системі. Такими
подіями можуть бути: перехід на певний
режим роботи (наприклад, бойовий режим
системи управління зброєю або режим
усунення аварійної ситуації на атомній
електростанції тощо), настання встановленої
дати, досягнення певного напрацювання
і так далі.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Програмні і
апаратні &quot;закладки&quot; для здійснення
неконтрольованого входу в програми,
використання привілейованих режимів
роботи (наприклад, режимів операційної
системи), обходу засобів захисту
інформації отримали назву &quot;люки&quot;.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>2.5. Шкідливі
                                програми</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Одним з основних
джерел загроз безпеці інформації в КС
є використання спеціальних програм, що
отримали загальну назву &quot;Шкідливі
програми&quot;. Залежно від механізму дії
шкідливі програми діляться на чотири
класи: </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">&quot;логічні
бомби&quot;; </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">&quot;черв'яки&quot;;
</SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">&quot;троянські
коні&quot;; </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">&quot;комп'ютерні
віруси&quot;.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">&quot;Логічні
бомби&quot; – це програми або їх частини,
що постійно знаходяться в ЕОМ або
обчислювальних системах (ОСс) і виконувані
лише при дотриманні певних умов.
Прикладами таких умов можуть бути:
настання заданої дати, перехід КС в
певний режим роботи, настання деяких
подій встановлене число разів і тому
подібне.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">&quot;Черв'яками&quot;
називаються програми, які виконуються
кожного разу при завантаженні системи,
володіють здатністю переміщатися у ОСс
або мережі і самовідтворювати копії.
Лавиноподібне розмноження програм
приводить до перевантаження каналів
зв'язку, пам'яті і, зрештою, до блокування
системи.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">&quot;Троянські
коні&quot; – це програми, отримані шляхом
явної зміни або додавання команд у
призначені для користувача програми.
При подальшому виконанні призначених
для користувача програм поряд із заданими
функціями виконуються несанкціоновані,
змінені або якісь нові функції.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">&quot;Комп'ютерні
віруси&quot; – це невеликі програми, які
після впровадження в ЕОМ самостійно
поширюються шляхом створення своїх
копій, а при виконанні певних умов
надають негативну дію на КС.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Оскільки вірусам
характерні властивості всіх класів
шкідливих програм, то останнім часом
будь-які шкідливі програми часто
називають вірусами.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>2.6. Класифікація
                                зловмисників</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Можливості
здійснення шкідливих дій у великій мірі
залежать від статусу зловмисника по
відношенню до КС. Зловмисником може
бути: </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">розробник
КС; </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">співробітник
з числа обслуговуючого персоналу;</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">користувач;
</SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">стороння
особа.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Розробник
володіє якнайповнішою інформацією про
програмні і апаратні засоби КС і має
можливість впровадження &quot;закладок&quot;
на етапах створення і модернізації
систем. Але він, як правило, не дістає
безпосереднього доступу на експлуатовані
об'єкти КС. Користувач має загальне
уявлення про структури КС, про роботу
механізмів захисту інформації. Він може
здійснювати збір даних про систему
захисту інформації методами традиційного
шпигунства, а також робити спроби
несанкціонованого доступу до інформації.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Можливості
впровадження &quot;закладок&quot; користувачами
дуже обмежені. Стороння особа, що не має
відношення до КС, знаходиться в найменш
вигідному положенні по відношенню до
інших зловмисників. Якщо передбачити,
що вона не має доступу на об'єкт КС, то
в її розпорядженні є дистанційні методи
традиційного шпигунства і можливість
диверсійної діяльності. Вона може
здійснювати шкідливі дії з використанням
електромагнітних випромінювань і
наведень, а також каналів зв'язку, якщо
КС є розподіленою.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Великі можливості
надання шкідливих дій на інформацію КС
мають фахівці, що обслуговують ці
системи. Причому, фахівці різних
підрозділів володіють різними потенційними
можливостями зловмисних дій. Найбільшої
шкоди можуть завдати працівники служби
безпеки інформації. Далі йдуть системні
програмісти, прикладні програмісти і
інженерно-технічний персонал.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">На практиці
небезпека зловмисника залежить також
від фінансових, матеріально-технічних
можливостей і кваліфікації зловмисника.</SPAN></FONT></FONT></P>
    </div></div>
</BODY>
</HTML>
