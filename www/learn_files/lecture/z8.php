<!--#include virtual="/topMenu.php" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
    <TITLE></TITLE>
    <META NAME="GENERATOR" CONTENT="LibreOffice 4.1.6.2 (Linux)">
    <META NAME="AUTHOR" CONTENT="Naks ®">
    <META NAME="CREATED" CONTENT="20151011;142900000000000">
    <META NAME="CHANGEDBY" CONTENT="Naks ®">
    <META NAME="CHANGED" CONTENT="20151011;142900000000000">
    <META NAME="AppVersion" CONTENT="14.0000">
    <META NAME="Company" CONTENT="SPecialiST RePack">
    <META NAME="DocSecurity" CONTENT="0">
    <META NAME="HyperlinksChanged" CONTENT="false">
    <META NAME="LinksUpToDate" CONTENT="false">
    <META NAME="ScaleCrop" CONTENT="false">
    <META NAME="ShareDoc" CONTENT="false">
    <STYLE TYPE="text/css">
        <!--
        @page { margin-left: 0.98in; margin-right: 0.59in; margin-top: 0.59in; margin-bottom: 0.59in }
        P { text-indent: 0.3in; margin-bottom: 0in; direction: ltr; line-height: 100%; text-align: justify; widows: 2; orphans: 2 }
        P.western { font-family: "Times New Roman", serif; font-size: 12pt; so-language: ru-RU }
        P.cjk { font-family: "Times New Roman"; font-size: 12pt; so-language: ru-RU }
        P.ctl { font-family: "Times New Roman"; font-size: 10pt }
        H2 { margin-top: 0.08in; margin-bottom: 0.04in; direction: ltr; line-height: 100%; widows: 2; orphans: 2 }
        H2.western { font-family: "Peterburg", serif; font-size: 11pt; so-language: uk-UA }
        H2.cjk { font-family: "Times New Roman"; font-size: 11pt; so-language: ru-RU }
        H2.ctl { font-family: "Times New Roman"; font-size: 10pt; font-weight: normal }
        A:link { color: #0000ff; so-language: zxx }
        -->
    </STYLE>
</HEAD>
<BODY LANG="uk-UA" LINK="#0000ff" DIR="LTR">
<div id="wrapper">
    <scrip><?php include('../toplearn.ini.php');?></scrip>
    <div id="content">
        <H2 CLASS="western" ALIGN=CENTER STYLE="widows: 0; orphans: 0; page-break-after: auto"><A NAME="_Toc365048722"></A>
            <FONT SIZE=5 STYLE="font-size: 20pt">Методи та засоби
                захисту від електромагнітних випромінювань
                і наведень</FONT></H2>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Всі
методи захисту від електромагнітних
випромінювань і наведень можна розділити
на пасивні і активні.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in">
            <IMG SRC="learn_files/z8/i_89be1f021d68b197_html_5b363745.gif"><IMG SRC="learn_files/z8/U-4.png" ALIGN=LEFT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=CENTER STYLE="text-indent: 0in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I>Рис. 1.4. Види
                            писавних методів захисту від ПЕМВН</I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=3><FONT SIZE=4><SPAN LANG="uk-UA"><B>1.
                                    Пасивні методи захисту від побічних
                                    електромагнітних випромінювань і
                                    наведень (ПЕМВН)</B></SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Пасивні
методи забезпечують зменшення рівня
небезпечного сигналу або зниження
інформативності сигналів.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Активні
методи захисту спрямовані на створення
перешкод у каналах побічних електромагнітних
випромінювань і наведень, що ускладнюють
прийом і виділення корисної інформації
з перехоплених зловмисником сигналів.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Для
блокування загрози впливу на електронні
блоки та магнітні запам'ятовуючі пристрої
потужними зовнішніми електромагнітними
імпульсами і високочастотними
випромінюваннями, що приводять до
несправності електронних блоків і
стирання інформації з магнітних носіїв
інформації, використовується екранування
захищаються засобів.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Захист
від побічних електромагнітних
випромінювань і наведень здійснюється
як пасивними, так і активними методами.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Пасивні
методи захисту від ПЕМВН можуть бути
розбиті на три групи:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><I><FONT SIZE=4><SPAN LANG="uk-UA"><B>1.1.
                                        Екранування </B></SPAN></FONT></I></FONT></FONT></FONT>
        </P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Екранування
є одним з найефективніших методів
захисту від електромагнітних випромінювань.
Під екрануванням розуміється розміщення
елементів КС, що створюють електричні,
магнітні і електромагнітні поля, в
просторово замкнутих конструкціях.
Способи екранування залежать від
особливостей полів, створюваних
елементами КС при протіканні в них
електричного струму.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Характеристики
полів залежать від параметрів електричних
сигналів в КС. Так при малих струмах і
високих напругах в створюваному поле
переважає електрична складова. Таке
поле називається електричним
(електростатичним). Якщо в провіднику
протікає струм великої величини при
малих значеннях напруги, то в полі
переважає магнітна складова, а поле
називається магнітним. Поля, у яких
електрична і магнітна складові сумісні,
називаються електромагнітними.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">В
залежності від типу створюваного
електромагнітного поля розрізняють
наступні види екранування:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">екранування
електричного поля;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">екранування
магнітного поля;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">екранування
електромагнітного поля. </SPAN></FONT></FONT></FONT></FONT>
        </P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Екранування
електричного поля заземленим металевим
екраном забезпечує нейтралізацію
електричних зарядів, які стікають по
заземлювального контуру. Контур
заземлення повинен мати опір не більше
4 Ом. Електричне поле може екранувати і
за допомогою діелектричних екранів, що
мають високу відносну діелектричну
проникність. При цьому поле послаблюється
у 8 разів.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">При
екранування магнітних полів розрізняють
низькочастотні магнітні поля (до 10 кГц)
і високочастотні магнітні поля. .</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Високочастотне
магнітне поле викликає виникнення в
екрані змінних індукційних вихрових
струмів, які створюваним ним магнітним
полем перешкоджають поширенню побічного
магнітного поля. Заземлення не впливає
на екранування магнітних полів. Поглинаюча
здатність екрану залежить від частоти
побічного випромінювання і від матеріалу,
з якого виготовляється екран. Чим нижче
частота випромінювання, тим більшою
повинна бути товщина екрана. Для
випромінювань в діапазоні середніх
хвиль і вище достатньо ефективним є
екран товщиною 0,5-1,5 мм. Для випромінювань
на частотах понад 10 МГц досить мати
екран з міді або срібла товщиною 0,1 мм.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Електромагнітні
випромінювання блокуються методами
високочастотного електричного і
магнітного екранування.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Екранування
здійснюється на п'яти рівнях:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">рівень
елементів схем;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">рівень
блоків;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">рівень
пристроїв;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">рівень
кабельних ліній;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">рівень
приміщень.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Елементи
схем з високим рівнем побічних
випромінювань можуть поміщатися в
металеві або металізовані напиленням
заземленні корпуси. Починаючи з рівня
блоків, екранування здійснюється за
допомогою конструкцій з листової сталі,
металевих сіток і напилення. Екранування
кабелів здійснюється за допомогою
металевої сітки, сталевих коробів або
труб.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">При
екрануванні приміщень використовуються:
листова сталь товщиною до 2 мм, сталева
(мідна, латунна) сітка з осередком до
2,5 мм. У захищених приміщеннях екрануються
двері та вікна. Вікна екрануються сіткою,
металізованими шторами, металізацією
вікон і обклеюванням їх струмопровідними
плівками. Двері виконуються із сталі
або покриваються струмопровідними
матеріалами (сталевий лист, металева
сітка). Особлива увага звертається на
наявність електричного контакту
струмопровідних шарів дверей і стін по
всьому периметру дверного отвору. При
екрануванні полів неприпустима наявність
зазорів, щілин в екрані. Розмір вічка
сітки повинен бути не більше 0,1 довжини
хвилі випромінювання.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Вибір
числа рівнів та матеріалів екранування
здійснюється з урахуванням:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">характеристик
випромінювання (тип, частота і потужність);</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">вимог
до рівня випромінювання за межами
контрольованої зони і розмірів зони;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">наявності
або відсутності інших методів захисту
від ПЕМВН;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">мінімізації
витрат на екранування.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">У
захищеної ПЕОМ, наприклад, екрануються
блоки керування електронно-променевою
трубкою, корпус виконується зі сталі
або металлізіруется зсередини, екран
монітора покривається струмопровідної
заземленою плівкою і (або) захищається
металевою сіткою.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Екранування,
крім виконання своєї прямої функції-захисту
від ПЕМВН, значно знижує шкідливий вплив
електромагнітних випромінювань на
організм людини. Екранування дозволяє
також зменшити вплив електромагнітних
шумів на роботу пристроїв.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><I><FONT SIZE=4><SPAN LANG="uk-UA"><B>1.2.
                                        Зниження потужності випромінювань і
                                        наведень</B></SPAN></FONT></I></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Способи
захисту від ПЕМВН, об'єднані в цю групу,
реалізуються з метою зниження рівня
випромінювання і взаємного впливу
елементів КС.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">До
цієї групи відносяться такі методи:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">зміна
електричних схем;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">використання
оптичних каналів зв'язку;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">зміна
конструкції;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">використання
фільтрів;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">гальванічна
розв'язка в системі живлення. </SPAN></FONT></FONT></FONT></FONT>
        </P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Зміни
електричних схем здійснюються для
зменшення потужності побічних
випромінювань. Це досягається за рахунок
використання елементів з меншим
випромінюванням, зменшення крутості
фронтів сигналів, запобігання виникненню
паразитної генерації, порушення
регулярності повторень інформації.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Перспективним
напрямком боротьби з ПЕМВН є використання
оптичних каналів зв'язку. Для передачі
інформації на великі відстані успішно
використовуються волоконно-оптичні
кабелі. Передачу інформації в межах
одного приміщення (навіть великих
розмірів) можна здійснювати за допомогою
бездротових систем, що використовують
випромінювання в інфрачервоному
діапазоні. Оптичні канали зв'язку не
породжують ПЕМВН. Вони забезпечують
високу швидкість передачі і не піддані
впливу електромагнітних перешкод.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Зміни
конструкції зводяться до зміни взаємного
розташування окремих вузлів, блоків,
кабелів, скорочення довжини шин.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><I><FONT SIZE=4><SPAN LANG="uk-UA">Використання
фільтрів </SPAN></FONT></I></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-style: normal">є
одним з основних способів захисту від
ПЕМВН. Фільтри встановлюються як
усередині пристроїв, систем для усунення
розповсюдження та можливого посилення
наведених побічних електромагнітних
сигналів, так і на виході з об'єктів
ліній зв'язку, сигналізації та
електроживлення. Фільтри розраховуються
таким чином, щоб вони забезпечували
зниження сигналів в діапазоні побічних
наводок до безпечного рівня і не вносили
істотних спотворень корисного сигналу.</SPAN></SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 0; orphans: 0">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-style: normal">Повністю
виключається попадання побічних
наведених сигналів в зовнішній ланцюг
електроживлення при наявності генераторів
живлення, які забезпечують гальванічну
розв'язку між первинним і вторинним
ланцюгами.</SPAN></SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-style: normal">Використання
генераторів дозволяє також подавати у
вторинний ланцюг електроживлення з
іншими параметрами у порівнянні з
первинним ланцюгом. Так, у вторинному
ланцюзі може бути змінена частота в
порівнянні з первинним ланцюгом.</SPAN></SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-style: normal">Генератори
живлення, за рахунок інерційності
механічної частини, дозволяють згладжувати
пульсації напруги і короткочасні
відключення в первинному ланцюзі.</SPAN></SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <BR>
        </P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt"><I><FONT SIZE=4><SPAN LANG="uk-UA"><B>1.3.
                                        Зниження інформативності сигналів</B></SPAN></FONT></I></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Зниження
інформативності сигналів ПЕМВН
здійснюється наступними шляхами:</SPAN></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">спеціальні
схемні рішення;</SPAN></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">кодування
інформації.</SPAN></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">В
якості прикладів спеціальних схемних
рішень можна навести такі, як заміна
послідовного коду паралельним, збільшення
розрядності паралельних кодів, зміна
черговості розгортки рядків на моніторі
і т. п. Ці заходи ускладнюють процес
отримання інформації з перехопленого
зловмисником сигналу. </SPAN></FONT></FONT></FONT>
        </P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Для
запобігання витоку інформації може
використовуватися кодування інформації,
в тому числі і криптографічне перетворення.</SPAN></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=LEFT STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <BR>
        </P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt"><B><FONT SIZE=4><SPAN LANG="uk-UA">2.
Активні методи захисту від ПЕМВН</SPAN></FONT></B></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Активні
методи захисту від ПЕМВН припускають
застосування генераторів шумів, що
розрізняються принципами формування
маскуючих перешкод. В якості маскуючих
використовуються випадкові перешкоди
з нормальним законом розподілу
спектральної щільності миттєвих значень
амплітуд і прицільні перешкоди, що
представляють собою випадкову
послідовність сигналів перешкоди,
ідентичних побічним сигналам.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Використовується
просторове і лінійне зашумлення.
Просторове зашумлення здійснюється за
рахунок випромінювання за допомогою
антен електромагнітних сигналів в
простір. Застосовується локальне
просторове зашумлення для захисту
конкретного елемента КС і об'єктне
просторове зашумлення для захисту від
побічних електромагнітних випромінювань
КС всього об'єкта. При локальному
просторовому зашумлені використовуються
прицільні перешкоди. Антена знаходиться
поряд з елементом КС, що захищається.
Об'єктне просторове зашумлення
здійснюється, як правило, кількома
генераторами зі своїми антенами, що
дозволяє створювати перешкоди у всіх
діапазонах побічних електромагнітних
випромінювань усіх випромінювальних
пристроїв об'єкта.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Просторове
зашумлення повинно забезпечувати
неможливість виділення побічних
випромінювань на тлі створюваних
перешкод у всіх діапазонах випромінювання
і, разом з тим, рівень створюваних
перешкод не повинен перевищувати
санітарних норм і норм щодо електромагнітної
сумісності радіоелектронних апаратур.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=LEFT STYLE="text-indent: 0in"><FONT SIZE=2><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">При
використанні лінійного зашумлення
генератори прицільних перешкод
підключаються до струмопровідних ліній
для створення в них електричних перешкод,
які не дозволяють зловмисникам виділяти
наведені сигнали.</SPAN></FONT></FONT></FONT></FONT></P>
    </div></div>
</BODY>
</HTML>
