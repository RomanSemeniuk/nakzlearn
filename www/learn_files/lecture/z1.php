<!--#include virtual="/topMenu.php" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
    <TITLE></TITLE>
    <META NAME="GENERATOR" CONTENT="LibreOffice 4.1.6.2 (Linux)">
    <META NAME="AUTHOR" CONTENT="Naks ®">
    <META NAME="CREATED" CONTENT="20151011;142300000000000">
    <META NAME="CHANGEDBY" CONTENT="Naks ®">
    <META NAME="CHANGED" CONTENT="20151011;142400000000000">
    <META NAME="AppVersion" CONTENT="14.0000">
    <META NAME="Company" CONTENT="SPecialiST RePack">
    <META NAME="DocSecurity" CONTENT="0">
    <META NAME="HyperlinksChanged" CONTENT="false">
    <META NAME="LinksUpToDate" CONTENT="false">
    <META NAME="ScaleCrop" CONTENT="false">
    <META NAME="ShareDoc" CONTENT="false">
    <STYLE TYPE="text/css">
        <!--
        @page { margin-left: 0.98in; margin-right: 0.59in; margin-top: 0.59in; margin-bottom: 0.59in }
        P { text-indent: 0.3in; margin-bottom: 0in; direction: ltr; line-height: 100%; text-align: justify; widows: 2; orphans: 2 }
        P.western { font-family: "Times New Roman", serif; font-size: 12pt; so-language: ru-RU }
        P.cjk { font-family: "Times New Roman"; font-size: 12pt; so-language: ru-RU }
        P.ctl { font-family: "Times New Roman"; font-size: 10pt }
        H1 { margin-top: 0in; margin-bottom: 0.08in; border-top: none; border-bottom: 1.00pt solid #00000a; border-left: none; border-right: none; padding-top: 0in; padding-bottom: 0.08in; padding-left: 0in; padding-right: 0in; direction: ltr; text-transform: uppercase; line-height: 100%; page-break-inside: avoid; widows: 2; orphans: 2; page-break-before: always }
        H1.western { font-family: "Peterburg", serif; font-size: 12pt; so-language: uk-UA }
        H1.cjk { font-family: "Times New Roman"; font-size: 12pt; so-language: ru-RU }
        H1.ctl { font-family: "Times New Roman"; font-size: 10pt; font-weight: normal }
        H2 { margin-top: 0.08in; margin-bottom: 0.04in; direction: ltr; line-height: 100%; widows: 2; orphans: 2 }
        H2.western { font-family: "Peterburg", serif; font-size: 11pt; so-language: uk-UA }
        H2.cjk { font-family: "Times New Roman"; font-size: 11pt; so-language: ru-RU }
        H2.ctl { font-family: "Times New Roman"; font-size: 10pt; font-weight: normal }
        A:link { color: #0000ff; so-language: zxx }
        -->
    </STYLE>
</HEAD>
<BODY LANG="uk-UA" LINK="#0000ff" DIR="LTR">
<div id="wrapper">
    <scrip><?php include('../toplearn.ini.php');?></scrip>
    <div id="content">
        <H1 CLASS="western" ALIGN=CENTER><A NAME="_Toc507305738"></A><A NAME="_Toc506020131"></A><A NAME="_Toc365048716"></A>
            <FONT SIZE=4 STYLE="font-size: 16pt">Теоретичні основи
                захисту інформації в комп’ютерних
                системах </FONT>
        </H1>
        <H2 CLASS="western" ALIGN=CENTER><A NAME="_Toc365048717"></A><FONT FACE="Times New Roman, serif"><FONT SIZE=5 STYLE="font-size: 20pt">Основні
                    поняття захисту інформації в комп’ютерних
                    системах</FONT></FONT></H2>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I>По-справжньому
                            безпечною можна вважати лише систему,
                            що виключена, замурована в бетонний
                            корпус, замкнена в приміщенні зі
                            свинцевими стінами й охороняється
                            збройною вартою, але й у цьому випадку
                            сумніви не залишають мене.</I></SPAN></FONT></FONT></P>
        <P CLASS="western" ALIGN=JUSTIFY STYLE="margin-left: 2.88in; text-indent: 0in">
            <BR>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="margin-left: 2.88in; text-indent: 0in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I>Юджин Х.
                            Спаффорд</I></SPAN></FONT></FONT></P>
        <OL>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Предмет та
                                    об’єкт захисту.</B></SPAN></FONT></FONT></P>
        </OL>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Спокон віків
інформація була ключовим фактором
продуктивного існування й розвитку
людства. Зараз для обробки інформації
у всіх сферах людської діяльності
використовуються потужні обчислювальні
машини. Від результатів їхньої роботи
прямо залежить якість нашого життя. ЕОМ
аналізують умови ринку й формують
бізнес-проекти, розраховують траекторії
руху супутників, управляють складним
високотехнологічним виробництвом,
системами життєзабезпечення й безпеки.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Зростання ролі
й відповідальності інформаційних
технологій у життєдіяльності людини
неминуче спричиняє відповідальне
відношення до забезпечення надійної,
безпечної роботи автоматизованих
комп'ютерних систем. Помилки у
функціонуванні автоматизованих
комп'ютерних систем (випадкові або
навмисні, викликані злочинними діями
зловмисників, комп'ютерних хуліганів
або конкурентів) можуть призвести до
досить серйозних наслідків. Захищена
від зовнішніх і внутрішніх загроз
автоматизована комп'ютерна система –
це те, до чого прагнуть керівники великих
підприємств і власники домашніх
персональних ЕОМ. Захист інформації –
справа, необхідність і значимість якої
продиктовані практикою, але створити
захищену комп'ютерну систему не просто.
У цій справі безумовно одну з важливих
ролей відіграють програмні засоби
захисту інформації.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=LEFT STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Предмет
                                захисту</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">:
інформація.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=LEFT STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Об’єкт
                                захисту</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">:
комп’ютерна система.</SPAN></FONT></FONT></P>
        <OL START=2>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Основні
                                    поняття. </B></SPAN></FONT></FONT>
                </P>
        </OL>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Інформація</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
– це відомості про осіб, предмети, факти,
події, явища і процеси незалежно від
форми їх подання.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Залежно від
форми подання інформація може бути
розділена на мовну, телекомунікаційну
та документовану.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Мовна
                                інформація</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
виникає в ході ведення в приміщеннях
розмов, роботи систем зв’язку,
звукопідсилення та звуковідтворення.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Телекомунікаційна
                                інформація</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
циркулює в технічних засобах обробки
і зберігання інформації, а також в
каналах зв’язку при її передачі.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">До </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>документованої
                                інформації</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
(або документів) відносять інформацію,
представлену на матеріальних носіях
разом з ідентифікуючими її реквізитами.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">До </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>інформаційних
                                процесів</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
відносять процеси збору, обробки,
накопичення, зберігання, пошуку і
розповсюдження інформації.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Під </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>інформаційною
                                системою</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
розуміють впорядковану сукупність
документів і масивів документів та
інформаційних технологій, що реалізують
інформаційні процеси.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Інформаційними
                                ресурсами</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
називають документи і масиви документів,
що існують окремо або в складі інформаційних
систем.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Процес створення
оптимальних умов для задоволення
інформаційних потреб громадян,
організацій, суспільства і держави в
цілому називають </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>інформатизацією.</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Також інформація
може бути класифікована таким чином
(див. рис.&nbsp;1.1):</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=LEFT STYLE="text-indent: 0in"><IMG SRC="learn_files/z1/U-1.png"></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=CENTER STYLE="text-indent: 0in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I>Рис. 1.1. Види
                            інформації</I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">До службової
таємниці відносять: адвокатську таємницю,
таємницю суду і слідства тощо. До
комерційної таємниці відносять
банківську. До персональних даних
відносять відомості про факти, події і
обставини життя громадянина, що дозволяють
ідентифікувати його особу.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Інформація є
одним з об'єктів цивільних прав, у тому
числі і прав власності, володіння і
користування. </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Власник-розпорядник
                                інформаційних ресурсів, систем і
                                технологій</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
– це суб'єкт з повноваженнями володіння,
користування і розпорядження зазначеними
об'єктами. Власником інформаційних
ресурсів, систем і технологій є суб'єкт
з повноваженнями володіння і користування
зазначеними об'єктами. Під </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>користувачем
                                інформації</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
розуміється суб'єкт, який звертається
до інформаційної системи за отриманням
необхідної йому інформації і користується
нею.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>До інформації,
                                яка захищається відноситься</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
така, що є предметом власності і підлягає
захисту відповідно до вимог правових
документів або вимог, встановлених
власником-розпорядником інформації.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Захистом
                                інформації</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
називають діяльність щодо запобігання
витоку інформації, несанкціонованих і
ненавмисних дій на цю інформацію.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Під </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>витоком</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
розуміють неконтрольоване поширення
інформації шляхом її розголошення,
несанкціонованого доступу до неї та
отримання розвідками. </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Розголошення</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
– це доведення інформації до
неконтрольованої кількості одержувачів
інформації (наприклад, публікація
інформації на відкритому сайті в мережі
Інтернет або у відкритій пресі).
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Несанкціонований
                                доступ</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"> –
отримання інформації зацікавленим
суб'єктом з порушенням правил доступу
до неї.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Несанкціонований
                                вплив на інформацію – </B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">вплив
з порушенням правил її зміни (наприклад,
навмисне впровадження в інформаційні
ресурси шкідливого програмного коду
чи навмисна підміна електронного
документу).</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Під </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>ненавмисним
                                впливом</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
на інформацію розуміють вплив на неї
через помилки користувача, збій технічних
чи програмних засобів, природних явищ,
інших неціленаправлених впливів
(наприклад, знищення документів у
результаті відмови накопичувача на
жорсткому магнітному диску комп'ютера).</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Метою захисту
                                інформації</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
(її бажаним результатом) є запобігання
шкоди власнику-розпоряднику, власнику
чи користувачу інформації. Під </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>ефективністю</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
захисту інформації розуміють ступінь
відповідності результатів захисту
інформації поставленої мети. Об'єктом
захисту виступає інформація, її носій
або інформаційний процес, у відношенні
яких необхідно забезпечувати захист у
відповідності до поставленої мети.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Під якістю
                                інформації</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
розуміють сукупність властивостей, що
обумовлюють придатність інформації
задовольняти певні потреби її користувачів
відповідно до призначення інформації.
Одним з </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>показників</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>якості</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>інформації</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
є ії </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>захищеність</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
– підтримання на заданому рівні тих
параметрів інформації, які характеризують
встановлений статус її зберігання,
обробки та використання.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Основними
                                характеристиками інформації</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
є </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><B>конфіденційність,
                            цілісність і доступність</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Конфіденційність
                                інформації</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
– це відомість її змісту тільки тим
суб'єктам, які мають відповідні
повноваження. Конфіденційність є
суб'єктивною характеристикою інформації,
пов'язаною з об'єктивною необхідністю
захисту законних інтересів одних
суб'єктів від інших. Одним з основних
методів забезпечення конфіденційності
інформації є її шифрування. </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><B>Шифруванням</B></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
інформації називають процес її
перетворення, при якому зміст інформації
стає незрозумілим для суб'єктів, які не
володіють відповідними повноваженнями.
Результат шифрування інформації
називають шифр-текст, або криптограма.
Зворотний процес відновлення інформації
з шифртекста називають розшифрування
інформації. Алгоритми, що використовуються
при шифруванні і розшифруванні інформації,
звичайно не є конфіденційними, а
конфіденційність шифртекста забезпечується
використанням при шифруванні додаткового
параметра, званого </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>ключем</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
шифрування. Знання ключа шифрування
дозволяє виконати правильне розшифрування
шифртекста.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Цілісністю
                                інформації</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
називають незмінність інформації в
умовах її випадкового і (або) навмисного
викривлення або руйнування. Цілісність
є частиною більш широкої характеристики
інформації – її </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>достовірності</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">,
що включає крім цілісності ще й </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>повноту
                                і точність</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
відображення предметної області. Для
перевірки цілісності інформації часто
використовується її хешування. </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Хешуванням
                            </B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">інформації
називають процес її перетворення в
хеш-значення фіксованої довжини
(дайджест).</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Під доступністю
                                інформації</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
розуміють здатність забезпечення
безперешкодного доступу суб'єктів до
інформації, що їх цікавить. </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Відмовою
                                в обслуговуванні</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
називають стан інформаційної системи,
при якому блокується доступ до деякого
її ресурсу.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Сукупність
інформаційних ресурсів та системи
формування, розповсюдження і використання
інформації називають </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>інформаційним
                                середовищем суспільства.</B></I></SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Під </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>інформаційною
                                безпекою</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
розуміють стан захищеності інформаційного
середовища, що забезпечує її формування
та розвиток. Вона досягається шляхом
реалізації політики безпеки.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Політика
                                безпеки</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
– це набір документованих норм, правил
і практичних прийомів, що регулюють
управління, захист і розподіл інформації
обмеженого доступу.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Комп'ютерною
                                системою</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
(КС) обробки інформації називають
організаційно-технічну систему, що
включає в себе:</SPAN></FONT></FONT></P>
        <UL>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in; widows: 0; orphans: 0">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">технічні засоби
	обчислювальної техніки та зв'язку;</SPAN></FONT></FONT></P>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in; widows: 0; orphans: 0">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">методи та
	алгоритми обробки інформації, реалізовані
	у вигляді програмних засобів;</SPAN></FONT></FONT></P>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in; widows: 0; orphans: 0">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">інформацію
	(файли, бази даних) на різних носіях;</SPAN></FONT></FONT></P>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in; widows: 0; orphans: 0">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">обслуговуючий
	персонал та користувачів, об'єднаних
	за організаційно-структурними,
	тематичними, технологічними чи іншими
	ознаками.</SPAN></FONT></FONT></P>
        </UL>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Під </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>програмними
                                засобами захисту інформації</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
розуміють спеціальні програми, що
включаються до складу програмного
забезпечення КС виключно для виконання
захисних функцій.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">До основних
програмних засобів захисту інформації
належать:</SPAN></FONT></FONT></P>
        <UL>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in; widows: 0; orphans: 0">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">програми
	ідентифікації і аутентифікації
	користувачів КС;</SPAN></FONT></FONT></P>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in; widows: 0; orphans: 0">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">програми
	розмежування доступу користувачів до
	ресурсів КС;</SPAN></FONT></FONT></P>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in; widows: 0; orphans: 0">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">програми
	шифрування інформації;</SPAN></FONT></FONT></P>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in; widows: 0; orphans: 0">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">програми
	захисту інформаційних ресурсів
	(системного та прикладного програмного
	забезпечення, баз даних, комп'ютерних
	засобів навчання тощо) від несанкціонованої
	зміни, використання та копіювання.</SPAN></FONT></FONT></P>
        </UL>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in; widows: 0; orphans: 0">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Під </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>ідентифікацією</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">,
стосовно забезпечення інформаційної
безпеки КС, розуміють однозначне
розпізнавання унікального імені суб'єкта
КС. </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Аутентифікація
                            </B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">означає
підтвердження того, що пред'явлене ім'я
відповідає даному суб'єкту (підтвердження
автентичності суб'єкта). </SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Авторизація</B></I></SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">
– надання суб'єкту прав на доступ до
об'єкта.</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Приклади
допоміжних програмних засобів захисту
інформації: </SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">програми
знищення залишкової інформації (в блоках
оперативної пам'яті, тимчасових файлах
тощо);</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">програми
аудиту (ведення реєстраційних журналів)
подій, пов'язаних з безпекою КС, для
забезпечення можливості відновлення
і доведення факту що ці подій відбулися;
</SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            • <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">програми
імітації роботи з порушником (відволікання
його на отримання нібито конфіденційної
інформації);</SPAN></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            •<FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">програми
тестового контролю захищеності КС тощо.
</SPAN></FONT></FONT>
        </P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">До переваг
програмних засобів захисту інформації
належать:</SPAN></FONT></FONT></P>
        <UL>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in; widows: 0; orphans: 0">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">простота
	тиражування;</SPAN></FONT></FONT></P>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in; widows: 0; orphans: 0">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">гнучкість
	(можливість налаштування на різні умови
	застосування, що враховують специфіку
	загроз інформаційної безпеки конкретних
	КС);</SPAN></FONT></FONT></P>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in; widows: 0; orphans: 0">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">простота
	застосування – одні програмні засоби,
	наприклад, шифрування, працюють у
	«прозорому» (непомітному для користувача)
	режимі, а інші не вимагають від користувача
	ніяких нових (порівняно з іншими
	програмами) навичок;</SPAN></FONT></FONT></P>
            <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in; widows: 0; orphans: 0">
                    <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">практично
	необмежені можливості їх розвитку
	шляхом внесення змін для урахування
	нових загроз безпеки інформації.</SPAN></FONT></FONT></P>
        </UL>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.49in">
            <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">До недоліків
програмних засобів захисту інформації
належать:</SPAN></FONT></FONT></P>
        <OL>
            <UL>
                <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in; widows: 0; orphans: 0">
                        <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">зниження
		ефективності КС за рахунок споживання
		її ресурсів, необхідних для функціонування
		програм захисту;</SPAN></FONT></FONT></P>
                <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in; widows: 0; orphans: 0">
                        <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">більш низька
		продуктивність (у порівнянні з апаратними
		засобами захисту, які виконують
		аналогічні функції, наприклад,
		шифрування);</SPAN></FONT></FONT></P>
                <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in; widows: 0; orphans: 0">
                        <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">пристикованість
		багатьох програмних засобів захисту
		(а не їх вбудованість в програмне
		забезпечення КС), що створює для
		порушника принципову можливість їх
		обходу;</SPAN></FONT></FONT></P>
                <LI><P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0in; widows: 0; orphans: 0">
                        <FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">можливість
		зловмисної зміни програмних засобів
		захисту в процесі експлуатації КС.</SPAN></FONT></FONT></P>
            </UL>
        </OL>
    </div></div>
</BODY>
</HTML>
