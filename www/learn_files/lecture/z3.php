<!--#include virtual="/topMenu.php" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
    <TITLE></TITLE>
    <META NAME="GENERATOR" CONTENT="LibreOffice 4.1.6.2 (Linux)">
    <META NAME="AUTHOR" CONTENT="Naks ®">
    <META NAME="CREATED" CONTENT="20151011;142600000000000">
    <META NAME="CHANGEDBY" CONTENT="Naks ®">
    <META NAME="CHANGED" CONTENT="20151011;142600000000000">
    <META NAME="AppVersion" CONTENT="14.0000">
    <META NAME="Company" CONTENT="SPecialiST RePack">
    <META NAME="DocSecurity" CONTENT="0">
    <META NAME="HyperlinksChanged" CONTENT="false">
    <META NAME="LinksUpToDate" CONTENT="false">
    <META NAME="ScaleCrop" CONTENT="false">
    <META NAME="ShareDoc" CONTENT="false">
    <STYLE TYPE="text/css">
        <!--
        @page { margin-left: 0.98in; margin-right: 0.59in; margin-top: 0.59in; margin-bottom: 0.59in }
        P { margin-bottom: 0.08in; direction: ltr; line-height: 100%; widows: 2; orphans: 2 }
        P.western { font-family: "Times New Roman", serif; font-size: 10pt; so-language: ru-RU }
        P.cjk { font-family: "Times New Roman"; font-size: 10pt; so-language: ru-RU }
        P.ctl { font-family: "Times New Roman"; font-size: 10pt }
        H2 { margin-top: 0.08in; margin-bottom: 0.04in; direction: ltr; line-height: 100%; widows: 2; orphans: 2 }
        H2.western { font-family: "Peterburg", serif; font-size: 11pt; so-language: uk-UA }
        H2.cjk { font-family: "Times New Roman"; font-size: 11pt; so-language: ru-RU }
        H2.ctl { font-family: "Times New Roman"; font-size: 10pt; font-weight: normal }
        -->
    </STYLE>
</HEAD>
<script type="text/javascript" src="js.js"></script>
<BODY LANG="uk-UA" DIR="LTR">
<div id="wrapper">
    <scrip><?php include('../toplearn.ini.php');?></scrip>
    <div id="content">
        <H2 CLASS="western" ALIGN=CENTER><A NAME="_Toc365048719"></A><FONT FACE="Times New Roman, serif"><FONT SIZE=5 STYLE="font-size: 20pt">Правові
                    та організаційні методи захисту
                    інформації в КС</FONT></FONT></H2>
        <P LANG="ru-RU" CLASS="western" STYLE="margin-bottom: 0in"><FONT SIZE=4><SPAN LANG="uk-UA"><B>1.
                        Правове регулювання в галузі безпеки
                        інформації</B></SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; margin-bottom: 0in; widows: 0; orphans: 0">
            <FONT SIZE=4><SPAN LANG="uk-UA">Комплексна система
захисту інформації створюється на
об'єктах для блокування всіх можливих
або, принаймні, найбільш ймовірних
загроз безпеки інформації. Для блокування
тієї чи іншої загрози використовується
певна сукупність методів і засобів
захисту. Деякі з них захищають інформацію
від декількох загроз одночасно. Серед
методів захисту є й універсальні методи,
які є базовими при побудові будь-якої
системи захисту. Це, насамперед, правові
методи захисту інформації, що слугують
основою легітимної побудови та
використання системи захисту будь-якого
призначення. Організаційні методи
захисту інформації, як правило,
використовуються для блокування
декількох загроз. Крім того, організаційні
методи використовуються в будь-якій
системі захисту без винятків.</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">Держава повинна
забезпечити в країні захист інформації
як в масштабах всієї держави, так і на
рівні організацій та окремих громадян.
Для вирішення цієї проблеми держава
зобов'язана:</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">1) виробити державну
політику безпеки в області інформаційних
технологій;</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">2) законодавчо визначити
правовий статус комп'ютерних систем,
інформації, систем захисту інформації,
власників і користувачів інформації
</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">тощо</SPAN></FONT><FONT SIZE=4><SPAN LANG="uk-UA">;</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">3) створити ієрархічну
структуру державних органів, що виробляють
і втілюють у життя політику безпеки
інформаційних технологій;</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">4) створити систему
стандартизації, ліцензування та
сертифікації у сфері захисту інформації;</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">5) забезпечити
пріоритетний розвиток вітчизняних
захищених інформаційних технологій;</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">6) підвищувати рівень
освіти громадян в області інформаційних
технологій, виховувати у них патріотизм
і пильність;</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">7) встановити
відповідальність громадян за порушення
законодавства в галузі інформаційних
технологій.</SPAN></FONT></P>
        <P CLASS="western" STYLE="margin-bottom: 0in"><BR>
        </P>
        <P LANG="ru-RU" CLASS="western" STYLE="margin-bottom: 0in"><FONT SIZE=4><SPAN LANG="uk-UA"><B>1.1.
                        Політика держави в області безпеки
                        інформаційних технологій</B></SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">У державі повинна
проводитися єдина політика в області
безпеки інформаційних технологій.
Найважливішими завданнями держави в
галузі інформаційної безпеки є:</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; margin-bottom: 0in">
            • <FONT SIZE=4><SPAN LANG="uk-UA">встановлення
необхідного балансу між потребою у
вільному обміні інформацією і допустимими
обмеженнями її розповсюдження;</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; margin-bottom: 0in; widows: 0; orphans: 0">
            • <FONT SIZE=4><SPAN LANG="uk-UA">вдосконалення
інформаційної структури, прискорення
розвитку нових інформаційних технологій
та їх широке впровадження, уніфікація
засобів пошуку, збору, зберігання та
аналізу інформації з урахуванням
входження України в глобальну інформаційну
інфраструктуру;</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; margin-bottom: 0in">
            • <FONT SIZE=4><SPAN LANG="uk-UA">розробка відповідної
нормативної правової бази та координація
діяльності органів державної влади та
інших органів, що вирішують завдання
забезпечення інформаційної безпеки;</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; margin-bottom: 0in">
            • <FONT SIZE=4><SPAN LANG="uk-UA">розвиток вітчизняної
індустрії телекомунікаційних та
інформаційних засобів, їх пріоритетне
в порівнянні з зарубіжними аналогами
поширення на внутрішньому ринку;</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; margin-bottom: 0in">
            • <FONT SIZE=4><SPAN LANG="uk-UA">захист державного
інформаційного ресурсу і, перш за все,
у федеральних органах державної влади
та на підприємствах оборонного комплексу.</SPAN></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; margin-bottom: 0in">
            <FONT SIZE=4><SPAN LANG="uk-UA">Зусилля держави повинні
бути спрямовані на виховання
відповідальності громадян за неухильне
виконання правових норм в області
інформаційної безпеки. Необхідно
використовувати всі доступні засоби
для формування у громадян патріотизму,
почуття гордості за належність до
країни, колективу. Важливим завданням
держави є також підвищення рівня освіти
громадян в області інформаційних
технологій. Велика роль у цій роботі
належить освітній системі держави,
державним органам управління, засобам
масової інформації. Це важливий напрямок
реалізації політики інформаційної
безпеки.</SPAN></FONT></P>
        <P CLASS="western" STYLE="margin-bottom: 0in"><BR>
        </P>
        <P LANG="ru-RU" CLASS="western" STYLE="margin-bottom: 0in"><FONT SIZE=4><SPAN LANG="uk-UA"><B>1.2.
                        Законодавча база інформатизації
                        суспільства</B></SPAN></FONT></P>
        <UL>
            <LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=3><FONT SIZE=4>Закон
                            України “Про інформацію”;</FONT></FONT></P>
            <LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=3><FONT SIZE=4>Закон
                            України “Про захист інформації в
                            автоматизованих системах”;</FONT></FONT></P>
            <LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=3><FONT SIZE=4>Закон
                            України “Про науково-технічну
                            інформацію”;</FONT></FONT></P>
            <LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=3><FONT SIZE=4>Закон
                            України “Про державну таємницю&quot;;</FONT></FONT></P>
            <LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=3><FONT SIZE=4>Концепція
                            (основи державної політики) національної
                            безпеки України;</FONT></FONT></P>
            <LI><P ALIGN=JUSTIFY STYLE="margin-bottom: 0in"><FONT SIZE=3><FONT SIZE=4>Концепція
                            технічного захисту інформації в Україні;
                        </FONT></FONT>
                </P>
        </UL>
        <P LANG="ru-RU" CLASS="western" STYLE="margin-bottom: 0in"><FONT SIZE=4>Положення
                про порядок здійснення криптографічного
                захисту інформації в Україні.</FONT></P>
    </div></div>
</BODY>
</HTML>
