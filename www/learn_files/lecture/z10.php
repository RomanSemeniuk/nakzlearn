<!--#include virtual="/topMenu.php" -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
    <TITLE></TITLE>
    <META NAME="GENERATOR" CONTENT="LibreOffice 4.1.6.2 (Linux)">
    <META NAME="AUTHOR" CONTENT="Naks ®">
    <META NAME="CREATED" CONTENT="20151011;143000000000000">
    <META NAME="CHANGEDBY" CONTENT="Naks ®">
    <META NAME="CHANGED" CONTENT="20151011;143000000000000">
    <META NAME="AppVersion" CONTENT="14.0000">
    <META NAME="Company" CONTENT="SPecialiST RePack">
    <META NAME="DocSecurity" CONTENT="0">
    <META NAME="HyperlinksChanged" CONTENT="false">
    <META NAME="LinksUpToDate" CONTENT="false">
    <META NAME="ScaleCrop" CONTENT="false">
    <META NAME="ShareDoc" CONTENT="false">
    <STYLE TYPE="text/css">
        <!--
        @page { margin-left: 0.98in; margin-right: 0.59in; margin-top: 0.59in; margin-bottom: 0.59in }
        P { text-indent: 0.3in; margin-bottom: 0in; direction: ltr; line-height: 100%; text-align: justify; widows: 2; orphans: 2 }
        P.western { font-family: "Times New Roman", serif; font-size: 12pt; so-language: ru-RU }
        P.cjk { font-family: "Times New Roman"; font-size: 12pt; so-language: ru-RU }
        P.ctl { font-family: "Times New Roman"; font-size: 10pt }
        H2 { margin-top: 0.08in; margin-bottom: 0.04in; direction: ltr; line-height: 100%; widows: 2; orphans: 2 }
        H2.western { font-family: "Peterburg", serif; font-size: 11pt; so-language: uk-UA }
        H2.cjk { font-family: "Times New Roman"; font-size: 11pt; so-language: ru-RU }
        H2.ctl { font-family: "Times New Roman"; font-size: 10pt; font-weight: normal }
        A:link { color: #0000ff; so-language: zxx }
        -->
    </STYLE>
</HEAD>
<BODY LANG="uk-UA" LINK="#0000ff" DIR="LTR">
<div id="wrapper">
    <scrip><?php include('../toplearn.ini.php');?></scrip>
    <div id="content">
        <H2 CLASS="western" ALIGN=CENTER><A NAME="_Toc365048724"></A><FONT SIZE=5 STYLE="font-size: 20pt">Комп'ютерні
                віруси та механізми боротьби з ними</FONT></H2>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Шкідливі
програми і, перш за все, віруси являють
дуже серйозну небезпеку для інформації
в КС. Недооцінка цієї небезпеки може
мати серйозні наслідки для інформації
користувачів. Шкодить використанню
всіх можливостей КС і надмірне
перебільшення небезпеки вірусів. Знання
механізмів дії вірусів, методів і засобів
боротьби з ними дозволяє ефективно
організувати протидію вірусам, звести
до мінімуму ймовірність зараження і
втрат від їх впливу.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Термін
«</SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>комп'ютерний
                                        вірус</B></I></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">»
був введений порівняно недавно – в
середині 80-х років. Малі розміри, здатність
швидко розповсюджуватися, розмножуючись
і проникаючи в об'єкти (заражаючи їх),
негативний вплив на систему – всі ці
ознаки біологічних вірусів властиві і
шкідливим програмам, які отримали з
цієї причини назву «комп'ютерні віруси».
Разом з терміном «</SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>вірус</B></I></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">»
при роботі з комп'ютерними вірусами
використовуються і інші медичні терміни:
«зараження», «середовище проживання»,
«профілактика» та ін</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">«</SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Комп'ютерні
                                        віруси</B></I></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">»
– це невеликі виконувані або інтерпретовані
програми, що володіють властивістю
поширення і самовідтворення (реплікації)
в КС. Віруси можуть виконувати зміну
або знищення програмного забезпечення
або даних, що зберігаються в КС. У процесі
поширення віруси можуть себе модифікувати.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <BR>
        </P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><B><FONT SIZE=4><SPAN LANG="uk-UA">1.
Класифікація комп'ютерних вірусів</SPAN></FONT></B></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">В
даний час в світі налічується більше
40 тисяч лише зареєстрованих комп'ютерних
вірусів. Оскільки переважна більшість
сучасних шкідливих програм мають
здатність до саморозмноження, то часто
їх відносять до комп'ютерним вірусам.
Всі комп'ютерні віруси можуть бути
класифіковані за такими ознаками:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">по
середовищу проживання;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">за
способом зараження;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">за
ступенем небезпеки деструктивних
(шкідливих ) впливів;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">за
алгоритмом функціонування.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">За
середовищем існування комп'ютерні
віруси діляться на:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">мережеві;						•
файлові;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">завантажувальні;				•
комбіновані.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Середовищем
проживання </SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>мережевих</B></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
вірусів є елементи комп'ютерних мереж.
</SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Файлові</B></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
віруси розміщуються у виконуваних
файлах. </SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Завантажувальні</B></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
віруси знаходяться в завантажувальних
секторах (галузях) зовнішніх запам'ятовуючих
пристроїв (boot-сектори). Іноді завантажувальні
віруси називають бутовими. </SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Комбіновані</B></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
віруси розміщуються в декількох
середовищах проживання. Прикладом таких
вірусів служать завантажувально-файлові
віруси. Ці віруси можуть розміщуватися
як в завантажувальних секторах
накопичувачів на магнітних дисках, так
і в тілі завантажувальних файлів.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">За
способом зараження середовища проживання
комп'ютерні віруси діляться на:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">резидентні;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">нерезидентні.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Резидентні
віруси після їх активізації повністю
або частково переміщаються з довкілля
(мережа, завантажувальний сектор, файл)
в оперативну пам'ять ЕОМ. Ці віруси,
використовуючи, як правило, привілейовані
режими роботи, дозволені тільки
операційній системі, заражають середовище
проживання і при виконанні певних умов
реалізують деструктивну функцію. На
відміну від резидентних нерезидентні
віруси потрапляють в оперативну пам'ять
ЕОМ тільки на час їхньої активності,
протягом якого виконують деструктивну
функцію та функцію зараження. Потім
віруси повністю залишають оперативну
пам'ять, залишаючись в середовищі
проживання. Якщо вірус поміщає в
оперативну пам'ять програму, яка не
заражає середовище проживання, то такий
вірус вважається нерезидентом.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Арсенал
деструктивних або шкідливих можливостей
комп'ютерних вірусів досить великий.
Деструктивні можливості вірусів залежать
від цілей і кваліфікації їх творця, а
також від особливостей комп'ютерних
систем.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">За
ступенем небезпеки для інформаційних
ресурсів користувача комп'ютерні віруси
можна розділити на:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">нешкідливі
віруси;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">небезпечні
віруси;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">дуже
небезпечні віруси.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Нешкідливі
комп'ютерні віруси створюються авторами,
які не ставлять собі мети завдати якоїсь
шкоди ресурсас КС. Ними, як правило,
рухає бажання показати свої можливості
програміста. Іншими словами, створення
комп'ютерних вірусів для таких людей –
своєрідна спроба самоствердження. </SPAN></FONT></FONT></FONT></FONT>
        </P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 0; orphans: 0">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Однак
при всій удаваній безневинність таких
вірусів вони завдають певної шкоди КС.
По-перше, такі віруси витрачають ресурси
КС, в тій чи іншій мірі знижуючи її
ефективність функціонування. По-друге,
комп'ютерні віруси можуть містити
помилки, викликають небезпечні наслідки
для інформаційних ресурсів КС. Крім
того, при модернізації операційної
системи або апаратних засобів КС віруси,
створені раніше, можуть призводити до
порушень штатного алгоритму роботи
системи.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">До
небезпечних відносяться віруси, які
викликають істотне зниження ефективності
КС, але не призводять до порушення
цілісності та конфіденційності
інформації, що зберігається в
запам'ятовувальних пристроях. Наслідки
таких вірусів можуть бути ліквідовані
без особливих витрат матеріальних і
часових ресурсів. Прикладами таких
вірусів є віруси, що займають пам'ять
ЕОМ і канали зв'язку, але не блокують
роботу мережі; віруси, що викликають
необхідність повторного виконання
програм, перезавантаження операційної
системи або повторній передачі даних
по каналах зв'язку і т. п.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Дуже
небезпечними слід вважати віруси, що
викликають порушення конфіденційності,
знищення, необоротну модифікацію (у
тому числі і шифрування) інформації, а
також віруси, що блокують доступ до
інформації, що призводять до відмови
апаратних засобів. Такі віруси стирають
окремі файли, системні області пам'яті,
форматують диски, отримують несанкціонований
доступ до інформації, шифрують дані і
т. п.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Відомі
публікації, в яких згадуються віруси,
що викликають несправності апаратних
засобів. Передбачається, що на резонансній
частоті рухомі частини електромеханічних
пристроїв, наприклад в системі
позиціонування накопичувача на магнітних
дисках, можуть бути зруйновані. Саме
такий режим і може бути створений за
допомогою програми-вірусу. Інші автори
стверджують, що можливе завдання режимів
інтенсивного використання окремих
електронних схем (наприклад, великих
інтегральних схем), при яких настає їх
перегрів і вихід з ладу.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Використання
в сучасних ПЕОМ постійної пам'яті з
можливістю перезапису призвело до появи
вірусів, що змінюють програми BIOS, що
приводить до необхідності заміни
постійних запам'ятовуючих пристроїв.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Можливі
також впливи на психіку людини –
оператора ЕОМ за допомогою підбору
відеозображення, що видається на екран
монітора з певною частотою (кожен
двадцять п'ятий кадр). Вбудовані кадри
цієї відеоінформації сприймаються
людиною на підсвідомому рівні. У
результаті такого впливу можливе
нанесення серйозного збитку психіці
людини. У 1997 році 700 японців потрапили
до лікарні з ознаками епілепсії після
перегляду комп'ютерного мультфільму
по телебаченню. Припускають, що саме
таким чином була випробувана можливість
впливу на людину за допомогою вбудовування
25-го кадру.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">У
відповідності з особливостями алгоритму
функціонування віруси можна розділити
на два класи:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">віруси,
що не змінюють середовище проживання
(файли і сектори) при поширенні;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">віруси,
що змінюють середовище проживання при
поширенні.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 0; orphans: 0">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">У
свою чергу, віруси, що не змінюють
середовище проживання, можуть бути
розділені на дві групи: віруси-&quot;супутники&quot;
(companion); віруси-«черв'яки» (worm).</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 0; orphans: 0">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Віруси-&quot;</SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>супутники</B></I></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">&quot;
не змінюють файли. Механізм їх дії
полягає у створенні копій виконуваних
файлів. Наприклад, в MS DOS такі віруси
створюють копії для файлів, що мають
розширення. ЕХЕ. Копії присвоюється те
ж ім'я, що і виконуваного файлу, але
розширення змінюється на. СОМ. При
запуску файлу з загальним ім'ям операційна
система першого завантажує на виконання
файл з розширенням. СОМ, який є
програмою-вірусом. Файл-вірус запускає
потім і файл з розширенням. ЕХЕ.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><I><FONT SIZE=4><SPAN LANG="uk-UA">Віруси-«</SPAN></FONT></I></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><I><FONT SIZE=4><SPAN LANG="uk-UA"><B>черв'яки</B></SPAN></FONT></I></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><I><FONT SIZE=4><SPAN LANG="uk-UA">»
</SPAN></FONT></I></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-style: normal">потрапляють
в робочу станцію з мережі, обчислюють
адреси розсилки вірусу по іншим абонентам
мережі та здійснюють передачу вірусу.
Вірус не змінює файлів і не записується
в завантажувальні сектори дисків. Деякі
віруси-«черв'яки» створюють робочі
копії вірусу на диску, інші – розміщуються
тільки в оперативній пам'яті ЕОМ.</SPAN></SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-style: normal">За
складністю, ступенем досконалості та
особливостям маскування алгоритмів
віруси, що змінюють середовище проживання,
діляться на: •&nbsp;студентські; • «стелс»
– віруси (віруси-невидимки); • поліморфні.</SPAN></SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-style: normal">До
</SPAN></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-style: normal"><B>студентських</B></SPAN></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-style: normal">
відносять віруси, творці яких мають
низьку кваліфікацію. Такі віруси, як
правило, є нерезидентними, часто містять
помилки, досить просто виявляються і
видаляються.</SPAN></SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-style: normal">«</SPAN></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-style: normal"><B>Стелс</B></SPAN></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-style: normal">»
– віруси і поліморфні віруси створюються
кваліфікованими фахівцями, які добре
знають принцип роботи апаратних засобів
і операційної системи, а також володіють
навичками роботи з машинооріентованими
системами програмування.</SPAN></SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-style: normal">«</SPAN></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-style: normal"><B>Стелс</B></SPAN></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-style: normal">»-віруси
маскують свою присутність в середовищі
проживання шляхом перехоплення звернень
операційної системи до уражених файлів,
секторів і переадресовують ОС до
незаражених ділянок інформації. Вірус
є резидентним, маскується під програми
ОС, може переміщатися в пам'яті. Такі
віруси активізуються при виникненні
переривань, виконують певні дії, в тому
числі і щодо маскування, і тільки потім
управління передається на програми ОС,
оброблювальні ці переривання. «Стелс»
– віруси мають здатність протидіяти
резидентним антивірусним засобам.</SPAN></SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 0; orphans: 0">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-style: normal">Поліморфні
віруси не мають постійних розпізнавальних
груп – сигнатур. Звичайні віруси для
розпізнавання факту зараження середовища
проживання розміщуються в зараженому
об'єкті спеціальну розпізнавальну
двійкову послідовність або послідовність
символів (сигнатуру), яка однозначно
ідентифікує зараженість файлу або
сектора. Сигнатури використовуються
на етапі розповсюдження вірусів для
того, щоб уникнути багаторазового
зараження одних і тих же об'єктів, так
як при багатократному зараженні об'єкта
значно зростає ймовірність виявлення
вірусу. Для усунення демаскуючих ознак
поліморфні віруси використовують
шифрування тіла вірусу і модифікацію
програми шифрування. За рахунок такого
перетворення поліморфні віруси не мають
збігів кодів.</SPAN></SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 0; orphans: 0">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Будь-який
вірус, незалежно від приналежності до
певних класів, повинен мати три
функціональних блоки: блок зараження
(розповсюдження), блок маскування і блок
виконання деструктивних дій. Поділ на
функціональні блоки означає, що до
певного блоку відносяться команди
програми вірусу, що виконують одну з
трьох функцій, незалежно від місця
знаходження команд в тілі вірусу.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Після
передачі управління вірусу, як правило,
виконуються певні функції блоку
маскування. Наприклад, здійснюється
розшифрування тіла вірусу. Потім вірус
здійснює функцію впровадження в
незаражене середовище проживання. Якщо
вірусом повинні виконуватися деструктивні
впливи, то вони виконуються або безумовно,
або при виконанні певних умов.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Завершує
роботу вірусу завжди блок маскування.
При цьому виконуються, наприклад,
наступні дії: шифрування вірусу (якщо
функція шифрування реалізована),
відновлення старої дати зміни файлу,
відновлення атрибутів файлу, коректування
таблиць ОС і ін</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Останньою
командою вірусу виконується команда
переходу на виконання заражених файлів
або на виконання програм ОС.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <BR>
        </P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><B><FONT SIZE=4><SPAN LANG="uk-UA">2.
Файлові віруси</SPAN></FONT></B></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><B><FONT SIZE=4><SPAN LANG="uk-UA">2.1.
Структура файлового вірусу</SPAN></FONT></B></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 0; orphans: 0">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Файлові
віруси можуть впроваджуватися тільки
в виконувані файли: командні файли
(файли, що складаються з команд операційної
системи), саморрозпаковуючі файли,
користувальницькі та системні програми
в машинних кодах, а також у документи
(таблиці), що мають макрокоманди.
Макрокоманди або макроси являють собою
виконувані програми для автоматизації
роботи з документами (таблицями). Тому
такі документи (таблиці) можна розглядати
як виконуваний файл.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Для
IBM – сумісних ПЕОМ вірус може впроваджуватися
в файли наступних типів: командні файли
(ВАТ), завантажувальні драйвери (SYS),
програми в машинних (двійкових) кодах
(ЕХЕ, СОМ), документи Word (DOC) з версії 6.0 і
вище, таблиці EXCEL (XLS). Макровіруси можуть
запроваджуватися і в інші файли, що
містять макрокоманди.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Файлові
віруси можуть розміщуватися на початку,
всередині і наприкінці заражених файлів:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <IMG SRC="learn_files/z10/U-5.png" NAME="Рисунок 2" ALIGN=BOTTOM WIDTH=370 HEIGHT=139 BORDER=0></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><I><FONT SIZE=4><SPAN LANG="uk-UA">Заголовок
віруса</SPAN></FONT></I></FONT></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=CENTER STYLE="text-indent: 0in">
            <FONT SIZE=2><FONT FACE="Times New Roman, serif"><FONT SIZE=2><I><FONT SIZE=4>Рис.
                            </FONT></I></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><I><FONT SIZE=4><SPAN LANG="uk-UA">1.</SPAN></FONT></I></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><I><FONT SIZE=4>5.
                            </FONT></I></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><I><FONT SIZE=4><SPAN LANG="uk-UA">Варіанти
розміщення вірусів у файлах</SPAN></FONT></I></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 0; orphans: 0">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Незалежно
від місця розташування вірусу в тілі
зараженого! файлу після передачі
управління файлу першими виконуються
команди вірусу.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">На
початок файлу вірус впроваджується
одним з трьох способів. Перший з них
полягає в переписуванні початку файлу
в його кінець, а на звільнене місце
записується вірус. Другий спосіб
передбачає зчитування вірусу і зараженого
файлу в оперативну пам'ять, об'єднання
їх в один файл і запис його на місце
файлу. При третьому способі зараження
вірус записується в початок файлу без
збереження вмісту. У цьому випадку
заражений файл стає непрацездатним.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 0; orphans: 0">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">В
середину файлу вірус може бути записаний
також різними способами. Файл може
«розсуватися», а в звільнене місце може
бути записаний вірус. Вірус може
впроваджуватися в середину файлу без
збереження ділянки файлу, на місце якого
міститься вірус. Є і більш екзотичні
способи впровадження вірусу в середину
файлу. Наприклад, вірус «Mutant» застосовує
метод стиснення окремих ділянок файлу,
при цьому довжина файлу після впровадження
вірусу може не змінитися.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Найчастіше
вірус впроваджується в кінець файлу.
При цьому, як і у випадку з впровадженням
вірусу в середину файлу, перші команди
файлу замінюються командами переходу
на тіло вірусу.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; widows: 2; orphans: 2"><BR>
        </P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><I><B><FONT SIZE=4><SPAN LANG="uk-UA">2.2.
Алгоритм роботи файлового вірусу</SPAN></FONT></B></I></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; widows: 0; orphans: 0"><FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Незважаючи
на різноманіття файлових вірусів, можна
виділити дії та порядок їх виконання,
які присутні при реалізації більшості
вірусів цього класу. Такий узагальнений
алгоритм може бути представлений у
вигляді наступної послідовності кроків:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><U>Крок
                                    1.</U></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
Резидентний вірус перевіряє, чи заражена
оперативна пам'ять, і при необхідності
заражає її. Нерезидентний вірус шукає
незаражені файли і заражає їх.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; widows: 2; orphans: 2"><U>
                <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Крок
2.</U></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
Виконуються дії по збереженню
працездатності програми, в файл якій
впроваджується вірус (відновлення
перших байт програми, настроювання
адрес програм і т. д.)</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; widows: 2; orphans: 2"><U>
                <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Крок
3.</U></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
Здійснюється деструктивна функція
вірусу, якщо виконуються відповідні
умови.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; widows: 2; orphans: 2"><U>
                <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Крок
4.</U></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
Передається керування програмі, у файлі
якої знаходиться вірус.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">При
реалізації конкретних вірусів склад
дій та їх послідовність можуть відрізнятися
від наведених в алгоритмі.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; widows: 2; orphans: 2"><BR>
        </P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><I><B><FONT SIZE=4><SPAN LANG="uk-UA">2.3.
Особливості макровірусів</SPAN></FONT></B></I></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Особливе
місце серед файлових вірусів займають
макровіруси. Макровіруси представляють
собою шкідливі програми, написані на
макромовах, вбудованих у текстові
редактори, електронні таблиці та ін.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Для
існування вірусів у конкретній системі
(редакторі) необхідно, щоб вбудована в
неї макромова, мала наступні можливості:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">прив'язку
програми на макромові до конкретного
файлу;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">копіювання
макропрограми з одного файлу в інший;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">отримання
управління макропрограми без втручання
користувача.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Таким
умовам відповідають редактори MS Word, MS
Office, Ami Pro, табличний процесор MS Excel. У цих
системах використовуються макромови
Word Basic і Visual Basic.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">При
виконанні певних дій над файлами, що
містять макропрограми (відкриття,
збереження, закриття і т. д.), автоматично
виконуються макропрограми файлів. При
цьому управління отримують макровіруси,
які зберігають активність до тих пір,
поки активний відповідний редактор
(процесор). Тому при роботі з іншим файлом
в «зараженому редакторі (процесорі)»,
він також заражається. Тут простежується
аналогія з резидентними вірусами за
механізмом зараження. Для отримання
управління макровіруси, що заражають
файли MS Office, як правило, використовують
один із прийомів:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">1)
у вірусі є автомакрос (виконується
автоматично, при відкритті документа,
таблиці);</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">2)
у вірусі перевизначений один із
стандартних макросів, який виконується
при виборі певного пункту меню;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">3)
макрос вірус автоматично викликається
на виконання при натисканні певної
клавіші або комбінацій клавіш.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Перший
макровірус WinWord. Concept, що вражає документи
Word, з'явився влітку 1995 року. Шкідлива
функція цього вірусу полягає в зміні
формату документів текстового редактора
Word у формат файлів стилів. Інший макровірус
WinWord Nuclear вже не настільки нешкідливий.
Він дописує фразу з вимогою заборони
ядерних випробувань, проведених Францією
в Тихому океані. Крім того, цей вірус
щорічно 5 квітня намагається знищити
важливі системні файли.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <BR>
        </P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><B><FONT SIZE=4><SPAN LANG="uk-UA">3.
Завантажувальні віруси</SPAN></FONT></B></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Завантажувальні
віруси заражають завантажувальні (Boot)
сектора гнучких дисків і Boot-сектора або
Master Boot Record (MBR) жорстких дисків:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=JUSTIFY STYLE="text-indent: 0.5in">
            <IMG SRC="learn_files/z10/U-6.png" NAME="Рисунок 1" ALIGN=BOTTOM WIDTH=373 HEIGHT=88 BORDER=0></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><I><FONT SIZE=4><SPAN LANG="uk-UA">Рис.
1.6. Розміщення завантажувального віруса
на диску</SPAN></FONT></I></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Завантажувальні
віруси є резидентними. Зараження
відбувається при завантаженні операційної
системи з дисків.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Після
включення ЕОМ здійснюється контроль
її працездатності за допомогою програми,
записаної в постійному запам'ятовуючому
пристрої. Якщо перевірка завершилася
успішно, то здійснюється зчитування
першого сектора з гнучкого або жорсткого
диска. Порядок використання дисководів
для завантаження задається користувачем
за допомогою програми Setup. Якщо диск, з
якого проводиться завантаження ОС
заражений завантажувальним вірусом,
то зазвичай виконуються наступні кроки:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><U>Крок
                                    1.</U></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
Зчитаний з 1-го сектора диска завантажувальний
вірус (частина вірусу) отримує управління,
зменшує обсяг вільної пам'яті ОП і зчитує
з диска тіло вірусу.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><U>Крок
                                    2.</U></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
Вірус переписує сам себе в іншу область
Oil, найчастіше – в старші адреси пам'яті.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><U>Крок
                                    3.</U></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
Встановлюються необхідні вектора
переривань (вірус резидентний).</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><U>Крок
                                    4</U></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">.
При виконанні певних умов виробляються
деструктивні дії.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><U>Крок
                                    5.</U></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
Копіюється Boot-сектор в ОП і передається
йому управління.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Якщо
вірус був активізований з гнучкого
диска, то він записується в завантажувальний
сектор жорсткого диска. Активний вірус,
постійно перебуваючи в ОП, заражає
завантажувальні сектори всіх гнучких
дисків, а не тільки системні диски.</SPAN></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Зараження
робочих гнучких дисків завантажувальними
вірусами виконується в розрахунку на
помилкові дії користувача ЕОМ в момент
завантаження ОС. Якщо встановлено
порядок завантаження ОС спочатку з
гнучкого диска, а потім – з жорсткого,
то при наявності гнучкого диска в
накопичувачі буде зчитаний 1-й сектор
з гнучкого диска. Якщо диск був заражений,
то цього досить для зараження ЕОМ. Така
ситуація найбільш часто має місце при
перезавантаженні ОС після «зависань»
або відмов ЕОМ.</SPAN></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt"><B><FONT SIZE=4><SPAN LANG="uk-UA">4.
Методи і засоби боротьби з вірусами</SPAN></FONT></B></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Масове
поширення вірусів, серйозність наслідків
їх впливу на ресурси КС викликали
необхідність розробки і використання
спеціальних антивірусних засобів і
методів їх застосування. Антивірусні
засоби застосовуються для вирішення
наступних завдань:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">виявлення
вірусів в КС;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">блокування
роботи програм-вірусів;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">усунення
наслідків впливу вірусів.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Виявлення
вірусів бажано здійснювати на стадії
їх впровадження або, принаймні, до
початку здійснення деструктивних
функцій вірусів. Необхідно відзначити,
що не існує антивірусних засобів, що
гарантують виявлення всіх можливих
вірусів.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">При
виявленні вірусу необхідно відразу ж
припинити роботу програми-вірусу, щоб
мінімізувати збиток від його впливу на
систему.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Усунення
наслідків впливу вірусів ведеться в
двох напрямках:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">видалення
вірусів;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">відновлення
(при необхідності) файлів, областей
пам'яті.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Відновлення
системи залежить від типу вірусу, а
також від моменту часу виявлення вірусу
по відношенню до початку деструктивних
дій. Відновлення інформації без
використання дублюючої інформації може
бути нездійсненним, якщо віруси при
впровадженні не зберігають інформацію,
на місце якої вони поміщаються в пам'ять,
а також, якщо деструктивні дії вже
почалися, і вони передбачають зміни
інформації.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Для
боротьби з вірусами використовуються
програмні та апаратно-програмні засоби,
які застосовуються в визначено іншої
послідовності і комбінації, утворюючи
методи боротьби з вірусами. Можна
виділити методи виявлення вірусів і
методи видалення вірусів.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>4.1.
                                        Методи виявлення вірусів</B></I></SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Відомі
такі </SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>методи
                                        виявлення вірусів</B></I></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">сканування;					•
виявлення змін;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">евристичний
аналіз;				• вакцинування програм;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">використання
резидентних сторожів;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">апаратно-програмний
захист від вірусів.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Сканування</B></I></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
– один з найпростіших методів виявлення
вірусів. Сканування здійснюється
програмою-сканером, яка переглядає
файли в пошуках розпізнавальної частини
вірусу – сигнатури. Програма фіксує
наявність уже відомих вірусів, за
винятком поліморфних вірусів, які
застосовують шифрування тіла вірусу,
змінюючи при цьому кожен раз і сигнатуру.
Програми-сканери можуть зберігати не
сигнатури відомих вірусів, а їх контрольні
суми. Програми-сканери часто можуть
видаляти виявлені віруси. Такі програми
називаються поліфагами.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Метод
сканування застосуємо для виявлення
вірусів, сигнатури яких вже виділені і
є постійними. Для ефективного використання
методу необхідно регулярне оновлення
відомостей про нові віруси.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I><B>Метод
                                        виявлення змін</B></I></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
базується на використанні </SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I>програм-ревізорів</I></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">.
Ці програми визначають і запам'ятовують
характеристики всіх областей на дисках,
в яких зазвичай розміщуються віруси.
При періодичному виконанні програм
ревізорів порівнюються зберігаються
характеристики і характеристики,
одержувані при контролі областей дисків.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Зазвичай
програми-ревізори запам'ятовують в
спеціальних файлах образи головного
завантажувального запису, завантажувальних
секторів логічних дисків, характеристики
всіх контрольованих файлів, каталогів
та номери дефектних кластерів. Можуть
контролюватися також обсяг встановленої
оперативної пам'яті, кількість підключених
до комп'ютера дисків і їх параметри.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Головною
перевагою методу є можливість виявлення
вірусів всіх типів, а також нових
невідомих вірусів. Досконалі
програми-ревізори виявляють навіть
«стелс»-віруси.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Є
у цього методу і недоліки. За допомогою
програм-ревізорів неможливо визначити
вірус в файлах, які надходять в систему
вже зараженими. Віруси будуть виявлені
тільки після розмноження в системі.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Програми-ревізори
непридатні для виявлення зараження
макровірусами, так як документи і таблиці
дуже часто змінюються.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><I>Евристичний
                                    аналіз</I></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
порівняно недавно почав використовуватися
для виявлення вірусів. Як і метод
виявлення змін, даний метод дозволяє
визначати невідомі віруси, але не вимагає
попереднього збору, обробки та зберігання
інформації про файлову систему.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Сутність
евристичного аналізу полягає в перевірці
можливих середовищ існування вірусів
та виявлення в них команд (груп команд),
характерних для вірусів. Такими командами
можуть бути команди створення резидентних
модулів в оперативній пам'яті, команди
прямого звернення до дисків, минаючи
ОС. Евристичні аналізатори при виявленні
«підозрілих» команд в файлах або
завантажувальних секторах видають
повідомлення про можливе зараження.
Після отримання таких повідомлень
необхідно ретельно перевірити імовірно
заражені файли і завантажувальні сектора
всіма наявними антивірусними засобами.
Евристичний аналізатор є, наприклад, в
антивірусній програмі Doctor Web.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Метод
використання резидентних сторожів
заснований на застосуванні програм, що
постійно знаходяться в ОП ЕОМ і відстежують
всі дії інших програм.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">У
разі виконання якою-небудь програмою
підозрілих дій (звернення для запису в
завантажувальні сектори, приміщення в
ОП резидентних модулів, спроби перехоплення
переривань і т. п.) резидентний сторож
видає повідомлення користувачу.
Програма-сторож може завантажувати на
виконання інші антивірусні програми
для перевірки «підозрілих» програм, а
також для контролю всіх ззовнішніх
файлів (зі змінних дисків, по мережі).</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Істотним
недоліком даного методу є значний
відсоток помилкових тривог, що заважає
роботі користувача, викликає роздратування
і бажання відмовитися від використання
резидентних сторожів.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Під
</SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>вакцинацією</B></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
програм розуміється створення спеціального
модуля для контролю її цілісності. В
якості характеристики цілісності файлу
зазвичай використовується контрольна
сума. При зараженні вакцинованого файлу,
модуль контролю виявляє зміну контрольної
суми і повідомляє про це користувачеві.
Метод дозволяє виявляти всі віруси, у
тому числі і незнайомі, за винятком
«стелс» – вірусів.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Самим
надійним методом захисту від вірусів
є використання апаратно-програмних
антивірусних засобів. В даний час для
захисту ПЕОМ використовуються спеціальні
контролери та їх програмне забезпечення.
Контролер встановлюється в роз'єм
розширення і має доступ до загальної
шини. Це дозволяє йому контролювати всі
звернення до дискової системи. У
програмному забезпеченні контролера
запам'ятовуються області на дисках,
зміна яких в звичайних режимах роботи
не допускається. Таким чином, можна
встановити захист на зміну головного
завантажувального запису, завантажувальних
секторів, файлів конфігурації, виконуваних
файлів та ін.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; widows: 2; orphans: 2"><FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">При
виконанні заборонених дій будь-якою
програмою контролер видає відповідне
повідомлення користувачеві і блокує
роботу ПЕОМ.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Апаратно-програмні
антивірусні засоби мають ряд переваг
перед програмними:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; widows: 2; orphans: 2"> •
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">працюють
постійно;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; widows: 2; orphans: 2"> •
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">виявляють
всі віруси, незалежно від механізму їх
дії;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; widows: 2; orphans: 2"> •
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">блокують
недозволені дії, що є результатом роботи
вірусу або некваліфікованого користувача.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Недолік
у цих засобів один – залежність від
апаратних засобів ПЕОМ. Зміна останніх
веде до необхідності заміни контролера.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; widows: 2; orphans: 2"><BR>
        </P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><I><B><FONT SIZE=4><SPAN LANG="uk-UA">4.2.
Методи видалення наслідків зараження
вірусами</SPAN></FONT></B></I></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">У
процесі видалення наслідків зараження
вірусами здійснюється видалення вірусів,
а також відновлення файлів і областей
пам'яті, в яких знаходився вірус. Існує
два методи видалення наслідків впливу
вірусів антивірусними програмами.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Перший
метод передбачає відновлення системи
після впливу відомих вірусів. Розробник
програми-фага, що видаляє вірус, повинен
знати структуру вірусу і його характеристики
розміщення в середовищі проживання.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Другий
метод дозволяє відновлювати файли і
завантажувальні сектора, заражені
невідомими вірусами. Для відновлення
файлів програма відновлення повинна
завчасно створити і зберігати інформацію
про файли, отриману в умовах відсутності
вірусів. Маючи інформацію про незаражений
файл та використовуючи відомості про
загальні принципи роботи вірусів,
здійснюється відновлення файлів. Якщо
вірус піддав файл незворотним змінам,
то відновлення можливо тільки з
використанням резервної копії або з
дистрибутива. При їх відсутності існує
тільки один вихід – знищити файл і
відновити його вручну.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Якщо
антивірусна програма не може відновити
головний завантажувальний запис або
завантажувальні сектори, то можна
спробувати це зробити вручну. У разі
невдачі слід відформатувати диск і
встановити ОС.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Існують
віруси, які, потрапляючи в ЕОМ, стають
частиною його ОС. Якщо просто видалити
такий вірус, то система стає непрацездатною.</SPAN></FONT></FONT></FONT></FONT></P>
        <P ALIGN=LEFT STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <BR>
        </P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2 STYLE="font-size: 11pt"><B><FONT SIZE=4><SPAN LANG="uk-UA">5.
Профілактика зараження вірусами
комп'ютерних систем</SPAN></FONT></B></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Щоб
убезпечити ЕОМ від впливу вірусів,
користувач, насамперед, повинен мати
уявлення про механізм дії вірусів, щоб
адекватно оцінювати можливість і
наслідки зараження КС. Головною умовою
безпечної роботи в КС є дотримання ряду
правил, які апробовані на практиці і
показали свою високу ефективність.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><U>Правило
                                    перше. </U></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Використання
програмних продуктів, отриманих законним
офіційним шляхом.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Імовірність
наявності вірусу в піратській копії у
багато разів вище, ніж в офіційно
отриманому програмному забезпеченні.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><U>Правило
                                    друге</U></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">.
Дублювання інформації.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Перш
за все, необхідно зберігати дистрибутивні
носії програмного забезпечення. При
цьому запис на носії, що допускають
виконання цієї операції, повинна бути,
по можливості, заблокована. Слід особливо
подбати про збереження робочої інформації.
Переважно регулярно створювати копії
робочих файлів на змінних машинних
носіях інформації з захистом від запису.
Якщо створюється копія на незнімному
носії, то бажано її створювати на інших
ЗЗП або ЕОМ. Копіюється або весь файл,
або тільки вносяться зміни. Останній
варіант застосуємо, наприклад, при
роботі з базами даних.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><U>Правило
                                    третє</U></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">.
Регулярно використовувати антивірусні
засоби. Перед початком роботи доцільно
виконувати програми-сканери та
програми-ревізори (Aidstest та Adinf). Антивірусні
засоби повинні регулярно оновлюватись.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><U>Правило
                                    четверте</U></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">.
Особливу обережність слід проявляти
при використанні нових змінних носіїв
інформації та нових файлів. Нові дискети
обов'язково повинні бути перевірені на
відсутність завантажувальних і файлових
вірусів, а отримані файли – на наявність
файлових вірусів. Перевірка здійснюється
програмами-сканерами і програмами, що
здійснюють евристичний аналіз (Aidstest,
Doctor Web, AntiVirus). При першому виконанні
виконуваного файлу використовуються
резидентні сторожа. При роботі з
отриманими документами і таблицями
доцільно заборонити виконання макрокоманд
засобами, вбудованими в текстові і
табличні редактори (MS Word, MS Excel), до
завершення повної перевірки цих файлів.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><U>Правило
                                    п'яте.</U></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
При роботі в розподілених системах або
в системах колективного користування
доцільно використовувати нові змінні
носії інформації та вводячи в систему
файли перевіряти на спеціально виділених
для цієї мети ЕОМ. Доцільно для цього
використовувати автоматизоване робоче
місце адміністратора системи або особи,
що відповідає за безпеку інформації.
Тільки після всебічної антивірусної
перевірки дисків і файлів вони можуть
передаватися користувачам системи.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-style: normal"><U><SPAN STYLE="font-weight: normal">Правило
шосте.</SPAN></U></SPAN></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><I><B><FONT SIZE=4><SPAN LANG="uk-UA">
</SPAN></FONT></B></I></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-style: normal"><SPAN STYLE="font-weight: normal">Якщо
не передбачається здійснювати запис
інформації на носій, то необхідно
заблокувати виконання цієї операції.
На магнітних дискетах 3,5 дюйма для цього
достатньо відкрити квадратний отвір.</SPAN></SPAN></SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-style: normal"><SPAN STYLE="font-weight: normal">Постійне
проходження всім наведеним рекомендаціям
дозволяє значно зменшити ймовірність
зараження програмними вірусами і захищає
користувача від безповоротних втрат
інформації.</SPAN></SPAN></SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><SPAN STYLE="font-style: normal"><SPAN STYLE="font-weight: normal">В
особливо відповідальних системах для
боротьби з вірусами необхідно
використовувати апаратно-програмні
засоби (наприклад, Sheriff).</SPAN></SPAN></SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; widows: 2; orphans: 2">
            <BR>
        </P>
        <P LANG="ru-RU" ALIGN=JUSTIFY STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2; page-break-after: avoid">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=3><FONT SIZE=4><SPAN LANG="uk-UA"><B>6.
                                    Порядок дій користувача при виявленні
                                    зараження ЕОМ вірусами</B></SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 0; orphans: 0">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Навіть
при скрупульозному виконанні всіх
правил профілактики можливість зараження
ЕОМ комп'ютерними вірусами повністю
виключити не можна. І якщо вірус все ж
потрапив до КС, то результат його
перебування можна звести до мінімуму,
дотримуючись певної послідовності дій.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Про
наявність вірусу в КС користувач може
судити за наступними подіями:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">поява
повідомлень антивірусних засобів про
зараження або про передбачуване
зараженні;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">явні
прояви присутності вірусу, такі як
повідомлення, що видаються на монітор
або принтер, звукові ефекти, знищення
файлів та інші аналогічні дії, що
однозначно вказують на наявність вірусу
в КС;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            • <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">неявні
прояви зараження, які можуть бути
викликані і іншими причинами, наприклад,
збоями або відмовами апаратних і
програмних засобів КС.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">До
неявних проявів наявності вірусів у КС
можна віднести «зависання» системи,
уповільнення виконання певних дій,
порушення адресації, збої пристроїв
тощо.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Отримавши
інформацію про передбачуване зараження,
користувач повинен переконатися в
цьому. Вирішити таке завдання можна за
допомогою всього комплексу антивірусних
засобів. Переконавшись у тому, що
зараження відбулося, користувачеві
слід виконати таку послідовність кроків:</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Крок
                                    1. </B></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Вимкнути
ЕОМ для знищення резидентних вірусів.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 0; orphans: 0">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Крок
                                    2. </B></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Здійснити
завантаження еталонної операційної
системи зі змінного носія інформації,
в якій відсутні віруси.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Крок
                                    3.</B></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
Зберегти на змінних носіях інформації
важливі для вас файли, які не мають
резервних копій.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Крок
                                    4</B></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">.
Використовувати антивірусні засоби
для видалення вірусів і відновлення
файлів, областей пам'яті. Якщо працездатність
ЕОМ відновлена, то здійснюється перехід
до кроку 8, інакше – до кроку 5.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Крок
                                    5.</B></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
Здійснити повне стирання і розмітку
(форматування) незнімних зовнішніх
запам'ятовуючих пристроїв. У ПЕОМ для
цього можуть бути використані програми
MS-DOS FDISK і FORMAT. Програма форматування
FORMAT не видаляє головний завантажувальний
запис на жорсткому диску, в якій може
знаходитися завантажувальний вірус.
Тому необхідно виконати програму FDISK з
не документованим параметром MBR, створити
за допомогою цієї ж програми розділи
та логічні диски на жорсткому диску.
Потім виконується програма FORMAT для всіх
логічних дисків.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Крок
                                    6.</B></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
Відновити ОС, інші програмні системи і
файли з дистрибутивів і резервних копій,
створених до зараження.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 2; orphans: 2">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Крок
                                    7.</B></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">
Ретельно перевірити файли, збережені
після виявлення зараження, і, при
необхідності, видалити віруси і відновити
файли;</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" STYLE="text-indent: 0.5in; line-height: 100%; widows: 0; orphans: 0">
            <FONT FACE="Arial Unicode MS, serif"><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA"><B>Крок
                                    8. </B></SPAN></FONT></FONT></FONT><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">Завершити
відновлення інформації всебічної
перевіркою ЕОМ за допомогою всіх наявних
у розпорядженні користувача антивірусних
засобів.</SPAN></FONT></FONT></FONT></FONT></P>
        <P LANG="ru-RU" CLASS="western" ALIGN=LEFT STYLE="text-indent: 0in"><FONT SIZE=2><FONT FACE="Times New Roman, serif"><FONT SIZE=2><FONT SIZE=4><SPAN LANG="uk-UA">При
виконанні рекомендацій з профілактики
зараження комп'ютерними вірусами, а
також при умілих і своєчасних діях у
разі зараження, вірусами, збиток
інформаційних ресурсів КС може бути
зведений до мінімуму.</SPAN></FONT></FONT></FONT></FONT></P>
    </div></div>
</BODY>
</HTML>
