<?php
if ($_SESSION['email']) {
    echo "connection<script>document.location.replace('/code/php_file_connection/login.html');</script>";
    exit;
}
if (isset($_REQUEST[session_name()])) session_start();
include_once('/topMenu.ini.php');
?>
<!DOCTYPE html>
<html lang="ua">
<body>
<script src="/styles/js/cards.js"></script>
<div class="">
    <script type="text/javascript" src="/styles/js/jquery.js"></script>
    <style type="text/css">
        @import url(/styles/css/css.css);
    </style>
    <script type="text/javascript" src="/styles/js/loader.js"></script>
    <!--<div class="cards-wrapper">-->
    <div id="wrapper">
        <ul id = "nav">
            <li><a href="/learn_files/lecture/z1.php"><span class="glyphicon glyphicon-file"></span></a></li>

            <li><a href="/learn_files/test/tst1.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z2.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst2.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z3.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst3.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z4.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst4.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z5.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst5.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z6.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst6.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z7.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst7.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z8.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst8.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z9.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst9.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z10.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst10.php"><span class="glyphicon glyphicon-check"></a></li>
        </ul>
        <div id="content">
            <form method="post" action="/code/php_file/count/count8.php" class="form-signin" role="form">
                <h3>Методи та засоби захисту від інформаційних випромінювань і наведень</h3>
                <ol>
                    <li>Всі методи захисту від електромагнітних випромінювань і наведень можна розділити на ...
                    </li>
                    <input type="checkbox" name="t1" value="true">пасивні<br>
                    <input type="checkbox" name="t1" value="true">активні<br>
                    <input type="checkbox" name="t1" value="false">організаційні<br>
                    <input type="checkbox" name="t1" value="false">захисні
                    <li>Які методи забезпечують зменшення рівня  небезпечного сигналу або зниження інформативності сигналу?</li>
                    <input type="radio" name="t2" value="false">активні<br>
                    <input type="radio" name="t2" value="true">пасивні<br>
                    <input type="radio" name="t2" value="false">захисні<br>
                    <input type="radio" name="t2" value="false">організаційні
                    <li>Які методи захисту спрямовані на створення перешкод у каналах побічних електромагнітних випромінювань і наведень?
                    </li>
                    <input type="radio" name="t3" value="false">пасивні і активні<br>
                    <input type="radio" name="t3" value="false"> правові<br>
                    <input type="radio" name="t3" value="false">суспільні<br>
                    <input type="radio" name="t3" value="true">актові
                    <li>Скільки груп захисту від ПЕМВН можуть мати пасивні методи захисту?
                    </li>
                    <input type="radio" name="t4" value="false"більше десяти><br>
                    <input type="radio" name="t4" value="false">п'ять<br>
                    <input type="radio" name="t4" value="false">одну<br>
                    <input type="radio" name="t4" value="true">три
                    <li>Одним з найефективніших методів захисту від електромагнітних випромінювань є...?
                    </li>
                    <input type="radio" name="t5" value="true">екранування<br>
                    <input type="radio" name="t5" value="false">зниження потужності випромінювань і наведень<br>
                    <input type="radio" name="t5" value="false">зниження інформаційності сигналів<br>
                    <input type="radio" name="t5" value="false">шифрування
                    <li>Характеристики полів залежать від параметрів електричних сигналів в КС.
                    </li>
                    <input type="radio" name="t6" value="true">так<br>
                    <input type="radio" name="t6" value="false">ні
                    <li>Як називається поле в якому при малих струмах і високих напругах переважає електрична складова?
                    </li>
                    <input type="checkbox" name="t7" value="false">електромагнітним<br>
                    <input type="checkbox" name="t7" value="true">електростатичним<br>
                    <input type="checkbox" name="t7" value="true">електричним<br>
                    <input type="checkbox" name="t7" value="false">магнітним
                    <li>Як називається поле якщо в провіднику протікає струм великої величини при малих значеннях напруги?
                    </li>
                    <input type="radio" name="t8" value="false">електростатичне<br>
                    <input type="radio" name="t8" value="false">електромагнітне<br>
                    <input type="radio" name="t8" value="true">магнітне<br>
                    <input type="radio" name="t8" value="false">електричне
                    <li>Для чого роблять заземлення металевим екраном екранування електричного поля?
                    </li>
                    <input type="radio" name="t9" value="true">щоб забеспечити нейтралізацію електричних зарядів<br>
                    <input type="radio" name="t9" value="false">щоб забеспечити електричний заряд<br>
                    <input type="radio" name="t9" value="false">щоб струм протікав швидше<br>
                    <input type="radio" name="t9" value="false"> щоб підвишити електричні проникність
                    <li>Приекрануванні магнітних полів розрізняють два види мангітних полів а саме...
                    </li>
                    <input type="checkbox" name="t10" value="true">низькочастотні<br>
                    <input type="checkbox" name="t10" value="false">середньочастотні<br>
                    <input type="checkbox" name="t10" value="true">високочастотні<br>
                    <input type="checkbox" name="t10" value="false">помірночастотні
                    <li>Чим нижче частота випромінювання, тим більшою повинна бути товщина екрана.
                    </li>
                    <input type="radio" name="t11" value="true">так<br>
                    <input type="radio" name="t11" value="false">ні
                    <li>Якої товщини повинна бути мідь або срібло на екрані для випромінюванні на частотах понад 10 МГц?
                    </li>
                    <input type="radio" name="t12" value="false">0,01мм<br>
                    <input type="radio" name="t12" value="false">1см<br>
                    <input type="radio" name="t12" value="false">0,5-1,5мм<br>
                    <input type="radio" name="t12" value="true">0,1мм
                    <li>На скількох рівнях здійснюється екранування?
                    </li>
                    <input type="radio" name="t13" value="false">3<br>
                    <input type="radio" name="t13" value="true">5<br>
                    <input type="radio" name="t13" value="false">7<br>
                    <input type="radio" name="t13" value="false">2
                    <li>Число рівнів та матеріалів екранування здійснюється з урахуванням...
                    </li>
                    <input type="checkbox" name="t14" value="true">характеристик випромінювання<br>
                    <input type="checkbox" name="t14" value="false">наявність електромагнітних полів<br>
                    <input type="checkbox" name="t14" value="true">відсутності інших методів захисту від ПЕМВН<br>
                    <input type="checkbox" name="t14" value="true">мінімізації затрат на екранування
                    <li> Перспективним напрямком боротьби з ПЕМВН є ...
                    </li>
                    <input type="radio" name="t15" value="false">використання фільтрів<br>
                    <input type="radio" name="t15" value="false">використання електричних схем<br>
                    <input type="radio" name="t15" value="true">використання оптичних каналів зв'язку<br>
                    <input type="radio" name="t15" value="false">використання волокнистих кабелів
                    <li>Які кабелі використовують для передачі інформації на великі відстані?
                    </li>
                    <input type="radio" name="t16" value="false">оптичні<br>
                    <input type="radio" name="t16" value="false">волокнисті<br>
                    <input type="radio" name="t16" value="false">металеві<br>
                    <input type="radio" name="t16" value="true">волокнисто-оптичні
                    <li>Зменшення області дії електростатичного поля в середовищі з вільними носіями заряду називається...
                    </li>
                    <input type="radio" name="t17" value="false">фільтрація заряду<br>
                    <input type="radio" name="t17" value="true">екранування заряду<br>
                    <input type="radio" name="t17" value="false">випромінювання заряду<br>
                    <input type="radio" name="t17" value="false">гальмування заряду

                    <li> Що є основним способом захисту від ПЕМВН?
                    </li>
                    <input type="radio" name="t18" value="false">використання оптичних каналів зв'язку<br>
                    <input type="radio" name="t18" value="false">використання волокнисто-оптичних кабелів<br>
                    <input type="radio" name="t18" value="true">використання фільтрів<br>
                    <input type="radio" name="t18" value="false">зміна елекричниз схем
                    <li>Що дозволяє згладжувати пульсацію напруги і короткочасні відключення в  первинному ланцюгу?
                    </li>
                    <input type="radio" name="t19" value="false">фільтр<br>
                    <input type="radio" name="t19" value="true">генератири живлення<br>
                    <input type="radio" name="t19" value="false">гальванічна розв'язка в системі живлення<br>
                    <input type="radio" name="t19" value="false">зміна конструкцій
                    <li>Для зниження інформаційності сигналів ПЕМВН використовують...?
                    </li>
                    <input type="checkbox" name="t20" value="false">фільтри<br>
                    <input type="checkbox" name="t20" value="true">спеціальні схеми рішення<br>
                    <input type="checkbox" name="t20" value="true">кодування інформації<br>
                    <input type="checkbox" name="t20" value="false">екранування
                    <li>Для чого використовують кодування інформації?
                    </li>
                    <input type="radio" name="t21" value="false">для збереження<br>
                    <input type="radio" name="t21" value="false">для захисту від вірусів<br>
                    <input type="radio" name="t21" value="true">для запобігання витоку<br>
                    <input type="radio" name="t21" value="false">для конфеденційності
                    <li>Для яких методів захисту від ПЕМВН застосовують генератори шуму?
                    </li>
                    <input type="radio" name="t22" value="false">пасивних<br>
                    <input type="radio" name="t22" value="false">конструктивних<br>
                    <input type="radio" name="t22" value="true">активних<br>
                    <input type="radio" name="t22" value="false">комбінованих
                    <li>Які види генераторного зашумлення використовують?
                    </li>
                    <input type="checkbox" name="t23" value="false">групове<br>
                    <input type="checkbox" name="t23" value="true">лінійне<br>
                    <input type="checkbox" name="t23" value="false">системне<br>
                    <input type="checkbox" name="t23" value="true">просторове
                    <li> Інформаційні ресурси- це...
                    </li>
                    <input type="radio" name="t24" value="false">інформація з інтернетних ресурсів<br>
                    <input type="radio" name="t24" value="false">інформаційні документи з правом користування ними<br>
                    <input type="radio" name="t24" value="true">документи і масиви документів в інформаційних системах<br>
                    <input type="radio" name="t24" value="false">засоби захисту інформації
                    <li>Сукупність методів і засобів, що забеспечують цілісність, конфіденційність і доступність інформації насивається...
                    </li>
                    <input type="radio" name="t25" value="false">спрямованість інформації<br>
                    <input type="radio" name="t25" value="true">захист інформації<br>
                    <input type="radio" name="t25" value="false">захист інформаційних ресурсів<br>
                    <input type="radio" name="t25" value="false">інформативність сигналів


                </ol>
                <input type="submit" class="btn3 buttton btn btn-lg btn-primary" name="enter" value="Завершити" />
                <?php $test= 'test1';?>
            </form>
        </div>
        <div id="foot">Copyright 2015, Nakslearn, Inc. by <a href="http://lnkd.in/dgaEqmB">Roman Semenyuk</a>.</div>
    </div>
</div>
</div>
</div>
</body>
</html>