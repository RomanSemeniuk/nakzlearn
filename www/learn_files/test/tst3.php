<?php
if ($_SESSION['email']) {
    echo "connection<script>document.location.replace('/code/php_file_connection/login.html');</script>";
    exit;
}
if (isset($_REQUEST[session_name()])) session_start();
include_once('/topMenu.ini.php');
?>
<!DOCTYPE html>
<html lang="ua">
<body>
<script src="/styles/js/cards.js"></script>
<div class="">
    <script type="text/javascript" src="/styles/js/jquery.js"></script>
    <style type="text/css">
        @import url(/styles/css/css.css);
    </style>
    <script type="text/javascript" src="/styles/js/loader.js"></script>
    <!--<div class="cards-wrapper">-->
    <div id="wrapper">
        <ul id = "nav">
            <li><a href="/learn_files/lecture/z1.php"><span class="glyphicon glyphicon-file"></span></a></li>

            <li><a href="/learn_files/test/tst1.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z2.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst2.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z3.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst3.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z4.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst4.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z5.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst5.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z6.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst6.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z7.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst7.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z8.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst8.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z9.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst9.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z10.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst10.php"><span class="glyphicon glyphicon-check"></a></li>
        </ul>
        <div id="content">
            <form method="post" action="/code/php_file/count/count3.php" class="form-signin" role="form">
                <ol>
                    <li>Державна інформаційна політика - це ...</li>
                    <input type="radio" name="t1" value="true"> сукупність основних напрямів і методів діяльності держави по
                    одержанню, використанню, поширенню та зберіганню інформації <br>
                    <input type="radio" name="t1" value="false"> сукупність основних способів і методів діяльності держави в
                    сфері політики<br>
                    <input type="radio" name="t1" value="false"> сукупність основних напрямів діяльності держави по
                    поширенню та зберіганню інформації<br>
                    <input type="radio" name="t1" value="false">інший варіант
                    <li>Основні принципи інформаційних відносин
                    </li>
                    <input type="radio" name="t2" value="true"> повнота і точність інформації<br>
                    <input type="radio" name="t2" value="false"> недоступність та обмеженість доступу<br>
                    <input type="radio" name="t2" value="false">недостовірність інформації
                    <li>Вкажіть основні види інформації
                    </li>
                    <input type="radio" name="t3" value="false"> юридична інформація<br>
                    <input type="radio" name="t3" value="true"> статистична інформація<br>
                    <input type="radio" name="t3" value="false"> вигадана інформація<br>
                    <li>Право власності на інформацію - це...
                    </li>
                    <input type="radio" name="t4" value="true"> врегульовані законом суспільні відносини щодо розповсюдження
                    і розпорядження інформацією<br>
                    <input type="radio" name="t4" value="false"> право на продаж інформації одних осіб іншим<br>
                    <input type="radio" name="t4" value="false"> врегульовані законом суспільні відносини щодо володіння,
                    користування і розпорядження інформацією<br>
                    <input type="radio" name="t4" value="false">інший варіант
                    <li>В яких випадках особа порушує законодавство про інформацію?
                    </li>
                    <input type="radio" name="t5" value="true"> необгрунтована відмова у наданні відповідної інформації<br>
                    <input type="radio" name="t5" value="true"> порушенння порядку зберігання інформації<br>
                    <input type="radio" name="t5" value="false"> відмова у наданні доступу до засекреченої інформації<br>
                    <input type="radio" name="t5" value="false">все вище перераховане
                    <li>Головними напрямами і методами державної інформаційної політики є:
                    </li>
                    <input type="radio" name="t6" value="true"> забезпечення доступу громадян до інформації<br>
                    <input type="radio" name="t6" value="true"> створення загальної системи охорони інформації<br>
                    <input type="radio" name="t6" value="true"> створення національних систем і мереж інформації<br>
                    <input type="radio" name="t6" value="false">інший варіант
                    <li>Найважливішими завданнями держави в галузі інформаційної безпеки є:
                    </li>
                    <input type="radio" name="t7" value="false"> гарантованість права на інформацію<br>
                    <input type="radio" name="t7" value="false"> законність одержання інформації<br>
                    <input type="radio" name="t7" value="true"> встановлення необхідного балансу між потребою у вільному
                    обміні інформацією і допустимими обмеженнями її розповсюдження<br>
                    <input type="radio" name="t7" value="false">забезпечення доступу громадян до інформації
                    <li>Державна політика повинна забезпечити в країні захист інформації...
                    </li>
                    <input type="radio" name="t8" value="true"> тільки в межах держави<br>
                    <input type="radio" name="t8" value="false"> на рівні організацій та окремих громадян<br>
                    <input type="radio" name="t8" value="true">все вище перераховане
                    <li>. Що з перерахованого не відноситься до законодавчої бази інформатизації суспільства?
                    </li>
                    <input type="radio" name="t9" value="false"> Закон України "Про інформацію"<br>
                    <input type="radio" name="t9" value="false"> Закон України "Про державну таємницю"<br>
                    <input type="radio" name="t9" value="false"> Концепція технічного захисту інформації в Україні<br>
                    <input type="radio" name="t9" value="true">Інший варіант
                    <li>Одним із перших законодавчих актів у сфері захисту інформаціїз обмеженим доступом став...
                    </li>
                    <input type="radio" name="t10" value="false">Закон України "Про державну таємницю" <br>
                    <input type="radio" name="t10" value="true">Закон України "Про інформацію" <br>
                    <input type="radio" name="t10" value="false">Концепція технічного захисту інформації в Україні
                    <li>Суб’єктами інформаційних відносин є...
                    </li>
                    <input type="radio" name="t11" value="false"> громадяни України<br>
                    <input type="radio" name="t11" value="false"> юридичні особи<br>
                    <input type="radio" name="t11" value="false"> держава<br>
                    <input type="radio" name="t11" value="false"> інші держави<br>
                    <input type="radio" name="t11" value="true">все вище перераховане
                    <li>Режим доступу до інформації - це...
                    </li>
                    <input type="radio" name="t12" value="true"> передбачений правовими нормами порядок одержання,
                    використання, поширення і зберігання інформації<br>
                    <input type="radio" name="t12" value="false"> передбачений правовими нормами порядок опрацювання
                    інформації<br>
                    <input type="radio" name="t13" value="false"> передбачений правовими нормами порядок одержання, доступу
                    до інформації<br>
                    <input type="radio" name="t13" value="false">порядок використання і поширення інформації
                    <li>За режимом доступу інформація поділяється...
                    </li>
                    <input type="radio" name="t14" value="false"> на відкриту та закриту<br>
                    <input type="radio" name="t14" value="true"> на відриту та інформацію з обмеженим доступом<br>
                    <input type="radio" name="t14" value="false"> на таємну та загальнодоступну<br>
                    <input type="radio" name="t14" value="false">інший варіант
                    <li>Інформація з обмеженим доступом за своїм правовим режимом поділяється...
                    </li>
                    <input type="radio" name="t15" value="true"> на конфіденційну і таємну<br>
                    <input type="radio" name="t15" value="false"> на обмежену і кофіденційну<br>
                    <input type="radio" name="t15" value="false"> на таємну і обмежену<br>
                    <input type="radio" name="t15" value="false">на закриту і відкриту
                    <li>Учасниками інформаційних відносин є:
                    </li>
                    <input type="radio" name="t16" value="true"> автори та споживачі інформації<br>
                    <input type="radio" name="t16" value="true"> поширювачі інформації<br>
                    <input type="radio" name="t16" value="true"> зберігачі інформації<br>
                    <input type="radio" name="t16" value="false">винищувачі інформації
                </ol>
                <input type="submit" class="btn3 buttton btn btn-lg btn-primary" name="enter" value="Завершити" />
                <?php $test= 'test3';?>
            </form>
        </div>
        <div id="foot">Copyright 2015, Nakslearn, Inc. by <a href="http://lnkd.in/dgaEqmB">Roman Semenyuk</a>.</div>
    </div>
</div>
</div>
</div>
</body>
</html>