<?php
if ($_SESSION['email']) {
    echo "connection<script>document.location.replace('/code/php_file_connection/login.html');</script>";
    exit;
}
if (isset($_REQUEST[session_name()])) session_start();
include_once('/topMenu.ini.php');
?>
<!DOCTYPE html>
<html lang="ua">
<body>
<script src="/styles/js/cards.js"></script>
<div class="">
    <script type="text/javascript" src="/styles/js/jquery.js"></script>
    <style type="text/css">
        @import url(/styles/css/css.css);
    </style>
    <script type="text/javascript" src="/styles/js/loader.js"></script>
    <!--<div class="cards-wrapper">-->
    <div id="wrapper">
        <ul id = "nav">
            <li><a href="/learn_files/lecture/z1.php"><span class="glyphicon glyphicon-file"></span></a></li>

            <li><a href="/learn_files/test/tst1.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z2.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst2.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z3.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst3.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z4.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst4.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z5.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst5.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z6.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst6.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z7.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst7.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z8.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst8.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z9.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst9.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z10.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst10.php"><span class="glyphicon glyphicon-check"></a></li>
        </ul>
        <div id="content">
            <form method="post" action="/code/php_file/count/count5.php" class="form-signin" role="form">
                <ol>
                    <li>Згідно з Законом України «Про інформацію» під захистом інформації розуміють…</li>
                    <input type="radio" name="t1" value="true">
                    перетворення інформації з використанням спеціальних даних з метою приховування змісту інформації,
                    підтвердження її справжності, цілісності;
                    <br>
                    <input type="radio" name="t1" value="false">
                    сукупність методів та засобів, що забезпечують цілісність, конфіденційність і доступність інформації
                    за
                    умов впливу на неї загроз природного або штучного характеру;
                    <br>
                    <input type="radio" name="t1" value="false">
                    сукупність правових, адміністративних, організаційних, технічних та інших заходів, що забезпечують
                    збереження, цілісність інформації та належний порядок доступу до неї;
                    <br>
                    <input type="radio" name="t1" value="false">
                    діяльність, спрямовану на забезпечення інженерно-технічними заходами конфіденційності, цілісності та
                    доступності інформації.
                    <li>Суб’єктами інформаційних відносин є …</li>
                    <input type="radio" name="t2" value="true">фізичні особи, юридичні особи, інформація; <br>
                    <input type="radio" name="t2" value="false">інформація, об’єднання громадян, суб’єкти владних
                    повноважень; <br>
                    <input type="radio" name="t2" value="false">суб’єкти владних повноважень, фізичні особи, юридичні
                    особи;
                    <br>
                    <input type="radio" name="t2" value="false">юридичні особи, інформація, об’єднання громадян.
                    <li>Предметом суспільного інтересу не вважається інформація, яка ….</li>
                    <input type="radio" name="t3" value="true">свідчить про загрозу державному суверенітету; <br>
                    <input type="radio" name="t3" value="false">свідчить про можливість порушення прав людини, введення
                    громадськості в оману; <br>
                    <input type="radio" name="t3" value="false">забезпечує реалізацію конституційних прав; <br>
                    <input type="radio" name="t3" value="false">свідчить про фізичну особу, яка ідентифікована або може
                    бути
                    конкретно ідентифікована.
                    <li>Які дані відносять до інформації про довкілля?</li>
                    <input type="radio" name="t4" value="true">дані про стан здоров'я та безпеки людей, умови життя
                    людей,
                    стан об'єктів культури і споруд тією мірою, якою на них впливає або може вплинути стан складових
                    довкілля; <br>
                    <input type="radio" name="t4" value="false">відомості про ставлення до окремих осіб, подій, явищ,
                    процесів, фактів тощо; <br>
                    <input type="radio" name="t4" value="false"> дані, що дають кількісну характеристику масових явищ та
                    процесів, які відбуваються в економічній, соціальній, культурній та інших сферах життя
                    суспільства;<br>
                    <input type="radio" name="t4" value="false">відомості про право, його систему, джерела, реалізацію,
                    юридичні факти, правовідносини, правопорядок, правопорушення.
                    <li>До інформації з обмеженим доступом не можуть бути віднесені такі відомості…</li>
                    <input type="radio" name="t5" value="true"> про незаконні дії органів державної влади, органів
                    місцевого
                    самоврядування, їх посадових та службових осіб;<br>
                    <input type="radio" name="t5" value="false"> про фізичну особу, а також інформація, доступ до якої
                    обмежено фізичною або юридичною особою;<br>
                    <input type="radio" name="t5" value="false"> про аварії, катастрофи, небезпечні природні явища та
                    інші
                    надзвичайні ситуації, що сталися або можуть статися і загрожують безпеці людей;<br>
                    <input type="radio" name="t5" value="false">про факти порушення прав і свобод людини і громадянина.
                    <li>Що не є джерелом правової інформації?</li>
                    <input type="radio" name="t6" value="true"> Конституція України;<br>
                    <input type="radio" name="t6" value="false"> архіви різноманітних довідкових інформаційних служб;<br>
                    <input type="radio" name="t6" value="false"> ненормативні правові акти;<br>
                    <input type="radio" name="t6" value="false">повідомлення засобів масової інформації, публічні виступи
                    з
                    правових питань.
                    <li>Інформація довідково-енциклопедичного характеру – це…</li>
                    <input type="radio" name="t7" value="true"> відомості та/або дані, які розкривають кількісні, якісні
                    та
                    інші характеристики товару;<br>
                    <input type="radio" name="t7" value="false"> будь-які відомості та/або дані про вітчизняні та
                    зарубіжні
                    досягнення науки, техніки і виробництва, одержані в ході науково-дослідної,
                    дослідно-конструкторської,
                    проектно-технологічної, виробничої та громадської діяльності, які можуть бути збережені на
                    матеріальних
                    носіях або відображені в електронному вигляді;<br>
                    <input type="radio" name="t7" value="false"> документована інформація, що дає кількісну
                    характеристику
                    масових явищ та процесів, які відбуваються в економічній, соціальній, культурній та інших сферах
                    життя
                    суспільства;<br>
                    <input type="radio" name="t7" value="false">систематизовані, документовані, публічно оголошені або
                    іншим
                    чином поширені відомості про суспільне, державне життя та навколишнє природне середовище.
                    <li>До основних видів інформаційної діяльності відносять:</li>
                    <input type="radio" name="t8" value="true"> створення, збирання, одержання, видалення;<br>
                    <input type="radio" name="t8" value="false"> зберігання, використання, поширення, блокування;<br>
                    <input type="radio" name="t8" value="false"> збирання, охорона, зберігання, одержання;<br>
                    <input type="radio" name="t8" value="false">захист, знищення, поширення, створення.
                    <li>Які основні напрями інформаційної діяльності?</li>
                    <input type="radio" name="t9" value="true"> політичний, економічний, комп’ютерний, екологічний;<br>
                    <input type="radio" name="t9" value="false"> міжнародний, соціальний, політичний, духовний;<br>
                    <input type="radio" name="t9" value="false"> духовний, науково-технічний, спортивний,
                    біологічний;<br>
                    <input type="radio" name="t10" value="false">екологічний, економічний, міжнародний, суспільний.
                    <li>Які принципи не належать до принципів інформаційних відносин?</li>
                    <input type="radio" name="t10" value="true"> рівноправність, незалежно від ознак раси, політичних,
                    релігійних та інших переконань, статі, етнічного та соціального походження, майнового стану, місця
                    проживання, мовних або інших ознак, вільне отримання та поширення інформації, крім обмежень,
                    встановлених законом;<br>
                    <input type="radio" name="t10" value="false"> відкритість, доступність інформації, свобода обміну
                    інформацією, захищеність особи від втручання в її особисте та сімейне життя;<br>
                    <input type="radio" name="t10" value="false"> достовірність і повнота інформації, свобода вираження
                    поглядів і переконань;<br>
                    <input type="radio" name="t10" value="false">гарантованість права на інформацію, правомірність
                    одержання,
                    використання, поширення, зберігання та захисту інформації.
                </ol>
                <input type="submit" class="btn3 buttton btn btn-lg btn-primary" name="enter" value="Завершити"/>
                <?php $test = 'test5'; ?>
            </form>
        </div>
        <div id="foot">Copyright 2015, Nakslearn, Inc. by <a href="http://lnkd.in/dgaEqmB">Roman Semenyuk</a>.</div>
    </div>
</div>
</div>
</div>
</body>
</html>