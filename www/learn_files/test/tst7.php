<?php
if ($_SESSION['email']) {
    echo "connection<script>document.location.replace('/code/php_file_connection/login.html');</script>";
    exit;
}
if (isset($_REQUEST[session_name()])) session_start();
include_once('/topMenu.ini.php');
?>
<!DOCTYPE html>
<html lang="ua">
<body>
<script src="/styles/js/cards.js"></script>
<div class="">
    <script type="text/javascript" src="/styles/js/jquery.js"></script>
    <style type="text/css">
        @import url(/styles/css/css.css);
    </style>
    <script type="text/javascript" src="/styles/js/loader.js"></script>
    <!--<div class="cards-wrapper">-->
    <div id="wrapper">
        <ul id = "nav">
            <li><a href="/learn_files/lecture/z1.php"><span class="glyphicon glyphicon-file"></span></a></li>

            <li><a href="/learn_files/test/tst1.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z2.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst2.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z3.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst3.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z4.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst4.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z5.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst5.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z6.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst6.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z7.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst7.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z8.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst8.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z9.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst9.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z10.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst10.php"><span class="glyphicon glyphicon-check"></a></li>
        </ul>
        <div id="content">
            <form method="post" action="/code/php_file/count/count7.php" class="form-signin" role="form">
                <h3>Методи і засоби захисту інформації в КС від традиційного шпигунства і диверсій.</h3>
                <ol>
                    <li>Для захисту об’єктів КС від традиційного шпигунства і диверсій повинні бути вирішені такі завдання:</li>
                    <input type="radio" name="t1" value="false">створення системи охорони об’єкта;<br>
                    <input type="radio" name="t1" value="true">протидія спостереженню;<br>
                    <input type="radio" name="t1" value="true">захист від зловмисників;<br>
                    <input type="radio" name="t1" value="true">протидія підслуховування;<br>
                    <input type="radio" name="t1" value="false">організація робіт з конфіденційними інформаційними ресурсами на об’єкті КС.
                    <li>Від шпигунства і диверсій необхідно захищати обслуговуючий персонал і рубежі. Вкажіть ці рубежі.</li>
                    <input type="radio" name="t2" value="false">інформаційні ресурси;<br>
                    <input type="radio" name="t2" value="true">контрольована територія;<br>
                    <input type="radio" name="t2" value="true">програма;<br>
                    <input type="radio" name="t2" value="true">будівля;<br>
                    <input type="radio" name="t2" value="true">пристрій, носій інформації;<br>
                    <input type="radio" name="t2" value="false">приміщення.
                    <li>З якою метою створюється система охорони об’єкта (СОО) КС?</li>
                    <input type="radio" name="t3" value="false">запобігання несанкціонованого проникнення на територію та в приміщення об’єкта сторонніх осіб, дітей та підлітків;<br>
                    <input type="radio" name="t3" value="true">запобігання несанкціонованого проникнення на територію та в приміщення обслуговуючого персоналу і користувачів;<br>
                    <input type="radio" name="t3" value="true">запобігання несанкціонованого проникнення на територію та в приміщення об’єкта сторонніх осіб, обслуговуючого персоналу і користувачів;<br>
                    <input type="radio" name="t3" value="false">запобігання санкціонованого проникнення на територію та в приміщення об’єкта сторонніх осіб, обслуговуючого персоналу і користувачів;
                    <li>Від чого залежить склад системи охорони КС?
                    </li>
                    <input type="radio" name="t4" value="false">від об’єкту, що охороняється;<br>
                    <input type="radio" name="t4" value="true">від системи охоронних засобів;<br>
                    <input type="radio" name="t4" value="true">від технічного стану будівлі, що охороняється;<br>
                    <input type="radio" name="t4" value="true">перша і друга відповіді правильні.
                    <li>У загальному випадку СОО КС повинна включати наступні компоненти:
                    </li>
                    <input type="radio" name="t5" value="false">засоби спостереження;<br>
                    <input type="radio" name="t5" value="true">підсистема доступу на об’єкт;<br>
                    <input type="radio" name="t5" value="true">шпигунські пристрої;<br>
                    <input type="radio" name="t5" value="true">інженерні конструкції;<br>
                    <input type="radio" name="t5" value="true">охоронна сигналізація;<br>
                    <input type="radio" name="t5" value="false">чергова зміна охорони.
                    <li>Для чого служать інженерні конструкції?
                    </li>
                    <input type="radio" name="t6" value="false">для покращення ефективності праці;<br>
                    <input type="radio" name="t6" value="true">для створення механічних перешкод на шляху зловмисника;<br>
                    <input type="radio" name="t6" value="true">для створення технічних перешкод;<br>
                    <input type="radio" name="t6" value="false">друга і третя відповіді правильні.
                    <li>Де створюються інженерні конструкції?
                    </li>
                    <input type="radio" name="t7" value="false">на всій території контрольованої зони;<br>
                    <input type="radio" name="t7" value="true">з передньої сторони контрольованої зони;<br>
                    <input type="radio" name="t7" value="true">з задньої сторони контрольованої зони;<br>
                    <input type="radio" name="t7" value="false">по периметру контрольованої зони.
                    <li>Що в першу чергу зміцнюють за допомогою інженерних конструкцій?
                    </li>
                    <input type="radio" name="t8" value="false">двері;<br>
                    <input type="radio" name="t8" value="true">вікна;<br>
                    <input type="radio" name="t8" value="true">кришу приміщення;<br>
                    <input type="radio" name="t8" value="false">перші дві відповіді вірні.
                    <li>Які замки мають найвищу стійкість?
                    </li>
                    <input type="radio" name="t9" value="false">механічні замки;<br>
                    <input type="radio" name="t9" value="true">кодові замки;<br>
                    <input type="radio" name="t9" value="true">електронні замки;<br>
                    <input type="radio" name="t9" value="false">сейфові замки.
                    <li>Якими шляхами найчастіше відбувається проникнення на об’єкти за статистикою?
                    </li>
                    <input type="radio" name="t10" value="false">через двері;<br>
                    <input type="radio" name="t10" value="true">через віконні отвори;<br>
                    <input type="radio" name="t10" value="true">через кришу приміщення;<br>
                    <input type="radio" name="t10" value="false">всі відповіді вірні.
                    <li>Інженерне укріплення вікон здійснюється такими шляхами:
                    </li>
                    <input type="radio" name="t11" value="false">реставрація вікон;<br>
                    <input type="radio" name="t11" value="true">установка віконних решіток;<br>
                    <input type="radio" name="t11" value="true">установка віконних жалюзі;<br>
                    <input type="radio" name="t11" value="false">застосування стекол, стійких до механічного впливу.
                    <li>Послідовно вкажіть напрями підвищення механічної міцності стекол:
                    </li>
                    <input type="radio" name="t12" value="false">загартовування скла;<br>
                    <input type="radio" name="t12" value="true">виготовлення багатошарових стекол;<br>
                    <input type="radio" name="t12" value="false">застосування захисних плівок.
                    <li>Для чого служить охоронна сигналізація?
                    </li>
                    <input type="radio" name="t13" value="false">для виявлення спроб несанкціонованого проникнення на об’єкт, що охороняється;<br>
                    <input type="radio" name="t13" value="true">щоб налякати зловмисника;<br>
                    <input type="radio" name="t13" value="true">для попередження спроб несанкціонованого проникнення на об’єкт, що охороняється;<br>
                    <input type="radio" name="t13" value="false">для затримання зловмисника.
                    <li>Вкажіть вимоги до системи охоронної сигналізації:
                    </li>
                    <input type="radio" name="t14" value="false">стійкість до природних перешкод;<br>
                    <input type="radio" name="t14" value="true">надійна робота в будь-яких погодних і часових умовах;<br>
                    <input type="radio" name="t14" value="true">швидкість і точність визначення місця порушення;<br>
                    <input type="radio" name="t14" value="true">висока чутливість до дій зловмисника;<br>
                    <input type="radio" name="t14" value="true">можливість централізованого контролю подій;<br>
                    <input type="radio" name="t14" value="false">охоплення контрольованої зони по всьому периметру.
                    <li>Що являє собою датчик (сповіщувач)?
                    </li>
                    <input type="radio" name="t15" value="false">пристрій, що формує магнітний сигнал тривоги при впливі на датчик або на створюване їм поле зовнішніх сил або об’єктів;<br>
                    <input type="radio" name="t15" value="true">пристрій, що формує магнітний сигнал тривоги при впливі на датчик;<br>
                    <input type="radio" name="t15" value="true">пристрій, що формує електричний сигнал тривоги при впливі на датчик або на створюване їм поле зовнішніх сил або об’єктів;<br>
                    <input type="radio" name="t15" value="false">пристрій, що формує електричний сигнал тривоги при впливі на датчик.
                    <li>Шлейф сигналізації утворює:
                    </li>
                    <input type="radio" name="t16" value="false">електричний ланцюг для передачі сигналу тривоги від контрольного пристрою  до датчика;<br>
                    <input type="radio" name="t16" value="true">електричний ланцюг для передачі сигналу тривоги від датчика до приймально-контрольного пристрою;<br>
                    <input type="radio" name="t16" value="true">магнітний ланцюг для передачі сигналу тривоги від контрольного пристрою  до датчика;<br>
                    <input type="radio" name="t16" value="false">магнітний ланцюг для передачі сигналу тривоги від датчика до приймально-контрольного пристрою;
                    <li>Яку функцію виконує оповіщувач?
                    </li>
                    <input type="radio" name="t17" value="false">видає світлові і звукові сигнали черговому охоронцю;<br>
                    <input type="radio" name="t17" value="true">видає світлові сигнали черговому охоронцю;<br>
                    <input type="radio" name="t17" value="false">видає звукові сигнали черговому охоронцю;
                    <li>За принципом виявлення зловмисників датчики діляться на:
                    </li>
                    <input type="radio" name="t18" value="false">мікрохвильові;<br>
                    <input type="radio" name="t18" value="true">контактні;<br>
                    <input type="radio" name="t18" value="true">вібраційні;<br>
                    <input type="radio" name="t18" value="true">акустичні;<br>
                    <input type="radio" name="t18" value="true">ємнісні;<br>
                    <input type="radio" name="t18" value="true">оптико-електронні;<br>
                    <input type="radio" name="t18" value="false">телевізійні.
                    <li>Контактні датчики бувають:
                    </li>
                    <input type="radio" name="19" value="false">електронні;<br>
                    <input type="radio" name="19" value="true">електроконтактні;<br>
                    <input type="radio" name="19" value="true">магнітні;<br>
                    <input type="radio" name="19" value="true">магнітноконтактні;<br>
                    <input type="radio" name="19" value="true">ударні;<br>
                    <input type="radio" name="19" value="false">ударноконтактні.
                    <li>Магнітноконтактні датчики використовуються для:
                    </li>
                    <input type="radio" name="t20" value="false">охорони великих предметів;<br>
                    <input type="radio" name="t20" value="true">охорони переносимих предметів;<br>
                    <input type="radio" name="t20" value="true">охорони складних конструкцій;<br>
                    <input type="radio" name="t20" value="false">охорони горища приміщення.
                    <li>Які є типи акустичних датчиків?
                    </li>
                    <input type="radio" name="t21" value="false">стійкі та нестійкі;<br>
                    <input type="radio" name="t21" value="true">сильні та слабкі;<br>
                    <input type="radio" name="t21" value="true">швидкі та повільні;<br>
                    <input type="radio" name="t21" value="false">активні та пасивні.
                    <li>Які є типи оптико-електронних датчиків?
                    </li>
                    <input type="radio" name="t22" value="false">стійкі та нестійкі;<br>
                    <input type="radio" name="t22" value="true">сильні та слабкі;<br>
                    <input type="radio" name="t22" value="true">активні та пасивні.<br>
                    <input type="radio" name="t22" value="false">швидкі та повільні;
                    <li>Мікрохвильові (радіохвильові) датчики бувають:
                    </li>
                    <input type="radio" name="t23" value="false">радіоструменеві;<br>
                    <input type="radio" name="t23" value="true">радіопроменеві;<br>
                    <input type="radio" name="t23" value="true">радіотехнічні;<br>
                    <input type="radio" name="t23" value="false">радіооптичні.
                    <li>Телевізійна система відео контролю забезпечує:
                    </li>
                    <input type="radio" name="t24" value="false">відеозапис дій зловмисника;<br>
                    <input type="radio" name="t24" value="true">автоматизоване відеоспостереження за рубежами захисту;<br>
                    <input type="radio" name="t24" value="true">контроль за діями персоналу організації;<br>
                    <input type="radio" name="t24" value="false">режим відеоохорони.
                    <li>У загальному випадку телевізійна система відеоконтролю включає такі пристрої:
                    </li>
                    <input type="radio" name="t25" value="false">монітори;<br>
                    <input type="radio" name="t25" value="true">пристрої реєстрації інформації;<br>
                    <input type="radio" name="t25" value="true">передавальні телевізійні камери;<br>
                    <input type="radio" name="t25" value="true">чіпи відеозв’язку;<br>
                    <input type="radio" name="t25" value="false">пристрої обробки і комутації відеоінформації.
                </ol>
                <input type="submit" class="btn3 buttton btn btn-lg btn-primary" name="enter" value="Завершити" />
                <?php $test= 'test7';?>
            </form>
        </div>
        <div id="foot">Copyright 2015, Nakslearn, Inc. by <a href="http://lnkd.in/dgaEqmB">Roman Semenyuk</a>.</div>
    </div>
</div>
</div>
</div>
</body>
</html>