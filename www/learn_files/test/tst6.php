<?php
if ($_SESSION['email']) {
    echo "connection<script>document.location.replace('/code/php_file_connection/login.html');</script>";
    exit;
}
if (isset($_REQUEST[session_name()])) session_start();
include_once('/topMenu.ini.php');
?>
<!DOCTYPE html>
<html lang="ua">
<body>
<script src="/styles/js/cards.js"></script>
<div class="">
    <script type="text/javascript" src="/styles/js/jquery.js"></script>
    <style type="text/css">
        @import url(/styles/css/css.css);
    </style>
    <script type="text/javascript" src="/styles/js/loader.js"></script>
    <!--<div class="cards-wrapper">-->
    <div id="wrapper">
        <ul id = "nav">
            <li><a href="/learn_files/lecture/z1.php"><span class="glyphicon glyphicon-file"></span></a></li>

            <li><a href="/learn_files/test/tst1.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z2.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst2.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z3.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst3.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z4.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst4.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z5.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst5.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z6.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst6.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z7.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst7.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z8.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst8.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z9.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst9.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z10.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst10.php"><span class="glyphicon glyphicon-check"></a></li>
        </ul>
        <div id="content">
            <form method="post" action="/code/php_file/count/count6.php" class="form-signin" role="form">
            <h3>Тести на тему: «Криптографічні методи захисту інформації.»</h3>
            <ol>
                <li>Чим займається криптографія?
                </li>
                <input type="radio" name="t1" value="false"> пошуком методів перетворення інформації з метою приховання її змісту;<br>
                <input type="radio" name="t1" value="true"> пошуком і дослідженням методів перетворення інформації з метою приховання її змісту;<br>
                <input type="radio" name="t1" value="false">дослідженням методів перетворення інформації з метою приховання її змісту;
                <li>Криптологія це –
                </li>
                <input type="radio" name="t2" value="true"> наука про захист інформації, шляхом її перетворення;<br>
                <input type="radio" name="t2" value="false"> передача конфіденційної інформації;<br>
                <input type="radio" name="t2" value="false"> дослідження можливості розшифровування інформації без знання ключів;<br>
                <input type="radio" name="t2" value="false">упорядкований набір з елементів алфавіту.
                <li>Криптоаналіз це –
                </li>
                <input type="radio" name="t3" value="true"> дослідження можливості розшифровування інформації без знання ключів;<br>
                <input type="radio" name="t3" value="false"> передача конфіденційної інформації;<br>
                <input type="radio" name="t3" value="false"> упорядкований набір з елементів алфавіту;<br>
                <input type="radio" name="t3" value="false">наука про захист інформації, шляхом її перетворення;
                <li>Назвіть методи криптографічного перетворення інформації:
                </li>
                <input type="checkbox" name="t4" value="false"> алфавіт;<br>
                <input type="checkbox" name="t4" value="true"> стенографія;<br>
                <input type="checkbox" name="t4" value="true"> шифрування;<br>
                <input type="checkbox" name="t4" value="true">кодування; <br>
                <input type="checkbox" name="t4" value="true"> стиснення; <br>
                <input type="checkbox" name="t4" value="false">спрощення.
                <li>В основі всіх методів стенографії лежить маскування закритої інформації.
                </li>
                <input type="radio" name="t5" value="true"> Так<br>
                <input type="radio" name="t5" value="false"> Ні
                <li>Метод стенографії дозволяє приховати
                </li>
                <input type="radio" name="t6" value="true"> сенс збереженої або переданої інформації;<br>
                <input type="radio" name="t6" value="true"> факт зберігання або передачі закритої інформації;<br>
                <input type="radio" name="t6" value="false">алгоритм перетворення і ключ.
                <li>При кодуванні і зворотному перетворенні використовуються спеціальні таблиці або словники.</li>
                <input type="radio" name="t7" value="true"> Так<br>
                <input type="radio" name="t7" value="false"> Ні
                <li>Метою стиснення інформації є:
                </li>
                <input type="radio" name="t8" value="true"> скорочення обсягу інформації;<br>
                <input type="radio" name="t8" value="false"> кодування інформації;<br>
                <input type="radio" name="t8" value="false">шифрування інформації.
                <li>Стисла інформація може бути прочитана чи використана без зворотного перетворення.</li>
                <input type="radio" name="t9" value="true"> Так<br>
                <input type="radio" name="t9" value="false"> Ні
                <li>Алфавіт це –
                </li>
                <input type="radio" name="t10" value="true"> кінцева множина використовуваних для кодування інформації знаків;<br>
                <input type="radio" name="t10" value="false"> упорядкований набір з елементів алфавіту;<br>
                <input type="radio" name="t10" value="false">процес перетворення вихідного тексту у шифрований текст.
                <li>Простір ключів ДО це –
                </li>
                <input type="radio" name="t11" value="true"> набір можливих значень ключа;<br>
                <input type="radio" name="t11" value="false"> упорядкований набір з елементів алфавіту;<br>
                <input type="radio" name="t11" value="false"> процес перетворення вихідного тексту у шифрований текст.
                <li>Криптосистеми підрозділяються на симетричні й асиметричні.</li>
                <input type="radio" name="t12" value="true"> Так<br>
                <input type="radio" name="t12" value="false"> Ні
                <li>Скільки ключів використовується у системах з відкритим ключем?
                </li>
                <input type="radio" name="t13" value="true"> 2;<br>
                <input type="radio" name="t13" value="false"> 3;<br>
                <input type="radio" name="t13" value="false"> 4;<br>
                <input type="radio" name="t13" value="false">1.
                <li>Терміни «розподіл ключів» і «керування ключами» ставляться до процесів системи обробки інформації, змістом яких є знищення ключів.</li>
                <input type="radio" name="t14" value="true"> Ні<br>
                <input type="radio" name="t14" value="false"> Так
                <li>За допомогою якого ключа шифрується інформація?
                </li>
                <input type="radio" name="t15" value="true"> відкритого;<br>
                <input type="radio" name="t15" value="false"> закритого.
                <li>За допомогою якого ключа розшифровується інформація?</li>
                <input type="radio" name="t16" value="false"> відкритого;<br>
                <input type="radio" name="t16" value="true"> закритого.
                <li>Змістом процесу кодування інформації є заміна смислових конструкцій вихідної інформації кодами.</li>
                <input type="radio" name="t17" value="true"> Так<br>
                <input type="radio" name="t17" value="false"> Ні
                <li>Основним видом криптографічного перетворення інформації в КС є:
                </li>
                <input type="radio" name="t18" value="true"> шифрування;<br>
                <input type="radio" name="t18" value="false"> кодування;<br>
                <input type="radio" name="t18" value="false">стиснення.
                <li>Сучасні методи шифрування повинні відповідати наступним вимогам:
                </li>
                <input type="radio" name="t19" value="true"> шифртекст не повинен істотно перевершувати за обємом вихідну інформацію;<br>
                <input type="radio" name="t19" value="true"> час шифрування не повинен бути великим;<br>
                <input type="radio" name="t19" value="false"> час шифрування  повинен бути великим;<br>
                <input type="radio" name="t19" value="true">вартість шифрування повинна бути узгоджена з вартістю закритої інформації.
                <li>Крипостійкість шифру не є його основним показником ефективності.</li>
                <input type="radio" name="t20" value="true"> Так<br>
                <input type="radio" name="t20" value="false"> Ні
                <li>Час і кошти, що витрачаються на криптоаналіз, залежать від:
                </li>
                <input type="radio" name="t21" value="true"> довжини ключа;<br>
                <input type="radio" name="t21" value="false"> розміру ключа;<br>
                <input type="radio" name="t21" value="true">складності алгоритму шифрування.
            </ol>
                <input type="submit" class="btn3 buttton btn btn-lg btn-primary" name="enter" value="Завершити" />
                <?php $test= 'test6';?>
            </form>
        </div>
        <div id="foot">Copyright 2015, Nakslearn, Inc. by <a href="http://lnkd.in/dgaEqmB">Roman Semenyuk</a>.</div>
    </div>
</div>
</div>
</div>
</body>
</html>