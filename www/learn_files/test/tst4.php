<?php
if ($_SESSION['email']) {
    echo "connection<script>document.location.replace('/code/php_file_connection/login.html');</script>";
    exit;
}
if (isset($_REQUEST[session_name()])) session_start();
include_once('/topMenu.ini.php');
?>
<!DOCTYPE html>
<html lang="ua">
<body>
<script src="/styles/js/cards.js"></script>
<div class="">
    <script type="text/javascript" src="/styles/js/jquery.js"></script>
    <style type="text/css">
        @import url(/styles/css/css.css);
    </style>
    <script type="text/javascript" src="/styles/js/loader.js"></script>
    <!--<div class="cards-wrapper">-->
    <div id="wrapper">
        <ul id = "nav">
            <li><a href="/learn_files/lecture/z1.php"><span class="glyphicon glyphicon-file"></span></a></li>

            <li><a href="/learn_files/test/tst1.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z2.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst2.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z3.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst3.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z4.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst4.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z5.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst5.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z6.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst6.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z7.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst7.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z8.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst8.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z9.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst9.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z10.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst10.php"><span class="glyphicon glyphicon-check"></a></li>
        </ul>
        <div id="content">
            <form method="post" action="/code/php_file/count/count4.php" class="form-signin" role="form">
                <ol>
                    <li>Науку про захист інформації, шляхом її перетворення називають:</li>
                    <input type="radio" name="t1" value="false">Криптографія <br>
                    <input type="radio" name="t1" value="true">Криптологія<br>
                    <input type="radio" name="t1" value="false">Криптосистема
                    <li>Криптологія поєднує такі напрямки</li>
                    <input type="checkbox" name="t2" value="true">Криптографія<br>
                    <input type="checkbox" name="t2" value="false">Криптосистема<br>
                    <input type="checkbox" name="t2" value="true">Криптоаналіз
                    <li>Криптографія займається:</li>
                    <input type="radio" name="t3" value="true">дослідженням можливості розшифрування інформації без
                    знання
                    ключів;<br>
                    <input type="radio" name="t3" value="false">зниженням інформаційних сигналів ПЕМВН;<br>
                    <input type="radio" name="t3" value="true">пошуком і дослідженням методів перетворення інформації з
                    метою
                    приховання її змісту. <br>
                    <li>Основні напрямки використання криптографічних методів:</li>
                    <input type="checkbox" name="t4" value="true">Передача конфіденційної інформації з каналів
                    зв’язку;<br>
                    <input type="checkbox" name="t4" value="true">установлення дійсності переданих повідомлень;<br>
                    <input type="checkbox" name="t4" value="true">зберігання інформації на носіях у зашифрованому
                    виді.<br>
                    <li>Дослідженням можливості розшифрування інформації без знання ключів займається…</li>
                    <input type="radio" name="t5" value="false">Криптографія<br>
                    <input type="radio" name="t5" value="false">Шифрування<br>
                    <input type="radio" name="t5" value="true">Криптоаналіз
                    <li>Упорядкований набір з елементів алфавіту це</li>
                    <input type="radio" name="t6" value="true">Текст<br>
                    <input type="radio" name="t6" value="false"> Ключ<br>
                    <input type="radio" name="t6" value="false"> Шифр
                    <li>Під словом «Шифрування» розуміють:</li>
                    <input type="radio" name="t7" value="true">Процес перетворення вихідного тексту, у шифрований
                    текст<br>
                    <input type="radio" name="t7" value="false">Процес перетворення вхідного тексту, у шифрований
                    текст<br>
                    <input type="radio" name="t7" value="false">Процес перетворення закритого тексту, у шифрований текст
                    <li>8. Послідовний ряд символів алфавіту звичайно являє собою…</li>
                    <input type="radio" name="t8" value="true">Текст<br>
                    <input type="radio" name="t8" value="false">Ключ<br>
                    <input type="radio" name="t8" value="false">Цифри<br>
                    <li>Криптосистеми поділяються на:
                    </li>
                    <input type="checkbox" name="t9" value="true">симетричні <br>
                    <input type="checkbox" name="t9" value="true">з відкритим ключем<br>
                    <input type="checkbox" name="t9" value="false">секретні
                    <li>Інформація шифрується з допомогою …………ключа, а розшифровується за допомогою ……………ключа.
                    </li>
                    <input type="radio" name="t10" value="true">відкритого, закритого;<br>
                    <input type="radio" name="t10" value="false">закритого, відкритого;<br>
                    <input type="radio" name="t10" value="false">секретного, секретного.
                    <li>Методи криптографічного перетворення інформації:
                    </li>
                    <input type="checkbox" name="t11" value="true"> шифрування;<br>
                    <input type="checkbox" name="t11" value="true"> стеганографія;<br>
                    <input type="checkbox" name="t11" value="true"> кодування;<br>
                    <input type="checkbox" name="t11" value="true">стиснення.
                    <li>Цей процес полягає в проведенні оборотних математичних, логічних, комбінаторних та інших
                        перетворень
                        вихідної інформації, в результаті яких зашифрована інформація являє собою хаотичний набір букв,
                        цифр, інших символів і двійкових кодів.
                    </li>
                    <input type="radio" name="t12" value="true"> шифрування;<br>
                    <input type="radio" name="t12" value="false"> кодування;<br>
                    <input type="radio" name="t12" value="false"> стиснення.
                    <li>В основі всіх методів стенографії лежить…
                    </li>
                    <input type="radio" name="t13" value="false">маскування відкритих файлів серед закритої
                    інформації;<br>
                    <input type="radio" name="t13" value="true">маскування закритої інформації серед відкритих файлів.
                    <li>За допомогою засобів стенографії можуть маскуватися:
                    </li>
                    <input type="checkbox" name="t14" value="true"> текст; <br>
                    <input type="checkbox" name="t14" value="true"> зображення, мова; <br>
                    <input type="checkbox" name="t14" value="true"> цифровий підпис; <br>
                    <input type="checkbox" name="t14" value="true">зашифроване повідомлення.
                    <li>Заміна смислових конструкцій вихідної інформації (слів, речень) кодами, є змістом процесу…
                    </li>
                    <input type="radio" name="t15" value="true"> кодування; <br>
                    <input type="radio" name="t15" value="false"> стиснення; <br>
                    <input type="radio" name="t15" value="false"> шифрування. <br>
                    <li>Метою стиснення інформації є…
                    </li>
                    <input type="radio" name="t16" value="false"> збільшення обсягу інформації; <br>
                    <input type="radio" name="t16" value="false"> зменшення кількості тексту; <br>
                    <input type="radio" name="t16" value="true"> скорочення обсягу інформації.
                    <li>Чи піддаються стислі файли конфіденційної інформації шифруванню?
                    </li>
                    <input type="radio" name="t17" value="true"> Так; <br>
                    <input type="radio" name="t17" value="false"> Ні; <br>
                    <input type="radio" name="t17" value="false"> лише в деяких випадках.
                    <li>Основним видом криптографічного перетворення інформації в КС є…
                    </li>

                    <input type="radio" name="t18" value="false"> кодування; <br>
                    <input type="radio" name="t18" value="true"> шифрування; <br>
                    <input type="radio" name="t18" value="false">стиснення.
                    <li>Процес перетворення закритої інформації у відкриту називається…
                    </li>
                    <input type="radio" name="t19" value="false"> шифрування; <br>
                    <input type="radio" name="t19" value="true"> розшифрування; <br>
                    <input type="radio" name="t19" value="false">зашифрування.
                    <li>Процес перетворення відкритої інформації в закриту отримав назву…
                    </li>
                    <input type="radio" name="t20" value="false"> шифрування; <br>
                    <input type="radio" name="t20" value="false"> розшифрування; <br>
                    <input type="radio" name="t20" value="true">зашифрування.
                    <li>Атака на шифр (криптоаналіз) - це процес …
                    </li>
                    <input type="checkbox" name="t21" value="true"> розшифрування закритої інформації без ключа; <br>
                    <input type="checkbox" name="t21" value="false"> атака вірусів на шифр; <br>
                    <input type="checkbox" name="t21" value="true">розшифрування інформації за відсутності відомостей
                    про алгоритм шифрування.
                    <li>Часом, або вартістю засобів, необхідних криптоаналітикам для отримання вихідної інформації по
                        шифртексту вимірюється…
                    </li>
                    <input type="radio" name="t22" value="false"> показник ефективності; <br>
                    <input type="radio" name="t22" value="true"> криптостійкість шифру; <br>
                    <input type="radio" name="t22" value="false">метод шифрування.
                    <li>
                        Шифр DES застосовуваний у …з 19** року в якості державного стандарту.
                    </li>
                    <input type="radio" name="t23" value="false"> у Швейцарії з 1978; <br>
                    <input type="radio" name="t23" value="false"> у Швеції з 1987; <br>
                    <input type="radio" name="t23" value="true">у США з 1978.
                    <li>
                        Алгоритм DES не є секретним і був опублікований у…
                    </li>
                    <input type="radio" name="t24" value="true"> відкритій пресі; <br>
                    <input type="radio" name="t24" value="false"> приватних сайтах; <br>
                    <input type="radio" name="t24" value="false">різних ресурсах.
                </ol>
                <input type="submit" class="btn3 buttton btn btn-lg btn-primary" name="enter" value="Завершити"/>
                <?php $test = 'test4' ?>
            </form>
        </div>
        <div id="foot">Copyright 2015, Nakslearn, Inc. by <a href="http://lnkd.in/dgaEqmB">Roman Semenyuk</a>.</div>
    </div>
</div>
</div>
</div>
</body>
</html>