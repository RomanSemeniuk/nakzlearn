<?php
if ($_SESSION['email']) {
    echo "connection<script>document.location.replace('/code/php_file_connection/login.html');</script>";
    exit;
}
if (isset($_REQUEST[session_name()])) session_start();
include_once('/topMenu.ini.php');
?>
<!DOCTYPE html>
<html lang="ua">
<body>
<script src="/styles/js/cards.js"></script>
<div class="">
    <script type="text/javascript" src="/styles/js/jquery.js"></script>
    <style type="text/css">
        @import url(/styles/css/css.css);
    </style>
    <script type="text/javascript" src="/styles/js/loader.js"></script>
    <!--<div class="cards-wrapper">-->
    <div id="wrapper">
        <ul id = "nav">
            <li><a href="/learn_files/lecture/z1.php"><span class="glyphicon glyphicon-file"></span></a></li>

            <li><a href="/learn_files/test/tst1.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z2.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst2.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z3.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst3.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z4.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst4.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z5.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst5.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z6.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst6.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z7.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst7.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z8.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst8.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z9.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst9.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z10.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst10.php"><span class="glyphicon glyphicon-check"></a></li>
        </ul>
        <div id="content">
            <form method="post" action="/code/php_file/count/count2.php" class="form-signin" role="form">
                <h4>Тестові завдання до теми «Загрози безпеки інформації в комп’ютерних системах.»</h4>
                <ol>
                    <li>Загрози безпеки інформації в КС поділяють на такі два класи:
                    </li>
                    <input type="radio" name="t1" value="true"> Випадкові та навмисні;<br>
                    <input type="radio" name="t1" value="false"> Випадкові та спеціальні;<br>
                    <input type="radio" name="t1" value="false"> Навмисні та конкретні;<br>
                    <input type="radio" name="t1" value="false"> Шкідливі та нешкідливі;<br>
                    <input type="radio" name="t1" value="false">Інша відповідь.
                    <li>Метою створення будь-якої КС є:
                    </li>
                    <input type="radio" name="t2" value="true"> задоволення потреб користувачів в своєчасному здобутті
                    достовірної інформації і збереженні її конфіденційності.<br>
                    <input type="radio" name="t2" value="false"> задоволення потреб користувачів у попередньо наданому
                    запиті.<br>
                    <input type="radio" name="t2" value="false"> задоволення потреб будь-яких осіб, у наданні конкретної
                    інформації.<br>
                    <input type="radio" name="t1" value="false"> приховування будь-якої інформації від усіх осіб, не
                    включаючи власника системи.<br>
                    <input type="radio" name="t2" value="false">Інша відповідь.
                    <li>З позиції забезпечення безпеки інформації в КС, такі системи доцільно розглядати у вигляді єдності трьох компонент:
                    </li>
                    <input type="radio" name="t3" value="true"> Інформація;<br>
                    <input type="radio" name="t3" value="false"> Власник КС;<br>
                    <input type="radio" name="t3" value="true"> Обслуговуючий персонал і користувачі;<br>
                    <input type="radio" name="t3" value="false">Захисні програмні засоби;<br>
                    <input type="radio" name="t3" value="false">Зашифровані дані;<br>
                    <input type="radio" name="t3" value="true">Технічні і програмні засоби.
                    <li>До випадкових загроз відносять:
                    </li>
                    <input type="radio" name="t4" value="true"> Стихійні лиха та аварії;<br>
                    <input type="radio" name="t4" value="false"> Шкідливі програми;<br>
                    <input type="radio" name="t4" value="true"> Алгоритмічні помилки;<br>
                    <input type="radio" name="t4" value="true"> Помилки при розробці КС;<br>
                    <input type="radio" name="t4" value="false"> Шпигунство;<br>
                    <input type="radio" name="t4" value="false">Несанкціонована модифікація структур.
                    <li>«Кінцевий продукт» вжитку в КС та центральний компонент системи – це …
                    </li>
                    <input type="radio" name="t5" value="false"> Власник КС;<br>
                    <input type="radio" name="t5" value="false"> Користувач;<br>
                    <input type="radio" name="t5" value="true"> Інформація;<br>
                    <input type="radio" name="t5" value="false"> Зашифровані дані;<br>
                    <input type="radio" name="t5" value="false">Інша відповідь
                    <li>Під загрозою безпеки інформації розуміється …
                    </li>
                    <input type="radio" name="t6" value="false"> сукупність дій, які шкодять системі.<br>
                    <input type="radio" name="t6" value="false"> певна послідовність запланованих дій, які ведуть до знешкодження або до знищення системи.<br>
                    <input type="radio" name="t6" value="true"> потенційно можлива подія, процес або явище, які можуть привести до знищення, втрати цілісності, конфіденційності або доступності інформації.<br>
                    <input type="radio" name="t6" value="false"> сукупність подій, які навмисно або ж ненавмисно призводять до певних неполадок у системі.<br>
                    <input type="radio" name="t6" value="false">Інша відповідь.
                    <li>Випадковими називають загрози, …
                    </li>
                    <input type="radio" name="t7" value="false"> які стаються незалежно від будь-яких дій користувача або власника;<br>
                    <input type="radio" name="t7" value="false"> причиною яких є недбалість зі сторони власника КС;<br>
                    <input type="radio" name="t7" value="false"> які незалежно від результату, мали за початкову мету зашкодити системі та її власнику;<br>
                    <input type="radio" name="t7" value="true"> які не пов’язані з навмисними діями зловмисників і реалізуються у випадкові моменти часу.<br>
                    <input type="radio" name="t7" value="false">Інша відповідь.
                    <li>Реалізація загроз якого класу приводить до найбільших втрат?
                    </li>
                    <input type="radio" name="t8" value="false"> Конкретних.<br>
                    <input type="radio" name="t8" value="false"> Штучних.<br>
                    <input type="radio" name="t8" value="false"> Навмисних.<br>
                    <input type="radio" name="t8" value="true"> Випадкових.<br>
                    <input type="radio" name="t8" value="false">Інша відповідь.
                    <li>Найбільш руйнівні наслідки для КС несуть:
                    </li>
                    <input type="radio" name="t9" value="false"> Шпигунські програми.<br>
                    <input type="radio" name="t9" value="true"> Стихійні лиха та аварії.<br>
                    <input type="radio" name="t9" value="false"> Шкідливі програми.<br>
                    <input type="radio" name="t9" value="false"> Помилки при розробці КС.<br>
                    <input type="radio" name="t9" value="false">Інша відповідь.
                    <li>Клас загроз, який вивчений недостатньо, дуже динамічний та постійно поповнюється новими загрозами – це ….
                    </li>
                    <input type="radio" name="t10" value="false"> Конкретні.<br>
                    <input type="radio" name="t10" value="false"> Штучні.<br>
                    <input type="radio" name="t10" value="true"> Навмисні.<br>
                    <input type="radio" name="t10" value="false"> Випадкові.<br>
                    <input type="radio" name="t10" value="false">Інша відповідь.
                    <li>Методи, які використовуються для здобуття відомостей про систему захисту з метою проникнення в КС, а також для розкрадання і знищення інформаційних ресурсів:
                    </li>
                    <input type="radio" name="t11" value="true"> Традиційне шпигунство та диверсії.<br>
                    <input type="radio" name="t11" value="false"> Несанкціонований доступ до інформації.<br>
                    <input type="radio" name="t11" value="false"> Помилки при  розробці КС.<br>
                    <input type="radio" name="t11" value="false">Модифікація структур.<br>
                    <input type="radio" name="t11" value="false">Інша відповідь.
                    <li>До методів шпигунства та диверсій відносяться:
                    </li>
                    <input type="radio" name="t12" value="true"> Підслуховування.<br>
                    <input type="radio" name="t12" value="true"> Підпали.<br>
                    <input type="radio" name="t12" value="false"> Спостереження при візуальному контакті.<br>
                    <input type="radio" name="t12" value="false"> Категоричні відмови.<br>
                    <input type="radio" name="t12" value="true"> Підкуп і шантаж співробітників.<br>
                    <input type="radio" name="t12" value="false">Відмова надання інформації.
                    <li>Залежно від чого скорочується дальність дії пристрою підслуховування у міських умовах?
                    </li>
                    <input type="radio" name="t13" value="false"> Від довжини кабеля.<br>
                    <input type="radio" name="t13" value="true"> Від фонового шуму.<br>
                    <input type="radio" name="t13" value="false"> Від скупчення людей.<br>
                    <input type="radio" name="t13" value="false"> Від відбитих лазерних променів.<br>
                    <input type="radio" name="t13" value="false">Від коливання шибок.
                    <li>Підслуховування може здійснюватися за допомогою:
                    </li>
                    <input type="radio" name="t14" value="true"> Стетоскопічних мікрофонів.<br>
                    <input type="radio" name="t14" value="true"> Телефонної лінії.<br>
                    <input type="radio" name="t14" value="false"> Віконної шибки.<br>
                    <input type="radio" name="t14" value="false"> Супутникової антени.<br>
                    <input type="radio" name="t14" value="false">Кабелів зв’язку
                    <li>Термін «несанкціонований доступ до інформації» визначений як:
                    </li>
                    <input type="radio" name="t15" value="false"> Доступ, що порушує правила системи;<br>
                    <input type="radio" name="t15" value="false"> Протиправні дії, що стосуються власника та користувача системи;<br>
                    <input type="radio" name="t15" value="true"> Доступ до інформації, що порушує правила розмежування доступу з використанням штатних засобів обчислювальної техніки або автоматизованих систем;<br>
                    <input type="radio" name="t15" value="false"> Доступ до інформації, що порушує цілісність засобів обчислювальної техніки або автоматизованих систем;<br>
                    <input type="radio" name="t15" value="false">Доступ до інформації, що порушує правила доступу до штатних засобів обчислювальної техніки або автоматизованих систем.
                    <li>Право доступу до ресурсів КС визначається:
                    </li>
                    <input type="radio" name="t16" value="false"> Власником КС;<br>
                    <input type="radio" name="t16" value="false"> Користувачем КС;<br>
                    <input type="radio" name="t16" value="true"> Керівництвом для кожного співробітника;<br>
                    <input type="radio" name="t16" value="false"> Самою системою;<br>
                    <input type="radio" name="t16" value="false">Керівництвом для всіх співробітників разом.
                    <li>Виконання встановлених правил розмежування доступу в КС реалізується за рахунок створення:
                    </li>
                    <input type="radio" name="t17" value="false"> Спільного реєстру.<br>
                    <input type="radio" name="t17" value="false"> Індивідуального доступу.<br>
                    <input type="radio" name="t17" value="true"> Системи розмежування доступу.<br>
                    <input type="radio" name="t17" value="false"> Загального доступу.<br>
                    <input type="radio" name="t17" value="false">Індивідуальних паролів.
                    <li>Електромагнітні випромінювання використовуються зловмисниками для:
                    </li>
                    <input type="radio" name="t18" value="false"> Переписування інформації.<br>
                    <input type="radio" name="t18" value="true"> Знищення інформації.<br>
                    <input type="radio" name="t18" value="false"> Відокремлення інформації.<br>
                    <input type="radio" name="t18" value="true"> Здобуття інформації.<br>
                    <input type="radio" name="t18" value="false">Шифрування інформації.
                    <li>Залежно від механізму дії шкідливі програми діляться на класи:
                    </li>
                    <input type="radio" name="t19" value="true"> «Логічні бомби»<br>
                    <input type="radio" name="t19" value="false"> «Шпигуни»<br>
                    <input type="radio" name="t19" value="false"> «Комп’ютерні віруси»<br>
                    <input type="radio" name="t19" value="true"> «Троянські коні»<br>
                    <input type="radio" name="t19" value="false">«Вбудовані макроси»<br>
                    <input type="radio" name="t19" value="true"> «Черв’яки»
                    <li>Зловмисником може бути:
                    </li>
                    <input type="radio" name="t20" value="true"> Розробник КС.<br>
                    <input type="radio" name="t20" value="false"> Власник КС.<br>
                    <input type="radio" name="t20" value="true"> Користувач.<br>
                    <input type="radio" name="t20" value="true"> Технічний персонал.<br>
                    <input type="radio" name="t20" value="true">Стороння особа.
                    <li>Закладні пристрої, які використовуються для  аудіо- та відео контролю за відсутності зловмисника у приміщенні мають назву:
                    </li>
                    <input type="radio" name="t21" value="false"> «Закладки»<br>
                    <input type="radio" name="t21" value="true"> «Жучки»<br>
                    <input type="radio" name="t21" value="false"> «Радіопередавачі»<br>
                    <input type="radio" name="t21" value="false"> «Шпигунські програми»<br>
                    <input type="radio" name="t21" value="false">«Дроти»
                </ol>
                <input type="submit" class="btn3 buttton btn btn-lg btn-primary" name="enter" value="Завершити" />
                <?php $test= 'test2';?>
            </form>
        </div>
        <div id="foot">Copyright 2015, Nakslearn, Inc. by <a href="http://lnkd.in/dgaEqmB">Roman Semenyuk</a>.</div>
    </div>
</div>
</div>
</div>
</body>
</html>