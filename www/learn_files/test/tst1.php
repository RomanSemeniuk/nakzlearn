<?php
if ($_SESSION['email']) {
    echo "connection<script>document.location.replace('/code/php_file_connection/login.html');</script>";
    exit;
}
if (isset($_REQUEST[session_name()])) session_start();
include_once('/topMenu.ini.php');
?>
<!DOCTYPE html>
<html lang="ua">
<body>
<script src="/styles/js/cards.js"></script>
<div class="">
    <script type="text/javascript" src="/styles/js/jquery.js"></script>
    <style type="text/css">
        @import url(/styles/css/css.css);
    </style>
    <script type="text/javascript" src="/styles/js/loader.js"></script>
    <!--<div class="cards-wrapper">-->
    <div id="wrapper">
        <ul id = "nav">
            <li><a href="/learn_files/lecture/z1.php"><span class="glyphicon glyphicon-file"></span></a></li>

            <li><a href="/learn_files/test/tst1.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z2.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst2.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z3.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst3.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z4.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst4.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z5.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst5.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z6.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst6.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z7.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst7.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z8.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst8.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z9.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst9.php"><span class="glyphicon glyphicon-check"></a></li>

            <li><a href="/learn_files/lecture/z10.php"><span class="glyphicon glyphicon-file"></a></li>

            <li><a href="/learn_files/test/tst10.php"><span class="glyphicon glyphicon-check"></a></li>
        </ul>
        <div id="content">
            <form method="post" action="/code/php_file/count/count.php" class="form-signin" role="form">
            <ol>
                <li>Комутатори дозволяють підключитидо одного монітора:</li>
                <input type="radio" name="t1" value="true"> 4-16 телекамер <br>
                <input type="radio" name="t1" value="false"> 4-16 телеканалів <br>
                <input type="radio" name="t1" value="false"> 4-16 телескопів <br>
                <input type="radio" name="t1" value="false"> 4-16 комп’ютерів
                <li> Квадратори забезпечують одночасну видачу зображення на одному моніторі від кількох:</li>
                <input type="radio" name="t2" value="false"> моніторів <br>
                <input type="radio" name="t2" value="true"> камер <br>
                <input type="radio" name="t2" value="false"> LAN підключень <br>
                <input type="radio" name="t2" value="false"> браузерів
                <li>Детектор руху оповіщає про …… в зоні контролю камери, підключає камеру для запису
                    відеоінформації:
                </li>
                <input type="radio" name="t3" value="true"> про рух <br>
                <input type="radio" name="t3" value="false"> про температуру <br>
                <input type="radio" name="t3" value="false"> про вологість камери <br>
                <input type="radio" name="t3" value="false"> освітленість поміщення
                <li>Відеомагнітофон відноситься до пристроїв:</li>
                <input type="radio" name="t4" value="true"> реєстрації відеоінформації <br>
                <input type="radio" name="t4" value="false"> відображення інформації <br>
                <input type="radio" name="t4" value="false"> видалення інформації <br>
                <input type="radio" name="t4" value="false"> монтування інформації
                <li>Для фіксації окремих кадрів на папері використовується другий ПРІ:</li>
                <input type="radio" name="t5" value="true"> відеопринтер <br>
                <input type="radio" name="t5" value="false"> аудіо принтер <br>
                <input type="radio" name="t5" value="false"> відеомагнітофон <br>
                <input type="radio" name="t5" value="false"> відеоприставка
                <li>Доступ на об’єкти проводиться на:</li>
                <input type="radio" name="t6" value="true"> контрольно-пропускних пунктах. <br>
                <input type="radio" name="t6" value="false"> самостійно-пропускних пунктах. <br>
                <input type="radio" name="t6" value="false"> лабораторно-пропускних пунктах. <br>
                <input type="radio" name="t6" value="false"> практично-пропускних пунктах.
                <li>Виберіть правильні відповіді « Суб’єктами доступу є:»</li>
                <input type="checkbox" name="t7" value="true"> ідентифікація <br>
                <input type="checkbox" name="t7" value="false"> аутенфікація <br>
                <input type="checkbox" name="t7" value="false"> Автентифікація <br>
                <input type="checkbox" name="t7" value="false"> Афтефікація
                <li>Виберіть лишню відповідь «Залежно від фізичних принципів запису, зберігання та зчитування ідентифікаційної інформації карти діляться на:»</li>
                <input type="radio" name="t8" value="false"> Магнітні <br>
                <input type="radio" name="t8" value="false"> Карти оптичної пам’яті <br>
                <input type="radio" name="t8" value="true"> SIM-карти <br>
                <input type="radio" name="t8" value="false"> Напівпровидникові
                <li>Найменш захищеними картами від  фальсифікації є:</li>
                <input type="radio" name="t9" value="false"> Інфрачервоні <br>
                <input type="radio" name="t9" value="true"> магнітні <br>
                <input type="radio" name="t9" value="false"> напівпровідникові <br>
                <input type="radio" name="t9" value="false"> SIM-карти
                <li>Максимально захищеними картами від  фальсифікації є:</li>
                <input type="radio" name="t10" value="false"> Інфрачервоні <br>
                <input type="radio" name="t10" value="false"> магнітні <br>
                <input type="radio" name="t10" value="false"> напівпровідникові <br>
                <input type="radio" name="t10" value="true"> Сматр-карти
                <li>Системи ідентифікації які аналізують графічне накреслення, інтенсивність натискання і швидкість написання букв:</li>
                <input type="radio" name="t11" value="false"> за швидкістю <br>
                <input type="radio" name="t11" value="true"> за почерком <br>
                <input type="radio" name="t11" value="false"> за натисканням <br>
                <input type="radio" name="t11" value="false"> за ритмом
                <li>Ідентифікація що ґрунтується на вимірюванні часу між послідовним натисканням двох клавіш:</li>
                <input type="radio" name="t12" value="false"> по ритму роботи на мишці <br>
                <input type="radio" name="t12" value="false"> по ритму роботи на фортепіано <br>
                <input type="radio" name="t12" value="true"> по ритму роботи на клавіатурі <br>
                <input type="radio" name="t12" value="false"> по ритму роботи в Paint
                <li>Діскретизація мовної інформації з подальшим шифруванням забезпечує :</li>
                <input type="radio" name="t13" value="true"> найвищий ступінь захисту. <br>
                <input type="radio" name="t13" value="false"> найнижчий ступінь захисту. <br>
                <input type="radio" name="t13" value="false"> середній ступінь захисту <br>
                <input type="radio" name="t13" value="false"> умовний ступінь захисту
                <li>Звукопоглинання здійснюється шляхом перетворення кінетичної енергії звукової хвилі у :</li>
                <input type="radio" name="t14" value="false"> потенціальну енергію <br>
                <input type="radio" name="t14" value="true"> теплову енергію <br>
                <input type="radio" name="t14" value="false"> імпульс <br>
                <input type="radio" name="t14" value="false"> струм
            </ol>
            <input type="submit" class="btn3 buttton btn btn-lg btn-primary" name="enter" value="Завершити" />
                <?php $test= 'test1';?>
            </form>
        </div>
        <div id="foot">Copyright 2015, by <a href="http://lnkd.in/dgaEqmB">Roman Semenyuk</a>.</div>
</div>
</div>
</div>
</div>
</body>
</html>