<?php
/**
 * Created by PhpStorm.
 * User: Naks
 * Date: 25.09.2015
 * Time: 23:28
 */
if ($_SESSION['email']) {
    echo "adas<script>document.location.replace('login.php');</script>";
    /*header("Location: /login.php");*/
    exit;
}
if (isset($_REQUEST[session_name()])) session_start();
/*-=--=-=-=-=-=-==-=-==-*/
include_once('topMenu.ini.php');
?>
<!DOCTYPE html>
<html lang="en">
<body>
<script src="code/js/cards.js"></script>
<!-- Контент -->
<div class="content-wrapper">
    <script type="text/javascript" src="modules/jquery/jquery.js"></script>
    <style type="text/css">
        @import url(styles/css.css);
    </style>
    <script type="text/javascript" src="code/js/loader.js"></script>
    <!--<div class="cards-wrapper">-->
    <div id="wrapper">
        <h1>ajax ... nettuts</h1>
        <ul id="nav">
            <li><a href="learn1.php">welcome</a></li>
            <li><a href="learn2.php">about</a></li>
            <li><a href="content/temp/material/marthy/z5.html" width=900 height=400 class="ots">portfolio</a></li>
            <li><a href="contact.html">contact</a></li>
            <li><a href="terms.html">terms</a></li>
        </ul>
        <div id="content">
            <h2>Welcome!</h2>

            <p>Hi, welcome to the demonstration for the NETTUTS tutorial - "How to Load In and Animate Content with
                jQuery"</p>

            <p>In this tutorial we will be taking your average everyday website and enhancing it with jQuery. We will be
                adding ajax functionality so that the content loads into the relevant container instead of the user
                having to navigate to another page. We will also be integrating some awesome effects...</p>
        </div>
        <div id="foot">Tutorial by James for NETTUTS</div>
    </div>
</div>
</div>-->
<!-- Прогрес бар -->
<div class="side">
    <div class="prog">
        <progress min="0" value="5" max="190">1</progress>
    </div>
    <!--  -->
    <!-- Кнопки -->
    <div class="btn1">
        <button onclick="prev()" class="btn btn-primary">Попередній</button>
    </div>
    <div class="btn2">
        <button onclick="next()" class="btn btn-primary">Наступний</button>
    </div>
</div>

</div>
</body>
</html>