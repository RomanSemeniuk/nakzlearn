/**
 * Created by Naks on 10.09.2015.
 */
var step = 0;
var next = function () {
    var target = document.getElementsByClassName("cards-wrapper")[0];
    var count = target.getElementsByClassName("card").length;
    var progress = document.getElementsByTagName("progress")[0];
    progress.value += 10;
    if (Math.abs(step) < 1024 * (count - 1)) {
        step -= 1024;
        target.style.webkitTransform = "translate3d(" + step + "px,0,0)";
    }
    else {
        target.style.webkitTransform = "translate3d(0,0,0)";

    }
};

var prev = function () {
    var target = document.getElementsByClassName("cards-wrapper")[0];
    var count = target.getElementsByClassName("card").length;
    if (step != 0) {
        step += 1024;
        target.style.webkitTransform = "translate3d(" + step + "px,0,0)";
    }
    else {
        //step=500 *count-1;
        target.style.webkitTransform = "translate3d(" + step + "px,0,0)";
        //step += 1024;
    }


};
